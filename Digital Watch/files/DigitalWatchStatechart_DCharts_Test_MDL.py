"""
__DigitalWatchStatechart_DCharts_MDL.py_____________________________________________________

Automatically generated AToM3 Model File (Do not modify directly)
Author: ward
Modified: Thu Dec 13 01:57:57 2012
____________________________________________________________________________________________
"""
from stickylink import *
from widthXfillXdecoration import *
from Composite import *
from Basic import *
from Orthogonal import *
from contains import *
from Hyperedge import *
from orthogonality import *
from graph_Basic import *
from graph_Orthogonal import *
from graph_Hyperedge import *
from graph_orthogonality import *
from graph_contains import *
from graph_Composite import *
from ATOM3Enum import *
from ATOM3String import *
from ATOM3BottomType import *
from ATOM3Constraint import *
from ATOM3Attribute import *
from ATOM3Float import *
from ATOM3List import *
from ATOM3Link import *
from ATOM3Connection import *
from ATOM3Boolean import *
from ATOM3Appearance import *
from ATOM3Text import *
from ATOM3Action import *
from ATOM3Integer import *
from ATOM3Port import *
from ATOM3MSEnum import *

def DigitalWatchStatechart_DCharts_MDL(self, rootNode, DChartsRootNode=None):

    # --- Generating attributes code for ASG DCharts ---
    if( DChartsRootNode ): 
        # variables
        DChartsRootNode.variables.setValue('\n')
        DChartsRootNode.variables.setHeight(15)

        # misc
        DChartsRootNode.misc.setValue('\n')
        DChartsRootNode.misc.setHeight(15)

        # event_clauses
        DChartsRootNode.event_clauses.setValue('\n')
        DChartsRootNode.event_clauses.setHeight(15)
    # --- ASG attributes over ---


    self.obj28=Composite(self)
    self.obj28.isGraphObjectVisual = True

    if(hasattr(self.obj28, '_setHierarchicalLink')):
      self.obj28._setHierarchicalLink(False)

    # auto_adjust
    self.obj28.auto_adjust.setValue((None, 1))
    self.obj28.auto_adjust.config = 0

    # name
    self.obj28.name.setValue('DigitalWatch')

    # is_default
    self.obj28.is_default.setValue((None, 0))
    self.obj28.is_default.config = 0

    # visible
    self.obj28.visible.setValue((None, 1))
    self.obj28.visible.config = 0

    # exit_action
    self.obj28.exit_action.setValue('\n')
    self.obj28.exit_action.setHeight(15)

    # enter_action
    self.obj28.enter_action.setValue('\n')
    self.obj28.enter_action.setHeight(15)

    self.obj28.graphClass_= graph_Composite
    if self.genGraphics:
       new_obj = graph_Composite(380.0,700.0,self.obj28)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Composite", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf2.handler,308.0,63.0,1595.0,790.0)
       self.UMLmodel.itemconfig(new_obj.gf2.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf2.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf2.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf2.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,308.0,56.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='DigitalWatch')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj28.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj28)
    self.globalAndLocalPostcondition(self.obj28, rootNode)
    self.obj28.postAction( rootNode.CREATE )

    self.obj29=Basic(self)
    self.obj29.isGraphObjectVisual = True

    if(hasattr(self.obj29, '_setHierarchicalLink')):
      self.obj29._setHierarchicalLink(False)

    # is_default
    self.obj29.is_default.setValue((None, 1))
    self.obj29.is_default.config = 0

    # name
    self.obj29.name.setValue('Time')

    # exit_action
    self.obj29.exit_action.setValue('\n')
    self.obj29.exit_action.setHeight(15)

    # enter_action
    self.obj29.enter_action.setValue('\n')
    self.obj29.enter_action.setHeight(15)

    self.obj29.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(500.0,240.0,self.obj29)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,512.0,243.0,530.0,261.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,523.125,270.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Time')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font-159207508')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj29.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj29)
    self.globalAndLocalPostcondition(self.obj29, rootNode)
    self.obj29.postAction( rootNode.CREATE )

    self.obj30=Basic(self)
    self.obj30.isGraphObjectVisual = True

    if(hasattr(self.obj30, '_setHierarchicalLink')):
      self.obj30._setHierarchicalLink(False)

    # is_default
    self.obj30.is_default.setValue((None, 1))
    self.obj30.is_default.config = 0

    # name
    self.obj30.name.setValue('TimeUpdate')

    # exit_action
    self.obj30.exit_action.setValue('\n')
    self.obj30.exit_action.setHeight(15)

    # enter_action
    self.obj30.enter_action.setValue('\n')
    self.obj30.enter_action.setHeight(15)

    self.obj30.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(400.0,640.0,self.obj30)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,412.0,643.0,430.0,661.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,423.125,670.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='TimeUpdate')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font58756200')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj30.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj30)
    self.globalAndLocalPostcondition(self.obj30, rootNode)
    self.obj30.postAction( rootNode.CREATE )

    self.obj31=Basic(self)
    self.obj31.isGraphObjectVisual = True

    if(hasattr(self.obj31, '_setHierarchicalLink')):
      self.obj31._setHierarchicalLink(False)

    # is_default
    self.obj31.is_default.setValue((None, 0))
    self.obj31.is_default.config = 0

    # name
    self.obj31.name.setValue('LightOn')

    # exit_action
    self.obj31.exit_action.setValue('\n')
    self.obj31.exit_action.setHeight(15)

    # enter_action
    self.obj31.enter_action.setValue('\n')
    self.obj31.enter_action.setHeight(15)

    self.obj31.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(360.0,400.0,self.obj31)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,372.0,403.0,390.0,421.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,383.125,430.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='LightOn')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font54742984')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj31.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj31)
    self.globalAndLocalPostcondition(self.obj31, rootNode)
    self.obj31.postAction( rootNode.CREATE )

    self.obj32=Basic(self)
    self.obj32.isGraphObjectVisual = True

    if(hasattr(self.obj32, '_setHierarchicalLink')):
      self.obj32._setHierarchicalLink(False)

    # is_default
    self.obj32.is_default.setValue((None, 1))
    self.obj32.is_default.config = 0

    # name
    self.obj32.name.setValue('LightOff')

    # exit_action
    self.obj32.exit_action.setValue('\n')
    self.obj32.exit_action.setHeight(15)

    # enter_action
    self.obj32.enter_action.setValue('\n')
    self.obj32.enter_action.setHeight(15)

    self.obj32.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(600.0,400.0,self.obj32)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,612.0,403.0,630.0,421.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,623.125,430.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='LightOff')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font53782720')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj32.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj32)
    self.globalAndLocalPostcondition(self.obj32, rootNode)
    self.obj32.postAction( rootNode.CREATE )

    self.obj33=Basic(self)
    self.obj33.isGraphObjectVisual = True

    if(hasattr(self.obj33, '_setHierarchicalLink')):
      self.obj33._setHierarchicalLink(False)

    # is_default
    self.obj33.is_default.setValue((None, 0))
    self.obj33.is_default.config = 0

    # name
    self.obj33.name.setValue('Button released')

    # exit_action
    self.obj33.exit_action.setValue('\n')
    self.obj33.exit_action.setHeight(15)

    # enter_action
    self.obj33.enter_action.setValue('\n')
    self.obj33.enter_action.setHeight(15)

    self.obj33.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(440.0,520.0,self.obj33)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,452.0,523.0,470.0,541.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,463.125,550.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Button released')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font54893384')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj33.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj33)
    self.globalAndLocalPostcondition(self.obj33, rootNode)
    self.obj33.postAction( rootNode.CREATE )

    self.obj34=Basic(self)
    self.obj34.isGraphObjectVisual = True

    if(hasattr(self.obj34, '_setHierarchicalLink')):
      self.obj34._setHierarchicalLink(False)

    # is_default
    self.obj34.is_default.setValue((None, 0))
    self.obj34.is_default.config = 0

    # name
    self.obj34.name.setValue('Chrono')

    # exit_action
    self.obj34.exit_action.setValue('\n')
    self.obj34.exit_action.setHeight(15)

    # enter_action
    self.obj34.enter_action.setValue('\n')
    self.obj34.enter_action.setHeight(15)

    self.obj34.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(400.0,120.0,self.obj34)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,412.0,123.0,430.0,141.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,423.125,150.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font54188728')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj34.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj34)
    self.globalAndLocalPostcondition(self.obj34, rootNode)
    self.obj34.postAction( rootNode.CREATE )

    self.obj35=Basic(self)
    self.obj35.isGraphObjectVisual = True

    if(hasattr(self.obj35, '_setHierarchicalLink')):
      self.obj35._setHierarchicalLink(False)

    # is_default
    self.obj35.is_default.setValue((None, 1))
    self.obj35.is_default.config = 0

    # name
    self.obj35.name.setValue('Chrono off')

    # exit_action
    self.obj35.exit_action.setValue('\n')
    self.obj35.exit_action.setHeight(15)

    # enter_action
    self.obj35.enter_action.setValue('\n')
    self.obj35.enter_action.setHeight(15)

    self.obj35.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(740.0,420.0,self.obj35)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,752.0,423.0,770.0,441.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,763.125,450.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono off')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font66002384')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj35.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj35)
    self.globalAndLocalPostcondition(self.obj35, rootNode)
    self.obj35.postAction( rootNode.CREATE )

    self.obj36=Basic(self)
    self.obj36.isGraphObjectVisual = True

    if(hasattr(self.obj36, '_setHierarchicalLink')):
      self.obj36._setHierarchicalLink(False)

    # is_default
    self.obj36.is_default.setValue((None, 0))
    self.obj36.is_default.config = 0

    # name
    self.obj36.name.setValue('Chrono on')

    # exit_action
    self.obj36.exit_action.setValue('\n')
    self.obj36.exit_action.setHeight(15)

    # enter_action
    self.obj36.enter_action.setValue('\n')
    self.obj36.enter_action.setHeight(15)

    self.obj36.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(960.0,440.0,self.obj36)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,972.0,443.0,990.0,461.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,983.125,470.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono on')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font67559792')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj36.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj36)
    self.globalAndLocalPostcondition(self.obj36, rootNode)
    self.obj36.postAction( rootNode.CREATE )

    self.obj37=Basic(self)
    self.obj37.isGraphObjectVisual = True

    if(hasattr(self.obj37, '_setHierarchicalLink')):
      self.obj37._setHierarchicalLink(False)

    # is_default
    self.obj37.is_default.setValue((None, 0))
    self.obj37.is_default.config = 0

    # name
    self.obj37.name.setValue('Time edit')

    # exit_action
    self.obj37.exit_action.setValue('\n')
    self.obj37.exit_action.setHeight(15)

    # enter_action
    self.obj37.enter_action.setValue('\n')
    self.obj37.enter_action.setHeight(15)

    self.obj37.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(700.0,100.0,self.obj37)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,712.0,103.0,730.0,121.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,723.125,130.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Time edit')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font60347960')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj37.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj37)
    self.globalAndLocalPostcondition(self.obj37, rootNode)
    self.obj37.postAction( rootNode.CREATE )

    self.obj38=Basic(self)
    self.obj38.isGraphObjectVisual = True

    if(hasattr(self.obj38, '_setHierarchicalLink')):
      self.obj38._setHierarchicalLink(False)

    # is_default
    self.obj38.is_default.setValue((None, 0))
    self.obj38.is_default.config = 0

    # name
    self.obj38.name.setValue('Alarm')

    # exit_action
    self.obj38.exit_action.setValue('\n')
    self.obj38.exit_action.setHeight(15)

    # enter_action
    self.obj38.enter_action.setValue('\n')
    self.obj38.enter_action.setHeight(15)

    self.obj38.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(700.0,320.0,self.obj38)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,712.0,323.0,730.0,341.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,723.125,350.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font96836552')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj38.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj38)
    self.globalAndLocalPostcondition(self.obj38, rootNode)
    self.obj38.postAction( rootNode.CREATE )

    self.obj39=Basic(self)
    self.obj39.isGraphObjectVisual = True

    if(hasattr(self.obj39, '_setHierarchicalLink')):
      self.obj39._setHierarchicalLink(False)

    # is_default
    self.obj39.is_default.setValue((None, 0))
    self.obj39.is_default.config = 0

    # name
    self.obj39.name.setValue('Edit')

    # exit_action
    self.obj39.exit_action.setValue('\n')
    self.obj39.exit_action.setHeight(15)

    # enter_action
    self.obj39.enter_action.setValue('\n')
    self.obj39.enter_action.setHeight(15)

    self.obj39.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1000.0,140.0,self.obj39)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1012.0,143.0,1030.0,161.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1023.125,170.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Edit')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font181967948')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj39.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj39)
    self.globalAndLocalPostcondition(self.obj39, rootNode)
    self.obj39.postAction( rootNode.CREATE )

    self.obj40=Basic(self)
    self.obj40.isGraphObjectVisual = True

    if(hasattr(self.obj40, '_setHierarchicalLink')):
      self.obj40._setHierarchicalLink(False)

    # is_default
    self.obj40.is_default.setValue((None, 0))
    self.obj40.is_default.config = 0

    # name
    self.obj40.name.setValue('Next')

    # exit_action
    self.obj40.exit_action.setValue('\n')
    self.obj40.exit_action.setHeight(15)

    # enter_action
    self.obj40.enter_action.setValue('\n')
    self.obj40.enter_action.setHeight(15)

    self.obj40.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1000.0,280.0,self.obj40)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1012.0,283.0,1030.0,301.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1023.125,310.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Next')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font181966220')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj40.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj40)
    self.globalAndLocalPostcondition(self.obj40, rootNode)
    self.obj40.postAction( rootNode.CREATE )

    self.obj41=Basic(self)
    self.obj41.isGraphObjectVisual = True

    if(hasattr(self.obj41, '_setHierarchicalLink')):
      self.obj41._setHierarchicalLink(False)

    # is_default
    self.obj41.is_default.setValue((None, 0))
    self.obj41.is_default.config = 0

    # name
    self.obj41.name.setValue('Start edit')

    # exit_action
    self.obj41.exit_action.setValue('\n')
    self.obj41.exit_action.setHeight(15)

    # enter_action
    self.obj41.enter_action.setValue('\n')
    self.obj41.enter_action.setHeight(15)

    self.obj41.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(800.0,200.0,self.obj41)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,812.0,203.0,830.0,221.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,823.125,230.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Start edit')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font182352332')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj41.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj41)
    self.globalAndLocalPostcondition(self.obj41, rootNode)
    self.obj41.postAction( rootNode.CREATE )

    self.obj42=Basic(self)
    self.obj42.isGraphObjectVisual = True

    if(hasattr(self.obj42, '_setHierarchicalLink')):
      self.obj42._setHierarchicalLink(False)

    # is_default
    self.obj42.is_default.setValue((None, 1))
    self.obj42.is_default.config = 0

    # name
    self.obj42.name.setValue('Check alarm')

    # exit_action
    self.obj42.exit_action.setValue('\n')
    self.obj42.exit_action.setHeight(15)

    # enter_action
    self.obj42.enter_action.setValue('\n')
    self.obj42.enter_action.setHeight(15)

    self.obj42.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1220.0,640.0,self.obj42)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1232.0,643.0,1250.0,661.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1243.125,670.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Check alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font41751256')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj42.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj42)
    self.globalAndLocalPostcondition(self.obj42, rootNode)
    self.obj42.postAction( rootNode.CREATE )

    self.obj43=Basic(self)
    self.obj43.isGraphObjectVisual = True

    if(hasattr(self.obj43, '_setHierarchicalLink')):
      self.obj43._setHierarchicalLink(False)

    # is_default
    self.obj43.is_default.setValue((None, 0))
    self.obj43.is_default.config = 0

    # name
    self.obj43.name.setValue('Start alarm')

    # exit_action
    self.obj43.exit_action.setValue('\n')
    self.obj43.exit_action.setHeight(15)

    # enter_action
    self.obj43.enter_action.setValue('\n')
    self.obj43.enter_action.setHeight(15)

    self.obj43.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1380.0,640.0,self.obj43)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1392.0,643.0,1410.0,661.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1403.125,670.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Start alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font41752624')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj43.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj43)
    self.globalAndLocalPostcondition(self.obj43, rootNode)
    self.obj43.postAction( rootNode.CREATE )

    self.obj44=Basic(self)
    self.obj44.isGraphObjectVisual = True

    if(hasattr(self.obj44, '_setHierarchicalLink')):
      self.obj44._setHierarchicalLink(False)

    # is_default
    self.obj44.is_default.setValue((None, 0))
    self.obj44.is_default.config = 0

    # name
    self.obj44.name.setValue('Blink')

    # exit_action
    self.obj44.exit_action.setValue('\n')
    self.obj44.exit_action.setHeight(15)

    # enter_action
    self.obj44.enter_action.setValue('\n')
    self.obj44.enter_action.setHeight(15)

    self.obj44.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1540.0,640.0,self.obj44)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1552.0,643.0,1570.0,661.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1563.125,670.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Blink')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font41764480')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj44.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj44)
    self.globalAndLocalPostcondition(self.obj44, rootNode)
    self.obj44.postAction( rootNode.CREATE )

    self.obj45=Basic(self)
    self.obj45.isGraphObjectVisual = True

    if(hasattr(self.obj45, '_setHierarchicalLink')):
      self.obj45._setHierarchicalLink(False)

    # is_default
    self.obj45.is_default.setValue((None, 1))
    self.obj45.is_default.config = 0

    # name
    self.obj45.name.setValue('AlarmOff')

    # exit_action
    self.obj45.exit_action.setValue('\n')
    self.obj45.exit_action.setHeight(15)

    # enter_action
    self.obj45.enter_action.setValue('\n')
    self.obj45.enter_action.setHeight(15)

    self.obj45.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(720.0,580.0,self.obj45)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,732.0,583.0,750.0,601.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,743.125,610.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmOff')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font180792460')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj45.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj45)
    self.globalAndLocalPostcondition(self.obj45, rootNode)
    self.obj45.postAction( rootNode.CREATE )

    self.obj46=Basic(self)
    self.obj46.isGraphObjectVisual = True

    if(hasattr(self.obj46, '_setHierarchicalLink')):
      self.obj46._setHierarchicalLink(False)

    # is_default
    self.obj46.is_default.setValue((None, 0))
    self.obj46.is_default.config = 0

    # name
    self.obj46.name.setValue('AlarmOn')

    # exit_action
    self.obj46.exit_action.setValue('\n')
    self.obj46.exit_action.setHeight(15)

    # enter_action
    self.obj46.enter_action.setValue('\n')
    self.obj46.enter_action.setHeight(15)

    self.obj46.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(900.0,580.0,self.obj46)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,912.0,583.0,930.0,601.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,923.125,610.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmOn')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font180792172')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj46.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj46)
    self.globalAndLocalPostcondition(self.obj46, rootNode)
    self.obj46.postAction( rootNode.CREATE )

    self.obj47=Basic(self)
    self.obj47.isGraphObjectVisual = True

    if(hasattr(self.obj47, '_setHierarchicalLink')):
      self.obj47._setHierarchicalLink(False)

    # is_default
    self.obj47.is_default.setValue((None, 0))
    self.obj47.is_default.config = 0

    # name
    self.obj47.name.setValue('AlarmStart')

    # exit_action
    self.obj47.exit_action.setValue('\n')
    self.obj47.exit_action.setHeight(15)

    # enter_action
    self.obj47.enter_action.setValue('\n')
    self.obj47.enter_action.setHeight(15)

    self.obj47.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1040.0,580.0,self.obj47)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1052.0,583.0,1070.0,601.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1063.125,610.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmStart')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font187842668')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj47.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj47)
    self.globalAndLocalPostcondition(self.obj47, rootNode)
    self.obj47.postAction( rootNode.CREATE )

    self.obj48=Basic(self)
    self.obj48.isGraphObjectVisual = True

    if(hasattr(self.obj48, '_setHierarchicalLink')):
      self.obj48._setHierarchicalLink(False)

    # is_default
    self.obj48.is_default.setValue((None, 1))
    self.obj48.is_default.config = 0

    # name
    self.obj48.name.setValue('Start')

    # exit_action
    self.obj48.exit_action.setValue('\n')
    self.obj48.exit_action.setHeight(15)

    # enter_action
    self.obj48.enter_action.setValue('\n')
    self.obj48.enter_action.setHeight(15)

    self.obj48.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(120.0,420.0,self.obj48)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,132.0,423.0,150.0,441.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,143.125,450.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Start')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font162174796')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj48.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj48)
    self.globalAndLocalPostcondition(self.obj48, rootNode)
    self.obj48.postAction( rootNode.CREATE )

    self.obj49=Basic(self)
    self.obj49.isGraphObjectVisual = True

    if(hasattr(self.obj49, '_setHierarchicalLink')):
      self.obj49._setHierarchicalLink(False)

    # is_default
    self.obj49.is_default.setValue((None, 0))
    self.obj49.is_default.config = 0

    # name
    self.obj49.name.setValue('Stop')

    # exit_action
    self.obj49.exit_action.setValue('\n')
    self.obj49.exit_action.setHeight(15)

    # enter_action
    self.obj49.enter_action.setValue('\n')
    self.obj49.enter_action.setHeight(15)

    self.obj49.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1760.0,420.0,self.obj49)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1772.0,423.0,1790.0,441.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1783.125,450.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Stop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font162158284')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj49.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj49)
    self.globalAndLocalPostcondition(self.obj49, rootNode)
    self.obj49.postAction( rootNode.CREATE )

    self.obj50=Basic(self)
    self.obj50.isGraphObjectVisual = True

    if(hasattr(self.obj50, '_setHierarchicalLink')):
      self.obj50._setHierarchicalLink(False)

    # is_default
    self.obj50.is_default.setValue((None, 0))
    self.obj50.is_default.config = 0

    # name
    self.obj50.name.setValue('ChronoPaused2')

    # exit_action
    self.obj50.exit_action.setValue('\n')
    self.obj50.exit_action.setHeight(15)

    # enter_action
    self.obj50.enter_action.setValue('\n')
    self.obj50.enter_action.setHeight(15)

    self.obj50.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1240.0,220.0,self.obj50)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1265.99663708,221.3,1282.91663708,244.7)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1276.45413708,256.4)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='ChronoPaused2')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font84109384')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [0.9400000000000002, 1.3000000000000005]
    else: new_obj = None
    self.obj50.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj50)
    self.globalAndLocalPostcondition(self.obj50, rootNode)
    self.obj50.postAction( rootNode.CREATE )

    self.obj51=Basic(self)
    self.obj51.isGraphObjectVisual = True

    if(hasattr(self.obj51, '_setHierarchicalLink')):
      self.obj51._setHierarchicalLink(False)

    # is_default
    self.obj51.is_default.setValue((None, 0))
    self.obj51.is_default.config = 0

    # name
    self.obj51.name.setValue('ChronoShowing')

    # exit_action
    self.obj51.exit_action.setValue('\n')
    self.obj51.exit_action.setHeight(15)

    # enter_action
    self.obj51.enter_action.setValue('\n')
    self.obj51.enter_action.setHeight(15)

    self.obj51.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1322.0,409.0,self.obj51)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1334.0,412.0,1352.0,430.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1345.125,439.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='ChronoShowing')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font84110920')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj51.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj51)
    self.globalAndLocalPostcondition(self.obj51, rootNode)
    self.obj51.postAction( rootNode.CREATE )

    self.obj52=Basic(self)
    self.obj52.isGraphObjectVisual = True

    if(hasattr(self.obj52, '_setHierarchicalLink')):
      self.obj52._setHierarchicalLink(False)

    # is_default
    self.obj52.is_default.setValue((None, 0))
    self.obj52.is_default.config = 0

    # name
    self.obj52.name.setValue('ChronoRunning')

    # exit_action
    self.obj52.exit_action.setValue('\n')
    self.obj52.exit_action.setHeight(15)

    # enter_action
    self.obj52.enter_action.setValue('\n')
    self.obj52.enter_action.setHeight(15)

    self.obj52.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1482.0,409.0,self.obj52)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1494.0,412.0,1512.0,430.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1505.125,439.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='ChronoRunning')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font86165448')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj52.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj52)
    self.globalAndLocalPostcondition(self.obj52, rootNode)
    self.obj52.postAction( rootNode.CREATE )

    self.obj53=Basic(self)
    self.obj53.isGraphObjectVisual = True

    if(hasattr(self.obj53, '_setHierarchicalLink')):
      self.obj53._setHierarchicalLink(False)

    # is_default
    self.obj53.is_default.setValue((None, 0))
    self.obj53.is_default.config = 0

    # name
    self.obj53.name.setValue('ChronoPaused')

    # exit_action
    self.obj53.exit_action.setValue('\n')
    self.obj53.exit_action.setHeight(15)

    # enter_action
    self.obj53.enter_action.setValue('\n')
    self.obj53.enter_action.setHeight(15)

    self.obj53.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1420.0,180.0,self.obj53)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1432.0,183.0,1450.0,201.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1443.125,210.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='ChronoPaused')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font98941384')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj53.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj53)
    self.globalAndLocalPostcondition(self.obj53, rootNode)
    self.obj53.postAction( rootNode.CREATE )

    self.obj54=Basic(self)
    self.obj54.isGraphObjectVisual = True

    if(hasattr(self.obj54, '_setHierarchicalLink')):
      self.obj54._setHierarchicalLink(False)

    # is_default
    self.obj54.is_default.setValue((None, 0))
    self.obj54.is_default.config = 0

    # name
    self.obj54.name.setValue('ChronoNotShowing')

    # exit_action
    self.obj54.exit_action.setValue('\n')
    self.obj54.exit_action.setHeight(15)

    # enter_action
    self.obj54.enter_action.setValue('\n')
    self.obj54.enter_action.setHeight(15)

    self.obj54.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1260.0,100.0,self.obj54)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1272.0,103.0,1290.0,121.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1283.125,130.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='ChronoNotShowing')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font99055560')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj54.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj54)
    self.globalAndLocalPostcondition(self.obj54, rootNode)
    self.obj54.postAction( rootNode.CREATE )

    self.obj55=Basic(self)
    self.obj55.isGraphObjectVisual = True

    if(hasattr(self.obj55, '_setHierarchicalLink')):
      self.obj55._setHierarchicalLink(False)

    # is_default
    self.obj55.is_default.setValue((None, 0))
    self.obj55.is_default.config = 0

    # name
    self.obj55.name.setValue('ChronoRunning2')

    # exit_action
    self.obj55.exit_action.setValue('\n')
    self.obj55.exit_action.setHeight(15)

    # enter_action
    self.obj55.enter_action.setValue('\n')
    self.obj55.enter_action.setHeight(15)

    self.obj55.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1420.0,100.0,self.obj55)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1432.0,103.0,1450.0,121.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1443.125,130.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='ChronoRunning2')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font101020936')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj55.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj55)
    self.globalAndLocalPostcondition(self.obj55, rootNode)
    self.obj55.postAction( rootNode.CREATE )

    self.obj56=Basic(self)
    self.obj56.isGraphObjectVisual = True

    if(hasattr(self.obj56, '_setHierarchicalLink')):
      self.obj56._setHierarchicalLink(False)

    # is_default
    self.obj56.is_default.setValue((None, 0))
    self.obj56.is_default.config = 0

    # name
    self.obj56.name.setValue('ChronoShowing2')

    # exit_action
    self.obj56.exit_action.setValue('\n')
    self.obj56.exit_action.setHeight(15)

    # enter_action
    self.obj56.enter_action.setValue('\n')
    self.obj56.enter_action.setHeight(15)

    self.obj56.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1260.0,160.0,self.obj56)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1272.0,163.0,1290.0,181.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1283.125,190.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='ChronoShowing2')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font103625864')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj56.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj56)
    self.globalAndLocalPostcondition(self.obj56, rootNode)
    self.obj56.postAction( rootNode.CREATE )

    self.obj57=Basic(self)
    self.obj57.isGraphObjectVisual = True

    if(hasattr(self.obj57, '_setHierarchicalLink')):
      self.obj57._setHierarchicalLink(False)

    # is_default
    self.obj57.is_default.setValue((None, 1))
    self.obj57.is_default.config = 0

    # name
    self.obj57.name.setValue('startTest')

    # exit_action
    self.obj57.exit_action.setValue('\n')
    self.obj57.exit_action.setHeight(15)

    # enter_action
    self.obj57.enter_action.setValue('\n')
    self.obj57.enter_action.setHeight(15)

    self.obj57.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1240.0,340.0,self.obj57)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1252.0,343.0,1270.0,361.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1263.125,370.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='startTest')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font105265736')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj57.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj57)
    self.globalAndLocalPostcondition(self.obj57, rootNode)
    self.obj57.postAction( rootNode.CREATE )

    self.obj59=Orthogonal(self)
    self.obj59.isGraphObjectVisual = True

    if(hasattr(self.obj59, '_setHierarchicalLink')):
      self.obj59._setHierarchicalLink(False)

    # visible
    self.obj59.visible.setValue((None, 1))
    self.obj59.visible.config = 0

    # name
    self.obj59.name.setValue('Main timer loop')

    # auto_adjust
    self.obj59.auto_adjust.setValue((None, 1))
    self.obj59.auto_adjust.config = 0

    self.obj59.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(260.0,520.0,self.obj59)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,368.0,594.0,478.0,685.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,368.0,587.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Main timer loop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj59.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj59)
    self.globalAndLocalPostcondition(self.obj59, rootNode)
    self.obj59.postAction( rootNode.CREATE )

    self.obj60=Orthogonal(self)
    self.obj60.isGraphObjectVisual = True

    if(hasattr(self.obj60, '_setHierarchicalLink')):
      self.obj60._setHierarchicalLink(False)

    # visible
    self.obj60.visible.setValue((None, 1))
    self.obj60.visible.config = 0

    # name
    self.obj60.name.setValue('Displayloop')

    # auto_adjust
    self.obj60.auto_adjust.setValue((None, 1))
    self.obj60.auto_adjust.config = 0

    self.obj60.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(400.0,320.0,self.obj60)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,346.0,83.0,1073.0,365.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,346.0,76.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Displayloop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj60.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj60)
    self.globalAndLocalPostcondition(self.obj60, rootNode)
    self.obj60.postAction( rootNode.CREATE )

    self.obj61=Orthogonal(self)
    self.obj61.isGraphObjectVisual = True

    if(hasattr(self.obj61, '_setHierarchicalLink')):
      self.obj61._setHierarchicalLink(False)

    # visible
    self.obj61.visible.setValue((None, 1))
    self.obj61.visible.config = 0

    # name
    self.obj61.name.setValue('Backlight')

    # auto_adjust
    self.obj61.auto_adjust.setValue((None, 1))
    self.obj61.auto_adjust.config = 0

    self.obj61.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(340.0,240.0,self.obj61)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,346.0,397.0,661.0,565.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,346.0,390.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Backlight')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 0.83
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj61.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj61)
    self.globalAndLocalPostcondition(self.obj61, rootNode)
    self.obj61.postAction( rootNode.CREATE )

    self.obj62=Orthogonal(self)
    self.obj62.isGraphObjectVisual = True

    if(hasattr(self.obj62, '_setHierarchicalLink')):
      self.obj62._setHierarchicalLink(False)

    # visible
    self.obj62.visible.setValue((None, 1))
    self.obj62.visible.config = 0

    # name
    self.obj62.name.setValue('Chrono loop')

    # auto_adjust
    self.obj62.auto_adjust.setValue((None, 1))
    self.obj62.auto_adjust.config = 0

    self.obj62.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(600.0,360.0,self.obj62)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,711.0,392.0,1183.0,518.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,711.0,385.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono loop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj62.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj62)
    self.globalAndLocalPostcondition(self.obj62, rootNode)
    self.obj62.postAction( rootNode.CREATE )

    self.obj63=Orthogonal(self)
    self.obj63.isGraphObjectVisual = True

    if(hasattr(self.obj63, '_setHierarchicalLink')):
      self.obj63._setHierarchicalLink(False)

    # visible
    self.obj63.visible.setValue((None, 1))
    self.obj63.visible.config = 0

    # name
    self.obj63.name.setValue('Alarm')

    # auto_adjust
    self.obj63.auto_adjust.setValue((None, 1))
    self.obj63.auto_adjust.config = 0

    self.obj63.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(1100.0,660.0,self.obj63)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,1187.0,579.0,1588.0,727.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='darkgray')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,1187.0,572.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj63.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj63)
    self.globalAndLocalPostcondition(self.obj63, rootNode)
    self.obj63.postAction( rootNode.CREATE )

    self.obj64=Orthogonal(self)
    self.obj64.isGraphObjectVisual = True

    if(hasattr(self.obj64, '_setHierarchicalLink')):
      self.obj64._setHierarchicalLink(False)

    # visible
    self.obj64.visible.setValue((None, 1))
    self.obj64.visible.config = 0

    # name
    self.obj64.name.setValue('AlarmState')

    # auto_adjust
    self.obj64.auto_adjust.setValue((None, 1))
    self.obj64.auto_adjust.config = 0

    self.obj64.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(800.0,420.0,self.obj64)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,701.0,567.0,1113.0,783.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='darkgray')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,701.0,560.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmState')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj64.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj64)
    self.globalAndLocalPostcondition(self.obj64, rootNode)
    self.obj64.postAction( rootNode.CREATE )

    self.obj65=Orthogonal(self)
    self.obj65.isGraphObjectVisual = True

    if(hasattr(self.obj65, '_setHierarchicalLink')):
      self.obj65._setHierarchicalLink(False)

    # visible
    self.obj65.visible.setValue((None, 1))
    self.obj65.visible.config = 0

    # name
    self.obj65.name.setValue('Test')

    # auto_adjust
    self.obj65.auto_adjust.setValue((None, 1))
    self.obj65.auto_adjust.config = 0

    self.obj65.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(1220.0,60.0,self.obj65)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,1201.0,97.0,1573.0,454.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='darkgray')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,1201.0,90.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Test')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [2.2499999999999996, 3.599999999999999]
    else: new_obj = None
    self.obj65.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj65)
    self.globalAndLocalPostcondition(self.obj65, rootNode)
    self.obj65.postAction( rootNode.CREATE )

    self.obj66=contains(self)
    self.obj66.isGraphObjectVisual = True

    if(hasattr(self.obj66, '_setHierarchicalLink')):
      self.obj66._setHierarchicalLink(False)

    self.obj66.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(-405.62601771,943.084048042,self.obj66)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj66.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj66)
    self.globalAndLocalPostcondition(self.obj66, rootNode)
    self.obj66.postAction( rootNode.CREATE )

    self.obj67=contains(self)
    self.obj67.isGraphObjectVisual = True

    if(hasattr(self.obj67, '_setHierarchicalLink')):
      self.obj67._setHierarchicalLink(False)

    self.obj67.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(433.005748984,279.53909536,self.obj67)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj67.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj67)
    self.globalAndLocalPostcondition(self.obj67, rootNode)
    self.obj67.postAction( rootNode.CREATE )

    self.obj68=contains(self)
    self.obj68.isGraphObjectVisual = True

    if(hasattr(self.obj68, '_setHierarchicalLink')):
      self.obj68._setHierarchicalLink(False)

    self.obj68.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(-298.41125024,407.51228148,self.obj68)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj68.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj68)
    self.globalAndLocalPostcondition(self.obj68, rootNode)
    self.obj68.postAction( rootNode.CREATE )

    self.obj69=contains(self)
    self.obj69.isGraphObjectVisual = True

    if(hasattr(self.obj69, '_setHierarchicalLink')):
      self.obj69._setHierarchicalLink(False)

    self.obj69.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(-195.49425102,469.53909536,self.obj69)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj69.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj69)
    self.globalAndLocalPostcondition(self.obj69, rootNode)
    self.obj69.postAction( rootNode.CREATE )

    self.obj70=contains(self)
    self.obj70.isGraphObjectVisual = True

    if(hasattr(self.obj70, '_setHierarchicalLink')):
      self.obj70._setHierarchicalLink(False)

    self.obj70.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(-192.91125024,690.01228148,self.obj70)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj70.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj70)
    self.globalAndLocalPostcondition(self.obj70, rootNode)
    self.obj70.postAction( rootNode.CREATE )

    self.obj71=contains(self)
    self.obj71.isGraphObjectVisual = True

    if(hasattr(self.obj71, '_setHierarchicalLink')):
      self.obj71._setHierarchicalLink(False)

    self.obj71.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(476.78301645,208.443472332,self.obj71)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj71.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj71)
    self.globalAndLocalPostcondition(self.obj71, rootNode)
    self.obj71.postAction( rootNode.CREATE )

    self.obj72=contains(self)
    self.obj72.isGraphObjectVisual = True

    if(hasattr(self.obj72, '_setHierarchicalLink')):
      self.obj72._setHierarchicalLink(False)

    self.obj72.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(892.67160001,414.809895833,self.obj72)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj72.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj72)
    self.globalAndLocalPostcondition(self.obj72, rootNode)
    self.obj72.postAction( rootNode.CREATE )

    self.obj73=contains(self)
    self.obj73.isGraphObjectVisual = True

    if(hasattr(self.obj73, '_setHierarchicalLink')):
      self.obj73._setHierarchicalLink(False)

    self.obj73.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1067.00574898,459.53909536,self.obj73)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj73.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj73)
    self.globalAndLocalPostcondition(self.obj73, rootNode)
    self.obj73.postAction( rootNode.CREATE )

    self.obj74=contains(self)
    self.obj74.isGraphObjectVisual = True

    if(hasattr(self.obj74, '_setHierarchicalLink')):
      self.obj74._setHierarchicalLink(False)

    self.obj74.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(757.671600007,244.309895833,self.obj74)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj74.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj74)
    self.globalAndLocalPostcondition(self.obj74, rootNode)
    self.obj74.postAction( rootNode.CREATE )

    self.obj75=contains(self)
    self.obj75.isGraphObjectVisual = True

    if(hasattr(self.obj75, '_setHierarchicalLink')):
      self.obj75._setHierarchicalLink(False)

    self.obj75.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1122.78301645,129.443472332,self.obj75)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj75.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj75)
    self.globalAndLocalPostcondition(self.obj75, rootNode)
    self.obj75.postAction( rootNode.CREATE )

    self.obj76=contains(self)
    self.obj76.isGraphObjectVisual = True

    if(hasattr(self.obj76, '_setHierarchicalLink')):
      self.obj76._setHierarchicalLink(False)

    self.obj76.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1275.48966832,122.71762454,self.obj76)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj76.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj76)
    self.globalAndLocalPostcondition(self.obj76, rootNode)
    self.obj76.postAction( rootNode.CREATE )

    self.obj77=contains(self)
    self.obj77.isGraphObjectVisual = True

    if(hasattr(self.obj77, '_setHierarchicalLink')):
      self.obj77._setHierarchicalLink(False)

    self.obj77.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1410.00574898,223.03909536,self.obj77)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj77.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj77)
    self.globalAndLocalPostcondition(self.obj77, rootNode)
    self.obj77.postAction( rootNode.CREATE )

    self.obj78=contains(self)
    self.obj78.isGraphObjectVisual = True

    if(hasattr(self.obj78, '_setHierarchicalLink')):
      self.obj78._setHierarchicalLink(False)

    self.obj78.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(667.49483416,367.85881227,self.obj78)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj78.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj78)
    self.globalAndLocalPostcondition(self.obj78, rootNode)
    self.obj78.postAction( rootNode.CREATE )

    self.obj79=contains(self)
    self.obj79.isGraphObjectVisual = True

    if(hasattr(self.obj79, '_setHierarchicalLink')):
      self.obj79._setHierarchicalLink(False)

    self.obj79.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1231.78301645,878.443472332,self.obj79)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj79.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj79)
    self.globalAndLocalPostcondition(self.obj79, rootNode)
    self.obj79.postAction( rootNode.CREATE )

    self.obj80=contains(self)
    self.obj80.isGraphObjectVisual = True

    if(hasattr(self.obj80, '_setHierarchicalLink')):
      self.obj80._setHierarchicalLink(False)

    self.obj80.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1463.00047774,839.241255093,self.obj80)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj80.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj80)
    self.globalAndLocalPostcondition(self.obj80, rootNode)
    self.obj80.postAction( rootNode.CREATE )

    self.obj81=contains(self)
    self.obj81.isGraphObjectVisual = True

    if(hasattr(self.obj81, '_setHierarchicalLink')):
      self.obj81._setHierarchicalLink(False)

    self.obj81.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1403.17160001,892.309895833,self.obj81)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj81.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj81)
    self.globalAndLocalPostcondition(self.obj81, rootNode)
    self.obj81.postAction( rootNode.CREATE )

    self.obj82=contains(self)
    self.obj82.isGraphObjectVisual = True

    if(hasattr(self.obj82, '_setHierarchicalLink')):
      self.obj82._setHierarchicalLink(False)

    self.obj82.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(551.166756321,475.521548042,self.obj82)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj82.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj82)
    self.globalAndLocalPostcondition(self.obj82, rootNode)
    self.obj82.postAction( rootNode.CREATE )

    self.obj83=contains(self)
    self.obj83.isGraphObjectVisual = True

    if(hasattr(self.obj83, '_setHierarchicalLink')):
      self.obj83._setHierarchicalLink(False)

    self.obj83.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(605.005748984,539.53909536,self.obj83)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj83.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj83)
    self.globalAndLocalPostcondition(self.obj83, rootNode)
    self.obj83.postAction( rootNode.CREATE )

    self.obj84=contains(self)
    self.obj84.isGraphObjectVisual = True

    if(hasattr(self.obj84, '_setHierarchicalLink')):
      self.obj84._setHierarchicalLink(False)

    self.obj84.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(756.50574898,576.53909536,self.obj84)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj84.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj84)
    self.globalAndLocalPostcondition(self.obj84, rootNode)
    self.obj84.postAction( rootNode.CREATE )

    self.obj85=contains(self)
    self.obj85.isGraphObjectVisual = True

    if(hasattr(self.obj85, '_setHierarchicalLink')):
      self.obj85._setHierarchicalLink(False)

    self.obj85.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(2186.87398229,-604.915951958,self.obj85)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj85.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj85)
    self.globalAndLocalPostcondition(self.obj85, rootNode)
    self.obj85.postAction( rootNode.CREATE )

    self.obj86=contains(self)
    self.obj86.isGraphObjectVisual = True

    if(hasattr(self.obj86, '_setHierarchicalLink')):
      self.obj86._setHierarchicalLink(False)

    self.obj86.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(2209.87398229,-499.415951958,self.obj86)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj86.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj86)
    self.globalAndLocalPostcondition(self.obj86, rootNode)
    self.obj86.postAction( rootNode.CREATE )

    self.obj87=contains(self)
    self.obj87.isGraphObjectVisual = True

    if(hasattr(self.obj87, '_setHierarchicalLink')):
      self.obj87._setHierarchicalLink(False)

    self.obj87.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1445.48966832,303.21762454,self.obj87)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj87.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj87)
    self.globalAndLocalPostcondition(self.obj87, rootNode)
    self.obj87.postAction( rootNode.CREATE )

    self.obj88=contains(self)
    self.obj88.isGraphObjectVisual = True

    if(hasattr(self.obj88, '_setHierarchicalLink')):
      self.obj88._setHierarchicalLink(False)

    self.obj88.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1647.50574898,185.53909536,self.obj88)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj88.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj88)
    self.globalAndLocalPostcondition(self.obj88, rootNode)
    self.obj88.postAction( rootNode.CREATE )

    self.obj89=contains(self)
    self.obj89.isGraphObjectVisual = True

    if(hasattr(self.obj89, '_setHierarchicalLink')):
      self.obj89._setHierarchicalLink(False)

    self.obj89.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1682.48966832,157.71762454,self.obj89)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj89.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj89)
    self.globalAndLocalPostcondition(self.obj89, rootNode)
    self.obj89.postAction( rootNode.CREATE )

    self.obj90=contains(self)
    self.obj90.isGraphObjectVisual = True

    if(hasattr(self.obj90, '_setHierarchicalLink')):
      self.obj90._setHierarchicalLink(False)

    self.obj90.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1692.00574898,186.03909536,self.obj90)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj90.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj90)
    self.globalAndLocalPostcondition(self.obj90, rootNode)
    self.obj90.postAction( rootNode.CREATE )

    self.obj91=contains(self)
    self.obj91.isGraphObjectVisual = True

    if(hasattr(self.obj91, '_setHierarchicalLink')):
      self.obj91._setHierarchicalLink(False)

    self.obj91.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1682.67160001,193.809895833,self.obj91)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj91.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj91)
    self.globalAndLocalPostcondition(self.obj91, rootNode)
    self.obj91.postAction( rootNode.CREATE )

    self.obj92=contains(self)
    self.obj92.isGraphObjectVisual = True

    if(hasattr(self.obj92, '_setHierarchicalLink')):
      self.obj92._setHierarchicalLink(False)

    self.obj92.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1606.17160001,272.309895833,self.obj92)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj92.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj92)
    self.globalAndLocalPostcondition(self.obj92, rootNode)
    self.obj92.postAction( rootNode.CREATE )

    self.obj93=Hyperedge(self)
    self.obj93.isGraphObjectVisual = True

    if(hasattr(self.obj93, '_setHierarchicalLink')):
      self.obj93._setHierarchicalLink(False)

    # name
    self.obj93.name.setValue('')
    self.obj93.name.setNone()

    # broadcast
    self.obj93.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj93.broadcast.setHeight(15)

    # guard
    self.obj93.guard.setValue('1')

    # trigger
    self.obj93.trigger.setValue('AFTER(1)')

    # action
    self.obj93.action.setValue('controller.increaseTimeByOne()\n')
    self.obj93.action.setHeight(15)

    # broadcast_to
    self.obj93.broadcast_to.setValue('')
    self.obj93.broadcast_to.setNone()

    # display
    self.obj93.display.setValue('tm(1s)')

    self.obj93.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(420.0,602.0,self.obj93)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj93.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj93)
    self.globalAndLocalPostcondition(self.obj93, rootNode)
    self.obj93.postAction( rootNode.CREATE )

    self.obj94=Hyperedge(self)
    self.obj94.isGraphObjectVisual = True

    if(hasattr(self.obj94, '_setHierarchicalLink')):
      self.obj94._setHierarchicalLink(False)

    # name
    self.obj94.name.setValue('')
    self.obj94.name.setNone()

    # broadcast
    self.obj94.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj94.broadcast.setHeight(15)

    # guard
    self.obj94.guard.setValue('1')

    # trigger
    self.obj94.trigger.setValue('AFTER(1)')

    # action
    self.obj94.action.setValue('controller.refreshTimeDisplay()\n')
    self.obj94.action.setHeight(15)

    # broadcast_to
    self.obj94.broadcast_to.setValue('')
    self.obj94.broadcast_to.setNone()

    # display
    self.obj94.display.setValue('tm(1s)')

    self.obj94.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(521.0,299.0,self.obj94)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 0.97
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj94.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj94)
    self.globalAndLocalPostcondition(self.obj94, rootNode)
    self.obj94.postAction( rootNode.CREATE )

    self.obj95=Hyperedge(self)
    self.obj95.isGraphObjectVisual = True

    if(hasattr(self.obj95, '_setHierarchicalLink')):
      self.obj95._setHierarchicalLink(False)

    # name
    self.obj95.name.setValue('')
    self.obj95.name.setNone()

    # broadcast
    self.obj95.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj95.broadcast.setHeight(15)

    # guard
    self.obj95.guard.setValue('1')

    # trigger
    self.obj95.trigger.setValue('topRightPressed')

    # action
    self.obj95.action.setValue('controller.setIndiglo()\n')
    self.obj95.action.setHeight(15)

    # broadcast_to
    self.obj95.broadcast_to.setValue('')
    self.obj95.broadcast_to.setNone()

    # display
    self.obj95.display.setValue('topRightPressed/lighton')

    self.obj95.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(501.00160692,422.021604248,self.obj95)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj95.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj95)
    self.globalAndLocalPostcondition(self.obj95, rootNode)
    self.obj95.postAction( rootNode.CREATE )

    self.obj96=Hyperedge(self)
    self.obj96.isGraphObjectVisual = True

    if(hasattr(self.obj96, '_setHierarchicalLink')):
      self.obj96._setHierarchicalLink(False)

    # name
    self.obj96.name.setValue('')
    self.obj96.name.setNone()

    # broadcast
    self.obj96.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj96.broadcast.setHeight(15)

    # guard
    self.obj96.guard.setValue('1')

    # trigger
    self.obj96.trigger.setValue('topRightReleased')

    # action
    self.obj96.action.setValue('\n')
    self.obj96.action.setHeight(15)

    # broadcast_to
    self.obj96.broadcast_to.setValue('')
    self.obj96.broadcast_to.setNone()

    # display
    self.obj96.display.setValue('topRightReleased')

    self.obj96.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(412.76366772,477.6808835,self.obj96)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj96.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj96)
    self.globalAndLocalPostcondition(self.obj96, rootNode)
    self.obj96.postAction( rootNode.CREATE )

    self.obj97=Hyperedge(self)
    self.obj97.isGraphObjectVisual = True

    if(hasattr(self.obj97, '_setHierarchicalLink')):
      self.obj97._setHierarchicalLink(False)

    # name
    self.obj97.name.setValue('')
    self.obj97.name.setNone()

    # broadcast
    self.obj97.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj97.broadcast.setHeight(15)

    # guard
    self.obj97.guard.setValue('1')

    # trigger
    self.obj97.trigger.setValue('AFTER(2)')

    # action
    self.obj97.action.setValue('controller.unsetIndiglo()\n')
    self.obj97.action.setHeight(15)

    # broadcast_to
    self.obj97.broadcast_to.setValue('')
    self.obj97.broadcast_to.setNone()

    # display
    self.obj97.display.setValue('tm(2s)')

    self.obj97.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(534.8442126,464.045289636,self.obj97)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj97.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj97)
    self.globalAndLocalPostcondition(self.obj97, rootNode)
    self.obj97.postAction( rootNode.CREATE )

    self.obj98=Hyperedge(self)
    self.obj98.isGraphObjectVisual = True

    if(hasattr(self.obj98, '_setHierarchicalLink')):
      self.obj98._setHierarchicalLink(False)

    # name
    self.obj98.name.setValue('')
    self.obj98.name.setNone()

    # broadcast
    self.obj98.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj98.broadcast.setHeight(15)

    # guard
    self.obj98.guard.setValue('1')

    # trigger
    self.obj98.trigger.setValue('topLeftPressed')

    # action
    self.obj98.action.setValue('controller.refreshChronoDisplay()\n')
    self.obj98.action.setHeight(15)

    # broadcast_to
    self.obj98.broadcast_to.setValue('')
    self.obj98.broadcast_to.setNone()

    # display
    self.obj98.display.setValue('topLeftPressed/showChrono')

    self.obj98.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(400.529982213,202.43327707,self.obj98)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj98.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj98)
    self.globalAndLocalPostcondition(self.obj98, rootNode)
    self.obj98.postAction( rootNode.CREATE )

    self.obj99=Hyperedge(self)
    self.obj99.isGraphObjectVisual = True

    if(hasattr(self.obj99, '_setHierarchicalLink')):
      self.obj99._setHierarchicalLink(False)

    # name
    self.obj99.name.setValue('')
    self.obj99.name.setNone()

    # broadcast
    self.obj99.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj99.broadcast.setHeight(15)

    # guard
    self.obj99.guard.setValue('1')

    # trigger
    self.obj99.trigger.setValue('topLeftPressed')

    # action
    self.obj99.action.setValue('controller.refreshTimeDisplay()\n')
    self.obj99.action.setHeight(15)

    # broadcast_to
    self.obj99.broadcast_to.setValue('')
    self.obj99.broadcast_to.setNone()

    # display
    self.obj99.display.setValue('topLeftPressed/showTime')

    self.obj99.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(468.494563329,174.621763677,self.obj99)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj99.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj99)
    self.globalAndLocalPostcondition(self.obj99, rootNode)
    self.obj99.postAction( rootNode.CREATE )

    self.obj100=Hyperedge(self)
    self.obj100.isGraphObjectVisual = True

    if(hasattr(self.obj100, '_setHierarchicalLink')):
      self.obj100._setHierarchicalLink(False)

    # name
    self.obj100.name.setValue('')
    self.obj100.name.setNone()

    # broadcast
    self.obj100.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj100.broadcast.setHeight(15)

    # guard
    self.obj100.guard.setValue('[INSTATE("DigitalWatch.Displayloop.Chrono", 0)]')

    # trigger
    self.obj100.trigger.setValue('bottomRightPressed')

    # action
    self.obj100.action.setValue('\n')
    self.obj100.action.setHeight(15)

    # broadcast_to
    self.obj100.broadcast_to.setValue('')
    self.obj100.broadcast_to.setNone()

    # display
    self.obj100.display.setValue('bottomRightPressed/startChrono')

    self.obj100.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(870.01594892,451.972404683,self.obj100)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj100.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj100)
    self.globalAndLocalPostcondition(self.obj100, rootNode)
    self.obj100.postAction( rootNode.CREATE )

    self.obj101=Hyperedge(self)
    self.obj101.isGraphObjectVisual = True

    if(hasattr(self.obj101, '_setHierarchicalLink')):
      self.obj101._setHierarchicalLink(False)

    # name
    self.obj101.name.setValue('')
    self.obj101.name.setNone()

    # broadcast
    self.obj101.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj101.broadcast.setHeight(15)

    # guard
    self.obj101.guard.setValue('1')

    # trigger
    self.obj101.trigger.setValue('AFTER(0.01)')

    # action
    self.obj101.action.setValue('controller.increaseChronoByOne()\n')
    self.obj101.action.setHeight(15)

    # broadcast_to
    self.obj101.broadcast_to.setValue('')
    self.obj101.broadcast_to.setNone()

    # display
    self.obj101.display.setValue('tm(0.01s)')

    self.obj101.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(983.0,490.0,self.obj101)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj101.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj101)
    self.globalAndLocalPostcondition(self.obj101, rootNode)
    self.obj101.postAction( rootNode.CREATE )

    self.obj102=Hyperedge(self)
    self.obj102.isGraphObjectVisual = True

    if(hasattr(self.obj102, '_setHierarchicalLink')):
      self.obj102._setHierarchicalLink(False)

    # name
    self.obj102.name.setValue('')
    self.obj102.name.setNone()

    # broadcast
    self.obj102.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj102.broadcast.setHeight(15)

    # guard
    self.obj102.guard.setValue('[INSTATE("DigitalWatch.Displayloop.Chrono", 0)]')

    # trigger
    self.obj102.trigger.setValue('bottomLeftPressed')

    # action
    self.obj102.action.setValue('controller.resetChrono()\ncontroller.refreshChronoDisplay()\n')
    self.obj102.action.setHeight(15)

    # broadcast_to
    self.obj102.broadcast_to.setValue('')
    self.obj102.broadcast_to.setNone()

    # display
    self.obj102.display.setValue('bottomLeftPressed/reset')

    self.obj102.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1083.0,455.0,self.obj102)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj102.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj102)
    self.globalAndLocalPostcondition(self.obj102, rootNode)
    self.obj102.postAction( rootNode.CREATE )

    self.obj103=Hyperedge(self)
    self.obj103.isGraphObjectVisual = True

    if(hasattr(self.obj103, '_setHierarchicalLink')):
      self.obj103._setHierarchicalLink(False)

    # name
    self.obj103.name.setValue('')
    self.obj103.name.setNone()

    # broadcast
    self.obj103.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj103.broadcast.setHeight(15)

    # guard
    self.obj103.guard.setValue('1')

    # trigger
    self.obj103.trigger.setValue('bottomRightPressed')

    # action
    self.obj103.action.setValue('\n')
    self.obj103.action.setHeight(15)

    # broadcast_to
    self.obj103.broadcast_to.setValue('')
    self.obj103.broadcast_to.setNone()

    # display
    self.obj103.display.setValue('bottomRightPressed/pauseChrono')

    self.obj103.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(876.99746,406.07080641,self.obj103)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj103.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj103)
    self.globalAndLocalPostcondition(self.obj103, rootNode)
    self.obj103.postAction( rootNode.CREATE )

    self.obj104=Hyperedge(self)
    self.obj104.isGraphObjectVisual = True

    if(hasattr(self.obj104, '_setHierarchicalLink')):
      self.obj104._setHierarchicalLink(False)

    # name
    self.obj104.name.setValue('')
    self.obj104.name.setNone()

    # broadcast
    self.obj104.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj104.broadcast.setHeight(15)

    # guard
    self.obj104.guard.setValue('[INSTATE("DigitalWatch.Displayloop.Chrono", 0)]')

    # trigger
    self.obj104.trigger.setValue('bottomLeftPressed')

    # action
    self.obj104.action.setValue('controller.resetChrono()\ncontroller.refreshChronoDisplay()\n')
    self.obj104.action.setHeight(15)

    # broadcast_to
    self.obj104.broadcast_to.setValue('')
    self.obj104.broadcast_to.setNone()

    # display
    self.obj104.display.setValue('bottomLeftPressed/resetChrono')

    self.obj104.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(763.0,470.0,self.obj104)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj104.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj104)
    self.globalAndLocalPostcondition(self.obj104, rootNode)
    self.obj104.postAction( rootNode.CREATE )

    self.obj105=Hyperedge(self)
    self.obj105.isGraphObjectVisual = True

    if(hasattr(self.obj105, '_setHierarchicalLink')):
      self.obj105._setHierarchicalLink(False)

    # name
    self.obj105.name.setValue('')
    self.obj105.name.setNone()

    # broadcast
    self.obj105.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj105.broadcast.setHeight(15)

    # guard
    self.obj105.guard.setValue('1')

    # trigger
    self.obj105.trigger.setValue('AFTER(0.01)')

    # action
    self.obj105.action.setValue('controller.refreshChronoDisplay()\n')
    self.obj105.action.setHeight(15)

    # broadcast_to
    self.obj105.broadcast_to.setValue('')
    self.obj105.broadcast_to.setNone()

    # display
    self.obj105.display.setValue('refreshChronoDisplay')

    self.obj105.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(422.0,92.0,self.obj105)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj105.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj105)
    self.globalAndLocalPostcondition(self.obj105, rootNode)
    self.obj105.postAction( rootNode.CREATE )

    self.obj106=Hyperedge(self)
    self.obj106.isGraphObjectVisual = True

    if(hasattr(self.obj106, '_setHierarchicalLink')):
      self.obj106._setHierarchicalLink(False)

    # name
    self.obj106.name.setValue('')
    self.obj106.name.setNone()

    # broadcast
    self.obj106.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj106.broadcast.setHeight(15)

    # guard
    self.obj106.guard.setValue('1')

    # trigger
    self.obj106.trigger.setValue('bottomRightPressed')

    # action
    self.obj106.action.setValue('\n')
    self.obj106.action.setHeight(15)

    # broadcast_to
    self.obj106.broadcast_to.setValue('')
    self.obj106.broadcast_to.setNone()

    # display
    self.obj106.display.setValue('bottomRightPressed/EnterTimeEdit')

    self.obj106.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(640.191269441,191.778068391,self.obj106)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj106.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj106)
    self.globalAndLocalPostcondition(self.obj106, rootNode)
    self.obj106.postAction( rootNode.CREATE )

    self.obj107=Hyperedge(self)
    self.obj107.isGraphObjectVisual = True

    if(hasattr(self.obj107, '_setHierarchicalLink')):
      self.obj107._setHierarchicalLink(False)

    # name
    self.obj107.name.setValue('')
    self.obj107.name.setNone()

    # broadcast
    self.obj107.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj107.broadcast.setHeight(15)

    # guard
    self.obj107.guard.setValue('1')

    # trigger
    self.obj107.trigger.setValue('bottomRightReleased')

    # action
    self.obj107.action.setValue('\n')
    self.obj107.action.setHeight(15)

    # broadcast_to
    self.obj107.broadcast_to.setValue('')
    self.obj107.broadcast_to.setNone()

    # display
    self.obj107.display.setValue('bottomRightReleased/Time')

    self.obj107.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(616.708276102,162.151972356,self.obj107)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj107.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj107)
    self.globalAndLocalPostcondition(self.obj107, rootNode)
    self.obj107.postAction( rootNode.CREATE )

    self.obj108=Hyperedge(self)
    self.obj108.isGraphObjectVisual = True

    if(hasattr(self.obj108, '_setHierarchicalLink')):
      self.obj108._setHierarchicalLink(False)

    # name
    self.obj108.name.setValue('')
    self.obj108.name.setNone()

    # broadcast
    self.obj108.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj108.broadcast.setHeight(15)

    # guard
    self.obj108.guard.setValue('1')

    # trigger
    self.obj108.trigger.setValue('bottomLeftPressed')

    # action
    self.obj108.action.setValue('controller.setAlarm()\n[EVENT("setAlarm")]\n')
    self.obj108.action.setHeight(15)

    # broadcast_to
    self.obj108.broadcast_to.setValue('')
    self.obj108.broadcast_to.setNone()

    # display
    self.obj108.display.setValue('bottomLeftPressed/toggleAlarm')

    self.obj108.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(644.046954461,278.658285288,self.obj108)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj108.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj108)
    self.globalAndLocalPostcondition(self.obj108, rootNode)
    self.obj108.postAction( rootNode.CREATE )

    self.obj109=Hyperedge(self)
    self.obj109.isGraphObjectVisual = True

    if(hasattr(self.obj109, '_setHierarchicalLink')):
      self.obj109._setHierarchicalLink(False)

    # name
    self.obj109.name.setValue('')
    self.obj109.name.setNone()

    # broadcast
    self.obj109.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj109.broadcast.setHeight(15)

    # guard
    self.obj109.guard.setValue('1')

    # trigger
    self.obj109.trigger.setValue('bottomLeftReleased')

    # action
    self.obj109.action.setValue('\n')
    self.obj109.action.setHeight(15)

    # broadcast_to
    self.obj109.broadcast_to.setValue('')
    self.obj109.broadcast_to.setNone()

    # display
    self.obj109.display.setValue('bottomLeftReleased/Time')

    self.obj109.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(614.673759346,312.697606353,self.obj109)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 1.34
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj109.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj109)
    self.globalAndLocalPostcondition(self.obj109, rootNode)
    self.obj109.postAction( rootNode.CREATE )

    self.obj110=Hyperedge(self)
    self.obj110.isGraphObjectVisual = True

    if(hasattr(self.obj110, '_setHierarchicalLink')):
      self.obj110._setHierarchicalLink(False)

    # name
    self.obj110.name.setValue('')
    self.obj110.name.setNone()

    # broadcast
    self.obj110.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj110.broadcast.setHeight(15)

    # guard
    self.obj110.guard.setValue('1')

    # trigger
    self.obj110.trigger.setValue('AFTER(0.3)')

    # action
    self.obj110.action.setValue('\ncontroller.increaseSelection()\n')
    self.obj110.action.setHeight(15)

    # broadcast_to
    self.obj110.broadcast_to.setValue('')
    self.obj110.broadcast_to.setNone()

    # display
    self.obj110.display.setValue('tm(0.3s)')

    self.obj110.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1017.0,112.5,self.obj110)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj110.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj110)
    self.globalAndLocalPostcondition(self.obj110, rootNode)
    self.obj110.postAction( rootNode.CREATE )

    self.obj111=Hyperedge(self)
    self.obj111.isGraphObjectVisual = True

    if(hasattr(self.obj111, '_setHierarchicalLink')):
      self.obj111._setHierarchicalLink(False)

    # name
    self.obj111.name.setValue('')
    self.obj111.name.setNone()

    # broadcast
    self.obj111.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj111.broadcast.setHeight(15)

    # guard
    self.obj111.guard.setValue('1')

    # trigger
    self.obj111.trigger.setValue('AFTER(1.5)')

    # action
    self.obj111.action.setValue('\ncontroller.startSelection()\n')
    self.obj111.action.setHeight(15)

    # broadcast_to
    self.obj111.broadcast_to.setValue('')
    self.obj111.broadcast_to.setNone()

    # display
    self.obj111.display.setValue('tm(1.5s)')

    self.obj111.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(779.839814604,153.222206491,self.obj111)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj111.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj111)
    self.globalAndLocalPostcondition(self.obj111, rootNode)
    self.obj111.postAction( rootNode.CREATE )

    self.obj112=Hyperedge(self)
    self.obj112.isGraphObjectVisual = True

    if(hasattr(self.obj112, '_setHierarchicalLink')):
      self.obj112._setHierarchicalLink(False)

    # name
    self.obj112.name.setValue('')
    self.obj112.name.setNone()

    # broadcast
    self.obj112.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj112.broadcast.setHeight(15)

    # guard
    self.obj112.guard.setValue('1')

    # trigger
    self.obj112.trigger.setValue('AFTER(1.5)')

    # action
    self.obj112.action.setValue('\ncontroller.refreshAlarmDisplay()\ncontroller.startSelection()\n')
    self.obj112.action.setHeight(15)

    # broadcast_to
    self.obj112.broadcast_to.setValue('')
    self.obj112.broadcast_to.setNone()

    # display
    self.obj112.display.setValue('tm(1.5s)')

    self.obj112.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(763.427438792,265.376052965,self.obj112)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj112.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj112)
    self.globalAndLocalPostcondition(self.obj112, rootNode)
    self.obj112.postAction( rootNode.CREATE )

    self.obj113=Hyperedge(self)
    self.obj113.isGraphObjectVisual = True

    if(hasattr(self.obj113, '_setHierarchicalLink')):
      self.obj113._setHierarchicalLink(False)

    # name
    self.obj113.name.setValue('')
    self.obj113.name.setNone()

    # broadcast
    self.obj113.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj113.broadcast.setHeight(15)

    # guard
    self.obj113.guard.setValue('1')

    # trigger
    self.obj113.trigger.setValue('bottomLeftPressed')

    # action
    self.obj113.action.setValue('\n\ncontroller.increaseSelection()\n')
    self.obj113.action.setHeight(15)

    # broadcast_to
    self.obj113.broadcast_to.setValue('')
    self.obj113.broadcast_to.setNone()

    # display
    self.obj113.display.setValue('bottomLeftPressed')

    self.obj113.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(903.622906591,164.601116864,self.obj113)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj113.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj113)
    self.globalAndLocalPostcondition(self.obj113, rootNode)
    self.obj113.postAction( rootNode.CREATE )

    self.obj114=Hyperedge(self)
    self.obj114.isGraphObjectVisual = True

    if(hasattr(self.obj114, '_setHierarchicalLink')):
      self.obj114._setHierarchicalLink(False)

    # name
    self.obj114.name.setValue('')
    self.obj114.name.setNone()

    # broadcast
    self.obj114.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj114.broadcast.setHeight(15)

    # guard
    self.obj114.guard.setValue('1')

    # trigger
    self.obj114.trigger.setValue('bottomLeftReleased')

    # action
    self.obj114.action.setValue('\n')
    self.obj114.action.setHeight(15)

    # broadcast_to
    self.obj114.broadcast_to.setValue('')
    self.obj114.broadcast_to.setNone()

    # display
    self.obj114.display.setValue('bottomLeftReleased')

    self.obj114.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(925.245311897,200.600067363,self.obj114)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj114.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj114)
    self.globalAndLocalPostcondition(self.obj114, rootNode)
    self.obj114.postAction( rootNode.CREATE )

    self.obj115=Hyperedge(self)
    self.obj115.isGraphObjectVisual = True

    if(hasattr(self.obj115, '_setHierarchicalLink')):
      self.obj115._setHierarchicalLink(False)

    # name
    self.obj115.name.setValue('')
    self.obj115.name.setNone()

    # broadcast
    self.obj115.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj115.broadcast.setHeight(15)

    # guard
    self.obj115.guard.setValue('1')

    # trigger
    self.obj115.trigger.setValue('bottomRightPressed')

    # action
    self.obj115.action.setValue('\n')
    self.obj115.action.setHeight(15)

    # broadcast_to
    self.obj115.broadcast_to.setValue('')
    self.obj115.broadcast_to.setNone()

    # display
    self.obj115.display.setValue('bottomRightPressed')

    self.obj115.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(911.204460874,272.62835995,self.obj115)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj115.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj115)
    self.globalAndLocalPostcondition(self.obj115, rootNode)
    self.obj115.postAction( rootNode.CREATE )

    self.obj116=Hyperedge(self)
    self.obj116.isGraphObjectVisual = True

    if(hasattr(self.obj116, '_setHierarchicalLink')):
      self.obj116._setHierarchicalLink(False)

    # name
    self.obj116.name.setValue('')
    self.obj116.name.setNone()

    # broadcast
    self.obj116.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj116.broadcast.setHeight(15)

    # guard
    self.obj116.guard.setValue('1')

    # trigger
    self.obj116.trigger.setValue('bottomRightReleased')

    # action
    self.obj116.action.setValue('\ncontroller.selectNext()\n')
    self.obj116.action.setHeight(15)

    # broadcast_to
    self.obj116.broadcast_to.setValue('')
    self.obj116.broadcast_to.setNone()

    # display
    self.obj116.display.setValue('bottomRightReleased')

    self.obj116.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(937.33627774,246.89620301,self.obj116)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 0.89
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj116.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj116)
    self.globalAndLocalPostcondition(self.obj116, rootNode)
    self.obj116.postAction( rootNode.CREATE )

    self.obj117=Hyperedge(self)
    self.obj117.isGraphObjectVisual = True

    if(hasattr(self.obj117, '_setHierarchicalLink')):
      self.obj117._setHierarchicalLink(False)

    # name
    self.obj117.name.setValue('')
    self.obj117.name.setNone()

    # broadcast
    self.obj117.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj117.broadcast.setHeight(15)

    # guard
    self.obj117.guard.setValue('1')

    # trigger
    self.obj117.trigger.setValue('AFTER(5)')

    # action
    self.obj117.action.setValue('\n\ncontroller.stopSelection()\n')
    self.obj117.action.setHeight(15)

    # broadcast_to
    self.obj117.broadcast_to.setValue('')
    self.obj117.broadcast_to.setNone()

    # display
    self.obj117.display.setValue('tm(5s)')

    self.obj117.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(671.006704458,232.021605547,self.obj117)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj117.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj117)
    self.globalAndLocalPostcondition(self.obj117, rootNode)
    self.obj117.postAction( rootNode.CREATE )

    self.obj118=Hyperedge(self)
    self.obj118.isGraphObjectVisual = True

    if(hasattr(self.obj118, '_setHierarchicalLink')):
      self.obj118._setHierarchicalLink(False)

    # name
    self.obj118.name.setValue('')
    self.obj118.name.setNone()

    # broadcast
    self.obj118.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj118.broadcast.setHeight(15)

    # guard
    self.obj118.guard.setValue('1')

    # trigger
    self.obj118.trigger.setValue('AFTER(2)')

    # action
    self.obj118.action.setValue('\n\ncontroller.stopSelection()\n')
    self.obj118.action.setHeight(15)

    # broadcast_to
    self.obj118.broadcast_to.setValue('')
    self.obj118.broadcast_to.setNone()

    # display
    self.obj118.display.setValue('tm(2s)')

    self.obj118.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(770.432506722,282.006873775,self.obj118)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj118.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj118)
    self.globalAndLocalPostcondition(self.obj118, rootNode)
    self.obj118.postAction( rootNode.CREATE )

    self.obj119=Hyperedge(self)
    self.obj119.isGraphObjectVisual = True

    if(hasattr(self.obj119, '_setHierarchicalLink')):
      self.obj119._setHierarchicalLink(False)

    # name
    self.obj119.name.setValue('')
    self.obj119.name.setNone()

    # broadcast
    self.obj119.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj119.broadcast.setHeight(15)

    # guard
    self.obj119.guard.setValue('[INSTATE("DigitalWatch.AlarmState.AlarmOn", 0)]')

    # trigger
    self.obj119.trigger.setValue('AFTER(1)')

    # action
    self.obj119.action.setValue('\ncontroller.checkTime()\n')
    self.obj119.action.setHeight(15)

    # broadcast_to
    self.obj119.broadcast_to.setValue('')
    self.obj119.broadcast_to.setNone()

    # display
    self.obj119.display.setValue('tm(1s)/checkTime')

    self.obj119.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1240.0,588.5,self.obj119)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj119.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj119)
    self.globalAndLocalPostcondition(self.obj119, rootNode)
    self.obj119.postAction( rootNode.CREATE )

    self.obj120=Hyperedge(self)
    self.obj120.isGraphObjectVisual = True

    if(hasattr(self.obj120, '_setHierarchicalLink')):
      self.obj120._setHierarchicalLink(False)

    # name
    self.obj120.name.setValue('')
    self.obj120.name.setNone()

    # broadcast
    self.obj120.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj120.broadcast.setHeight(15)

    # guard
    self.obj120.guard.setValue('1')

    # trigger
    self.obj120.trigger.setValue('alarmStart')

    # action
    self.obj120.action.setValue('\ncontroller.setIndiglo()\n')
    self.obj120.action.setHeight(15)

    # broadcast_to
    self.obj120.broadcast_to.setValue('')
    self.obj120.broadcast_to.setNone()

    # display
    self.obj120.display.setValue('alarmStart')

    self.obj120.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1319.00335223,643.510802773,self.obj120)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj120.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj120)
    self.globalAndLocalPostcondition(self.obj120, rootNode)
    self.obj120.postAction( rootNode.CREATE )

    self.obj121=Hyperedge(self)
    self.obj121.isGraphObjectVisual = True

    if(hasattr(self.obj121, '_setHierarchicalLink')):
      self.obj121._setHierarchicalLink(False)

    # name
    self.obj121.name.setValue('')
    self.obj121.name.setNone()

    # broadcast
    self.obj121.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj121.broadcast.setHeight(15)

    # guard
    self.obj121.guard.setValue('1')

    # trigger
    self.obj121.trigger.setValue('setAlarm')

    # action
    self.obj121.action.setValue('\n')
    self.obj121.action.setHeight(15)

    # broadcast_to
    self.obj121.broadcast_to.setValue('')
    self.obj121.broadcast_to.setNone()

    # display
    self.obj121.display.setValue('setAlarm')

    self.obj121.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(831.065852229,574.778645833,self.obj121)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj121.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj121)
    self.globalAndLocalPostcondition(self.obj121, rootNode)
    self.obj121.postAction( rootNode.CREATE )

    self.obj122=Hyperedge(self)
    self.obj122.isGraphObjectVisual = True

    if(hasattr(self.obj122, '_setHierarchicalLink')):
      self.obj122._setHierarchicalLink(False)

    # name
    self.obj122.name.setValue('')
    self.obj122.name.setNone()

    # broadcast
    self.obj122.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj122.broadcast.setHeight(15)

    # guard
    self.obj122.guard.setValue('1')

    # trigger
    self.obj122.trigger.setValue('setAlarm')

    # action
    self.obj122.action.setValue('\n')
    self.obj122.action.setHeight(15)

    # broadcast_to
    self.obj122.broadcast_to.setValue('')
    self.obj122.broadcast_to.setNone()

    # display
    self.obj122.display.setValue('setAlarm')

    self.obj122.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(824.013689867,615.021607987,self.obj122)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj122.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj122)
    self.globalAndLocalPostcondition(self.obj122, rootNode)
    self.obj122.postAction( rootNode.CREATE )

    self.obj123=Hyperedge(self)
    self.obj123.isGraphObjectVisual = True

    if(hasattr(self.obj123, '_setHierarchicalLink')):
      self.obj123._setHierarchicalLink(False)

    # name
    self.obj123.name.setValue('')
    self.obj123.name.setNone()

    # broadcast
    self.obj123.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj123.broadcast.setHeight(15)

    # guard
    self.obj123.guard.setValue('1')

    # trigger
    self.obj123.trigger.setValue('AFTER(0.5)')

    # action
    self.obj123.action.setValue('\n\ncontroller.unsetIndiglo()\n')
    self.obj123.action.setHeight(15)

    # broadcast_to
    self.obj123.broadcast_to.setValue('')
    self.obj123.broadcast_to.setNone()

    # display
    self.obj123.display.setValue('tm(0.5s)/unset')

    self.obj123.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1469.56585223,638.278645833,self.obj123)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj123.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj123)
    self.globalAndLocalPostcondition(self.obj123, rootNode)
    self.obj123.postAction( rootNode.CREATE )

    self.obj124=Hyperedge(self)
    self.obj124.isGraphObjectVisual = True

    if(hasattr(self.obj124, '_setHierarchicalLink')):
      self.obj124._setHierarchicalLink(False)

    # name
    self.obj124.name.setValue('')
    self.obj124.name.setNone()

    # broadcast
    self.obj124.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj124.broadcast.setHeight(15)

    # guard
    self.obj124.guard.setValue('1')

    # trigger
    self.obj124.trigger.setValue('AFTER(0.5)')

    # action
    self.obj124.action.setValue('\ncontroller.setIndiglo()\n')
    self.obj124.action.setHeight(15)

    # broadcast_to
    self.obj124.broadcast_to.setValue('')
    self.obj124.broadcast_to.setNone()

    # display
    self.obj124.display.setValue('tm(0.5s)/set')

    self.obj124.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1467.84309461,666.580548436,self.obj124)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj124.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj124)
    self.globalAndLocalPostcondition(self.obj124, rootNode)
    self.obj124.postAction( rootNode.CREATE )

    self.obj125=Hyperedge(self)
    self.obj125.isGraphObjectVisual = True

    if(hasattr(self.obj125, '_setHierarchicalLink')):
      self.obj125._setHierarchicalLink(False)

    # name
    self.obj125.name.setValue('')
    self.obj125.name.setNone()

    # broadcast
    self.obj125.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj125.broadcast.setHeight(15)

    # guard
    self.obj125.guard.setValue('1')

    # trigger
    self.obj125.trigger.setValue('alarmStart')

    # action
    self.obj125.action.setValue('\n')
    self.obj125.action.setHeight(15)

    # broadcast_to
    self.obj125.broadcast_to.setValue('')
    self.obj125.broadcast_to.setNone()

    # display
    self.obj125.display.setValue('alarmStart')

    self.obj125.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(994.56585223,575.278645833,self.obj125)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj125.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj125)
    self.globalAndLocalPostcondition(self.obj125, rootNode)
    self.obj125.postAction( rootNode.CREATE )

    self.obj126=Hyperedge(self)
    self.obj126.isGraphObjectVisual = True

    if(hasattr(self.obj126, '_setHierarchicalLink')):
      self.obj126._setHierarchicalLink(False)

    # name
    self.obj126.name.setValue('')
    self.obj126.name.setNone()

    # broadcast
    self.obj126.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj126.broadcast.setHeight(15)

    # guard
    self.obj126.guard.setValue('1')

    # trigger
    self.obj126.trigger.setValue('AFTER(4)')

    # action
    self.obj126.action.setValue('\n[EVENT("alarmStop")]\ncontroller.setAlarm()\n')
    self.obj126.action.setHeight(15)

    # broadcast_to
    self.obj126.broadcast_to.setValue('')
    self.obj126.broadcast_to.setNone()

    # display
    self.obj126.display.setValue('tm(4s)/alarmStop')

    self.obj126.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(908.446420542,635.71762454,self.obj126)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj126.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj126)
    self.globalAndLocalPostcondition(self.obj126, rootNode)
    self.obj126.postAction( rootNode.CREATE )

    self.obj127=Hyperedge(self)
    self.obj127.isGraphObjectVisual = True

    if(hasattr(self.obj127, '_setHierarchicalLink')):
      self.obj127._setHierarchicalLink(False)

    # name
    self.obj127.name.setValue('')
    self.obj127.name.setNone()

    # broadcast
    self.obj127.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj127.broadcast.setHeight(15)

    # guard
    self.obj127.guard.setValue('1')

    # trigger
    self.obj127.trigger.setValue('alarmStop')

    # action
    self.obj127.action.setValue('\ncontroller.unsetIndiglo()\n')
    self.obj127.action.setHeight(15)

    # broadcast_to
    self.obj127.broadcast_to.setValue('')
    self.obj127.broadcast_to.setNone()

    # display
    self.obj127.display.setValue('alarmStop/unset')

    self.obj127.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1319.44642054,682.71762454,self.obj127)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj127.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj127)
    self.globalAndLocalPostcondition(self.obj127, rootNode)
    self.obj127.postAction( rootNode.CREATE )

    self.obj128=Hyperedge(self)
    self.obj128.isGraphObjectVisual = True

    if(hasattr(self.obj128, '_setHierarchicalLink')):
      self.obj128._setHierarchicalLink(False)

    # name
    self.obj128.name.setValue('')
    self.obj128.name.setNone()

    # broadcast
    self.obj128.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj128.broadcast.setHeight(15)

    # guard
    self.obj128.guard.setValue('1')

    # trigger
    self.obj128.trigger.setValue('alarmStop')

    # action
    self.obj128.action.setValue('\n')
    self.obj128.action.setHeight(15)

    # broadcast_to
    self.obj128.broadcast_to.setValue('')
    self.obj128.broadcast_to.setNone()

    # display
    self.obj128.display.setValue('alarmStop')

    self.obj128.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1385.94642054,707.21762454,self.obj128)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj128.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj128)
    self.globalAndLocalPostcondition(self.obj128, rootNode)
    self.obj128.postAction( rootNode.CREATE )

    self.obj129=Hyperedge(self)
    self.obj129.isGraphObjectVisual = True

    if(hasattr(self.obj129, '_setHierarchicalLink')):
      self.obj129._setHierarchicalLink(False)

    # name
    self.obj129.name.setValue('')
    self.obj129.name.setNone()

    # broadcast
    self.obj129.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj129.broadcast.setHeight(15)

    # guard
    self.obj129.guard.setValue('1')

    # trigger
    self.obj129.trigger.setValue('bottomLeftPressed')

    # action
    self.obj129.action.setValue('\ncontroller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj129.action.setHeight(15)

    # broadcast_to
    self.obj129.broadcast_to.setValue('')
    self.obj129.broadcast_to.setNone()

    # display
    self.obj129.display.setValue('bottomLeftPressed/alarmStop')

    self.obj129.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(914.0,663.0,self.obj129)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj129.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj129)
    self.globalAndLocalPostcondition(self.obj129, rootNode)
    self.obj129.postAction( rootNode.CREATE )

    self.obj130=Hyperedge(self)
    self.obj130.isGraphObjectVisual = True

    if(hasattr(self.obj130, '_setHierarchicalLink')):
      self.obj130._setHierarchicalLink(False)

    # name
    self.obj130.name.setValue('')
    self.obj130.name.setNone()

    # broadcast
    self.obj130.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj130.broadcast.setHeight(15)

    # guard
    self.obj130.guard.setValue('1')

    # trigger
    self.obj130.trigger.setValue('bottomRightPressed')

    # action
    self.obj130.action.setValue('controller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj130.action.setHeight(15)

    # broadcast_to
    self.obj130.broadcast_to.setValue('')
    self.obj130.broadcast_to.setNone()

    # display
    self.obj130.display.setValue('bottomRightPressed/alarmStop')

    self.obj130.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(923.5,693.5,self.obj130)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj130.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj130)
    self.globalAndLocalPostcondition(self.obj130, rootNode)
    self.obj130.postAction( rootNode.CREATE )

    self.obj131=Hyperedge(self)
    self.obj131.isGraphObjectVisual = True

    if(hasattr(self.obj131, '_setHierarchicalLink')):
      self.obj131._setHierarchicalLink(False)

    # name
    self.obj131.name.setValue('')
    self.obj131.name.setNone()

    # broadcast
    self.obj131.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj131.broadcast.setHeight(15)

    # guard
    self.obj131.guard.setValue('1')

    # trigger
    self.obj131.trigger.setValue('topLeftPressed')

    # action
    self.obj131.action.setValue('controller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj131.action.setHeight(15)

    # broadcast_to
    self.obj131.broadcast_to.setValue('')
    self.obj131.broadcast_to.setNone()

    # display
    self.obj131.display.setValue('topLeftPressed/alarmStop')

    self.obj131.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(929.5,727.5,self.obj131)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj131.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj131)
    self.globalAndLocalPostcondition(self.obj131, rootNode)
    self.obj131.postAction( rootNode.CREATE )

    self.obj132=Hyperedge(self)
    self.obj132.isGraphObjectVisual = True

    if(hasattr(self.obj132, '_setHierarchicalLink')):
      self.obj132._setHierarchicalLink(False)

    # name
    self.obj132.name.setValue('')
    self.obj132.name.setNone()

    # broadcast
    self.obj132.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj132.broadcast.setHeight(15)

    # guard
    self.obj132.guard.setValue('1')

    # trigger
    self.obj132.trigger.setValue('topRightPressed')

    # action
    self.obj132.action.setValue('controller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj132.action.setHeight(15)

    # broadcast_to
    self.obj132.broadcast_to.setValue('')
    self.obj132.broadcast_to.setNone()

    # display
    self.obj132.display.setValue('topRightPressed/alarmStop')

    self.obj132.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(926.0,762.5,self.obj132)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj132.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj132)
    self.globalAndLocalPostcondition(self.obj132, rootNode)
    self.obj132.postAction( rootNode.CREATE )

    self.obj133=Hyperedge(self)
    self.obj133.isGraphObjectVisual = True

    if(hasattr(self.obj133, '_setHierarchicalLink')):
      self.obj133._setHierarchicalLink(False)

    # name
    self.obj133.name.setValue('')
    self.obj133.name.setNone()

    # broadcast
    self.obj133.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj133.broadcast.setHeight(15)

    # guard
    self.obj133.guard.setValue('1')

    # trigger
    self.obj133.trigger.setValue('start')

    # action
    self.obj133.action.setValue('\ncontroller=[PARAMS]\n')
    self.obj133.action.setHeight(15)

    # broadcast_to
    self.obj133.broadcast_to.setValue('')
    self.obj133.broadcast_to.setNone()

    # display
    self.obj133.display.setValue('start')

    self.obj133.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(207.000955474,428.982510187,self.obj133)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj133.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj133)
    self.globalAndLocalPostcondition(self.obj133, rootNode)
    self.obj133.postAction( rootNode.CREATE )

    self.obj134=Hyperedge(self)
    self.obj134.isGraphObjectVisual = True

    if(hasattr(self.obj134, '_setHierarchicalLink')):
      self.obj134._setHierarchicalLink(False)

    # name
    self.obj134.name.setValue('')
    self.obj134.name.setNone()

    # broadcast
    self.obj134.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj134.broadcast.setHeight(15)

    # guard
    self.obj134.guard.setValue('1')

    # trigger
    self.obj134.trigger.setValue('stop')

    # action
    self.obj134.action.setValue('\n')
    self.obj134.action.setHeight(15)

    # broadcast_to
    self.obj134.broadcast_to.setValue('')
    self.obj134.broadcast_to.setNone()

    # display
    self.obj134.display.setValue('stop')

    self.obj134.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1696.50574898,429.03909536,self.obj134)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj134.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj134)
    self.globalAndLocalPostcondition(self.obj134, rootNode)
    self.obj134.postAction( rootNode.CREATE )

    self.obj135=Hyperedge(self)
    self.obj135.isGraphObjectVisual = True

    if(hasattr(self.obj135, '_setHierarchicalLink')):
      self.obj135._setHierarchicalLink(False)

    # name
    self.obj135.name.setValue('')
    self.obj135.name.setNone()

    # broadcast
    self.obj135.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj135.broadcast.setHeight(15)

    # guard
    self.obj135.guard.setValue('1')

    # trigger
    self.obj135.trigger.setValue('AFTER(1)')

    # action
    self.obj135.action.setValue('[EVENT("bottomRightPressed")]\n')
    self.obj135.action.setHeight(15)

    # broadcast_to
    self.obj135.broadcast_to.setValue('')
    self.obj135.broadcast_to.setNone()

    # display
    self.obj135.display.setValue('tm(1s)/bottomRightPressed')

    self.obj135.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1423.00670446,421.021605547,self.obj135)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj135.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj135)
    self.globalAndLocalPostcondition(self.obj135, rootNode)
    self.obj135.postAction( rootNode.CREATE )

    self.obj136=Hyperedge(self)
    self.obj136.isGraphObjectVisual = True

    if(hasattr(self.obj136, '_setHierarchicalLink')):
      self.obj136._setHierarchicalLink(False)

    # name
    self.obj136.name.setValue('')
    self.obj136.name.setNone()

    # broadcast
    self.obj136.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj136.broadcast.setHeight(15)

    # guard
    self.obj136.guard.setValue('1')

    # trigger
    self.obj136.trigger.setValue('AFTER(3)')

    # action
    self.obj136.action.setValue('[EVENT("bottomRightPressed")]\n')
    self.obj136.action.setHeight(15)

    # broadcast_to
    self.obj136.broadcast_to.setValue('')
    self.obj136.broadcast_to.setNone()

    # display
    self.obj136.display.setValue('tm(3s)/bottomRightPressed')

    self.obj136.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1451.00670446,319.99331296,self.obj136)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj136.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj136)
    self.globalAndLocalPostcondition(self.obj136, rootNode)
    self.obj136.postAction( rootNode.CREATE )

    self.obj137=Hyperedge(self)
    self.obj137.isGraphObjectVisual = True

    if(hasattr(self.obj137, '_setHierarchicalLink')):
      self.obj137._setHierarchicalLink(False)

    # name
    self.obj137.name.setValue('')
    self.obj137.name.setNone()

    # broadcast
    self.obj137.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj137.broadcast.setHeight(15)

    # guard
    self.obj137.guard.setValue('1')

    # trigger
    self.obj137.trigger.setValue('AFTER(2)')

    # action
    self.obj137.action.setValue('[EVENT("bottomRightPressed")]\n')
    self.obj137.action.setHeight(15)

    # broadcast_to
    self.obj137.broadcast_to.setValue('')
    self.obj137.broadcast_to.setNone()

    # display
    self.obj137.display.setValue('tm(2s)/bottomRightPressed')

    self.obj137.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1441.00670446,151.99331296,self.obj137)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj137.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj137)
    self.globalAndLocalPostcondition(self.obj137, rootNode)
    self.obj137.postAction( rootNode.CREATE )

    self.obj138=Hyperedge(self)
    self.obj138.isGraphObjectVisual = True

    if(hasattr(self.obj138, '_setHierarchicalLink')):
      self.obj138._setHierarchicalLink(False)

    # name
    self.obj138.name.setValue('')
    self.obj138.name.setNone()

    # broadcast
    self.obj138.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj138.broadcast.setHeight(15)

    # guard
    self.obj138.guard.setValue('1')

    # trigger
    self.obj138.trigger.setValue('AFTER(3)')

    # action
    self.obj138.action.setValue('\n\n[EVENT("topLeftPressed")]\n')
    self.obj138.action.setHeight(15)

    # broadcast_to
    self.obj138.broadcast_to.setValue('')
    self.obj138.broadcast_to.setNone()

    # display
    self.obj138.display.setValue('tm(3s)/topLeftPressed')

    self.obj138.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1361.00670446,112.021605547,self.obj138)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj138.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj138)
    self.globalAndLocalPostcondition(self.obj138, rootNode)
    self.obj138.postAction( rootNode.CREATE )

    self.obj139=Hyperedge(self)
    self.obj139.isGraphObjectVisual = True

    if(hasattr(self.obj139, '_setHierarchicalLink')):
      self.obj139._setHierarchicalLink(False)

    # name
    self.obj139.name.setValue('')
    self.obj139.name.setNone()

    # broadcast
    self.obj139.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj139.broadcast.setHeight(15)

    # guard
    self.obj139.guard.setValue('1')

    # trigger
    self.obj139.trigger.setValue('AFTER(2)')

    # action
    self.obj139.action.setValue('\n\n[EVENT("topLeftPressed")]\n')
    self.obj139.action.setHeight(15)

    # broadcast_to
    self.obj139.broadcast_to.setValue('')
    self.obj139.broadcast_to.setNone()

    # display
    self.obj139.display.setValue('tm(2s)/topLeftPressed')

    self.obj139.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1281.00670446,141.99331296,self.obj139)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj139.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj139)
    self.globalAndLocalPostcondition(self.obj139, rootNode)
    self.obj139.postAction( rootNode.CREATE )

    self.obj140=Hyperedge(self)
    self.obj140.isGraphObjectVisual = True

    if(hasattr(self.obj140, '_setHierarchicalLink')):
      self.obj140._setHierarchicalLink(False)

    # name
    self.obj140.name.setValue('')
    self.obj140.name.setNone()

    # broadcast
    self.obj140.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj140.broadcast.setHeight(15)

    # guard
    self.obj140.guard.setValue('1')

    # trigger
    self.obj140.trigger.setValue('AFTER(2)')

    # action
    self.obj140.action.setValue('\n[EVENT("bottomRightPressed")]\n[EVENT("bottomLeftPressed")]\n')
    self.obj140.action.setHeight(15)

    # broadcast_to
    self.obj140.broadcast_to.setValue('')
    self.obj140.broadcast_to.setNone()

    # display
    self.obj140.display.setValue('tm(2s)/bottomRightPressed')

    self.obj140.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1277.72969801,201.146997404,self.obj140)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj140.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj140)
    self.globalAndLocalPostcondition(self.obj140, rootNode)
    self.obj140.postAction( rootNode.CREATE )

    self.obj141=Hyperedge(self)
    self.obj141.isGraphObjectVisual = True

    if(hasattr(self.obj141, '_setHierarchicalLink')):
      self.obj141._setHierarchicalLink(False)

    # name
    self.obj141.name.setValue('')
    self.obj141.name.setNone()

    # broadcast
    self.obj141.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj141.broadcast.setHeight(15)

    # guard
    self.obj141.guard.setValue('1')

    # trigger
    self.obj141.trigger.setValue('AFTER(1)')

    # action
    self.obj141.action.setValue('\n\n[EVENT("topLeftPressed")]\n')
    self.obj141.action.setHeight(15)

    # broadcast_to
    self.obj141.broadcast_to.setValue('')
    self.obj141.broadcast_to.setNone()

    # display
    self.obj141.display.setValue('tm(1s)/topLeftPressed')

    self.obj141.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1261.63707999,403.803053022,self.obj141)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj141.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj141)
    self.globalAndLocalPostcondition(self.obj141, rootNode)
    self.obj141.postAction( rootNode.CREATE )

    self.obj142=Hyperedge(self)
    self.obj142.isGraphObjectVisual = True

    if(hasattr(self.obj142, '_setHierarchicalLink')):
      self.obj142._setHierarchicalLink(False)

    # name
    self.obj142.name.setValue('')
    self.obj142.name.setNone()

    # broadcast
    self.obj142.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj142.broadcast.setHeight(15)

    # guard
    self.obj142.guard.setValue('1')

    # trigger
    self.obj142.trigger.setValue('AFTER(1)')

    # action
    self.obj142.action.setValue('[EVENT("topLeftPressed")]\n')
    self.obj142.action.setHeight(15)

    # broadcast_to
    self.obj142.broadcast_to.setValue('')
    self.obj142.broadcast_to.setNone()

    # display
    self.obj142.display.setValue('tm(1s)/topLeftPressed')

    self.obj142.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1267.73994572,293.837622404,self.obj142)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj142.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj142)
    self.globalAndLocalPostcondition(self.obj142, rootNode)
    self.obj142.postAction( rootNode.CREATE )

    self.obj143=orthogonality(self)
    self.obj143.isGraphObjectVisual = True

    if(hasattr(self.obj143, '_setHierarchicalLink')):
      self.obj143._setHierarchicalLink(False)

    self.obj143.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(330.5,387.0,self.obj143)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj143.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj143)
    self.globalAndLocalPostcondition(self.obj143, rootNode)
    self.obj143.postAction( rootNode.CREATE )

    self.obj144=orthogonality(self)
    self.obj144.isGraphObjectVisual = True

    if(hasattr(self.obj144, '_setHierarchicalLink')):
      self.obj144._setHierarchicalLink(False)

    self.obj144.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(327.0,73.0,self.obj144)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj144.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj144)
    self.globalAndLocalPostcondition(self.obj144, rootNode)
    self.obj144.postAction( rootNode.CREATE )

    self.obj145=orthogonality(self)
    self.obj145.isGraphObjectVisual = True

    if(hasattr(self.obj145, '_setHierarchicalLink')):
      self.obj145._setHierarchicalLink(False)

    self.obj145.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(338.0,328.5,self.obj145)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj145.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj145)
    self.globalAndLocalPostcondition(self.obj145, rootNode)
    self.obj145.postAction( rootNode.CREATE )

    self.obj146=orthogonality(self)
    self.obj146.isGraphObjectVisual = True

    if(hasattr(self.obj146, '_setHierarchicalLink')):
      self.obj146._setHierarchicalLink(False)

    self.obj146.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(504.5,315.0,self.obj146)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj146.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj146)
    self.globalAndLocalPostcondition(self.obj146, rootNode)
    self.obj146.postAction( rootNode.CREATE )

    self.obj147=orthogonality(self)
    self.obj147.isGraphObjectVisual = True

    if(hasattr(self.obj147, '_setHierarchicalLink')):
      self.obj147._setHierarchicalLink(False)

    self.obj147.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(509.5,227.5,self.obj147)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj147.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj147)
    self.globalAndLocalPostcondition(self.obj147, rootNode)
    self.obj147.postAction( rootNode.CREATE )

    self.obj148=orthogonality(self)
    self.obj148.isGraphObjectVisual = True

    if(hasattr(self.obj148, '_setHierarchicalLink')):
      self.obj148._setHierarchicalLink(False)

    self.obj148.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(747.5,321.0,self.obj148)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj148.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj148)
    self.globalAndLocalPostcondition(self.obj148, rootNode)
    self.obj148.postAction( rootNode.CREATE )

    self.obj149=orthogonality(self)
    self.obj149.isGraphObjectVisual = True

    if(hasattr(self.obj149, '_setHierarchicalLink')):
      self.obj149._setHierarchicalLink(False)

    self.obj149.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(754.5,80.0,self.obj149)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj149.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj149)
    self.globalAndLocalPostcondition(self.obj149, rootNode)
    self.obj149.postAction( rootNode.CREATE )

    # Connections for obj28 (graphObject_: Obj0) named DigitalWatch
    self.drawConnections(
(self.obj28,self.obj143,[305.99999999999983, 425.9999999999998, 341.2500000000001, 434.74999999999983, 330.49999999999994, 386.99999999999994],"true", 3),
(self.obj28,self.obj144,[951.0, 62.999999999999986, 327.0000000000001, 73.0],"true", 2),
(self.obj28,self.obj145,[949.0, 64.0, 337.99999999999955, 328.5],"true", 2),
(self.obj28,self.obj146,[305.99999999999983, 425.9999999999998, 504.4999999999999, 314.9999999999999],"true", 2),
(self.obj28,self.obj147,[305.99999999999983, 425.9999999999998, 509.5000000000003, 227.5],"true", 2),
(self.obj28,self.obj148,[951.0, 62.999999999999986, 747.5000000000002, 321.0],"true", 2),
(self.obj28,self.obj134,[1594.999999999999, 425.9999999999998, 1696.5057489840433, 429.03909536014714],"true", 2),
(self.obj28,self.obj149,[305.99999999999983, 425.9999999999998, 754.4999999999999, 80.00000000000006],"true", 2) )
    # Connections for obj29 (graphObject_: Obj1) named Time
    self.drawConnections(
(self.obj29,self.obj94,[525.2765809560262, 259.88694466351205, 536.9999999999997, 298.9999999999998, 521.0000000000001, 298.9999999999998],"true", 3),
(self.obj29,self.obj98,[512.0114979680868, 252.0781907202945, 423.39782812085275, 228.50356492490153, 400.5299822133067, 202.43327707001237],"true", 3),
(self.obj29,self.obj106,[528.920208901211, 247.4947916666667, 599.1764875056645, 224.54295403753403, 640.191269441, 191.778068391],"true", 3),
(self.obj29,self.obj108,[530.0019109474379, 251.96502037329134, 598.2116321940366, 254.74459246508133, 644.0469544607556, 278.65828528842513],"true", 3) )
    # Connections for obj30 (graphObject_: Obj2) named TimeUpdate
    self.drawConnections(
(self.obj30,self.obj93,[416.7479645862102, 644.1680960830694, 404.99999999999886, 601.9999999999994, 419.99999999999886, 601.9999999999995],"true", 3) )
    # Connections for obj31 (graphObject_: Obj3) named LightOn
    self.drawConnections(
(self.obj31,self.obj96,[385.2765809560262, 419.88694466351194, 394.8958218086387, 451.6105956455795, 412.76366771618586, 477.68088350046924],"true", 3) )
    # Connections for obj32 (graphObject_: Obj4) named LightOff
    self.drawConnections(
(self.obj32,self.obj95,[612.0114979680864, 412.07819072029423, 556.5040036703452, 422.04989683429653, 501.00160691518363, 422.02160424754584],"true", 3) )
    # Connections for obj33 (graphObject_: Obj5) named Button released
    self.drawConnections(
(self.obj33,self.obj97,[468.9202089012112, 527.4947916666666, 498.82943066156474, 491.8101752824891, 534.8442125965189, 464.04528963580174],"true", 3) )
    # Connections for obj34 (graphObject_: Obj6) named Chrono
    self.drawConnections(
(self.obj34,self.obj99,[428.8063455279241, 136.43524907991585, 445.62671742138434, 148.55147582168055, 468.49456332893067, 174.6217636765701],"true", 3),
(self.obj34,self.obj105,[416.7479645862115, 124.16809608307034, 406.9999999999999, 91.99999999999994, 422.0, 92.00000000000001],"true", 3) )
    # Connections for obj35 (graphObject_: Obj7) named Chrono off
    self.drawConnections(
(self.obj35,self.obj100,[770.0019109474374, 431.96502037329117, 819.513552159402, 446.94411209647444, 870.0159489145641, 451.972404683225],"true", 3),
(self.obj35,self.obj104,[765.2765809560262, 439.886944663512, 777.9999999999995, 469.9999999999997, 762.9999999999995, 469.9999999999997],"true", 3) )
    # Connections for obj36 (graphObject_: Obj8) named Chrono on
    self.drawConnections(
(self.obj36,self.obj101,[985.2765809560265, 459.88694466351194, 998.0, 489.99999999999994, 982.9999999999998, 489.99999999999994],"true", 3),
(self.obj36,self.obj102,[988.8063455279239, 456.43524907991576, 1082.9999999999995, 510.0000000000002, 1082.9999999999995, 454.9999999999997],"true", 3),
(self.obj36,self.obj103,[973.3432000143127, 447.61979166666663, 927.4998567561222, 411.09909899711033, 876.99746, 406.0708064099998],"true", 3) )
    # Connections for obj37 (graphObject_: Obj9) named Time edit
    self.drawConnections(
(self.obj37,self.obj107,[712.9793366410263, 116.43524907991588, 657.7230580365728, 129.38708670904833, 616.7082761016187, 162.15197235573612],"true", 3),
(self.obj37,self.obj111,[728.8063455279241, 116.43524907991583, 757.8544098391559, 131.28899474010322, 779.8398146037276, 153.2222064908919],"true", 3) )
    # Connections for obj38 (graphObject_: Obj10) named Alarm
    self.drawConnections(
(self.obj38,self.obj109,[712.0114979680869, 332.0781907202944, 660.8079729679305, 330.49374199939825, 614.6737593463332, 312.6976063527106],"true", 3),
(self.obj38,self.obj112,[725.3335126426696, 324.04309608307045, 740.6193087280606, 291.4150908196345, 763.4274387922848, 265.37605296474493],"true", 3) )
    # Connections for obj39 (graphObject_: Obj11) named Edit
    self.drawConnections(
(self.obj39,self.obj110,[1021.177499517693, 143.0245629600421, 1017.0, 134.0, 1017.0, 112.5],"true", 3),
(self.obj39,self.obj114,[1012.9793366410264, 156.43524907991588, 970.9896683205133, 186.71762453995794, 925.2453118971162, 200.6000673633019],"true", 3) )
    # Connections for obj40 (graphObject_: Obj12) named Next
    self.drawConnections(
(self.obj40,self.obj116,[1013.3432000143138, 287.61979166666663, 983.171600007157, 265.8098958333333, 937.3362777404379, 246.89620300998948],"true", 3),
(self.obj40,self.obj118,[1013.3432000143138, 287.61979166666663, 891.56672034366, 289.80300942199, 770.4325067220625, 282.0068737753023],"true", 3) )
    # Connections for obj41 (graphObject_: Obj13) named Start edit
    self.drawConnections(
(self.obj41,self.obj113,[828.920208901211, 207.49479166666677, 857.6081246556703, 177.366002510935, 903.6229065906244, 164.6011168642473],"true", 3),
(self.obj41,self.obj115,[828.8063455279241, 216.43524907991588, 865.4031727639621, 253.71762453995794, 911.2044608740027, 272.6283599500527],"true", 3),
(self.obj41,self.obj117,[812.0114979680868, 212.07819072029446, 671.0067044577623, 232.0216055467929],"true", 2) )
    # Connections for obj42 (graphObject_: Obj14) named Check alarm
    self.drawConnections(
(self.obj42,self.obj119,[1236.7479645862115, 644.1680960830704, 1203.0, 587.0, 1240.0000000000002, 588.5],"true", 3),
(self.obj42,self.obj120,[1250.0019109474374, 651.9650203732913, 1283.500955473718, 643.4825101866452, 1319.003352228881, 643.5108027733963],"true", 3) )
    # Connections for obj43 (graphObject_: Obj15) named Start alarm
    self.drawConnections(
(self.obj43,self.obj123,[1408.920208901211, 647.4947916666665, 1448.4601044506055, 638.2473958333333, 1469.5658522288813, 638.2786458333333],"true", 3),
(self.obj43,self.obj127,[1392.9793366410265, 656.4352490799158, 1355.4896683205134, 682.7176245399579, 1319.4464205422378, 682.7176245399579],"true", 3) )
    # Connections for obj44 (graphObject_: Obj16) named Blink
    self.drawConnections(
(self.obj44,self.obj124,[1552.9793366410263, 656.4352490799157, 1499.783016449784, 667.443472331756, 1467.8430946068734, 666.580548435857],"true", 3),
(self.obj44,self.obj128,[1552.9793366410263, 656.4352490799157, 1461.9896683205134, 707.2176245399579, 1385.9464205422378, 707.2176245399579],"true", 3) )
    # Connections for obj45 (graphObject_: Obj17) named AlarmOff
    self.drawConnections(
(self.obj45,self.obj121,[748.9202089012109, 587.4947916666665, 789.9601044506055, 574.7473958333326, 831.0658522288812, 574.7786458333333],"true", 3) )
    # Connections for obj46 (graphObject_: Obj18) named AlarmOn
    self.drawConnections(
(self.obj46,self.obj122,[912.9793366410263, 596.4352490799157, 864.5160866217188, 615.0499005733406, 824.0136898665567, 615.0216079865897],"true", 3),
(self.obj46,self.obj125,[928.9202089012115, 587.4947916666665, 963.4601044506055, 575.2473958333333, 994.5658522288813, 575.2786458333333],"true", 3) )
    # Connections for obj47 (graphObject_: Obj19) named AlarmStart
    self.drawConnections(
(self.obj47,self.obj126,[1052.9793366410263, 596.4352490799157, 984.4896683205138, 635.7176245399578, 908.446420542237, 635.7176245399578],"true", 3),
(self.obj47,self.obj129,[1056.566032899568, 599.8869446635119, 990.9999999999992, 665.0000000000001, 914.0000000000003, 663.0],"true", 3),
(self.obj47,self.obj130,[1056.566032899568, 599.8869446635119, 987.0000000000005, 693.0, 923.4999999999995, 693.5],"true", 3),
(self.obj47,self.obj131,[1056.566032899568, 599.8869446635119, 989.9999999999978, 733.0, 929.4999999999995, 727.5],"true", 3),
(self.obj47,self.obj132,[1056.566032899568, 599.8869446635119, 974.0000000000008, 766.0, 925.9999999999994, 762.5000000000001],"true", 3) )
    # Connections for obj48 (graphObject_: Obj20) named Start
    self.drawConnections(
(self.obj48,self.obj133,[150.0019109474379, 431.9650203732912, 207.00095547371893, 428.98251018664564],"true", 2) )
    # Connections for obj49 (graphObject_: Obj21) named Stop
    self.drawConnections(
 )
    # Connections for obj50 (graphObject_: Obj22) named ChronoPaused2
    self.drawConnections(
(self.obj50,self.obj142,[1274.3023919139619, 244.6506818480548, 1267.7399457158272, 293.8376224040485],"true", 2) )
    # Connections for obj51 (graphObject_: Obj23) named ChronoShowing
    self.drawConnections(
(self.obj51,self.obj135,[1352.001910947437, 420.9650203732914, 1423.0067044577627, 421.0216055467927],"true", 2) )
    # Connections for obj52 (graphObject_: Obj24) named ChronoRunning
    self.drawConnections(
(self.obj52,self.obj136,[1498.7479645862113, 413.16809608307045, 1451.0067044600003, 319.99331296],"true", 2) )
    # Connections for obj53 (graphObject_: Obj25) named ChronoPaused
    self.drawConnections(
(self.obj53,self.obj137,[1441.1774995176934, 183.0245629600421, 1441.0067044577631, 151.9933129600421],"true", 2) )
    # Connections for obj54 (graphObject_: Obj26) named ChronoNotShowing
    self.drawConnections(
(self.obj54,self.obj139,[1280.8359093978331, 120.96206296004212, 1281.0067044577634, 141.9933129600421],"true", 2) )
    # Connections for obj55 (graphObject_: Obj27) named ChronoRunning2
    self.drawConnections(
(self.obj55,self.obj138,[1432.0114979680866, 112.0781907202944, 1361.0067044577631, 112.02160554679284],"true", 2) )
    # Connections for obj56 (graphObject_: Obj28) named ChronoShowing2
    self.drawConnections(
(self.obj56,self.obj140,[1280.8359093978331, 180.96206296004186, 1277.7296980123035, 201.14699740404853],"true", 2) )
    # Connections for obj57 (graphObject_: Obj29) named startTest
    self.drawConnections(
(self.obj57,self.obj141,[1260.8359093978315, 360.96206296004203, 1251.6516752277264, 393.61984127097924, 1261.6370799922984, 403.803053021768],"true", 3) )
    # Connections for obj59 (graphObject_: Obj31) named Main timer loop
    self.drawConnections(
(self.obj59,self.obj66,[374.0000000000002, 639.0, -405.62601771000004, 943.084048042],"true", 2) )
    # Connections for obj60 (graphObject_: Obj32) named Displayloop
    self.drawConnections(
(self.obj60,self.obj67,[339.0, 211.0, 433.005748984, 279.5390953599997],"true", 2),
(self.obj60,self.obj71,[339.0, 211.0, 476.78301645000033, 208.443472332],"true", 2),
(self.obj60,self.obj74,[902.0000000000002, 211.0, 757.6716000070002, 244.30989583299998],"true", 2),
(self.obj60,self.obj75,[1073.0, 224.0, 1122.78301645, 129.4434723320001],"true", 2),
(self.obj60,self.obj76,[1073.0, 224.0, 1275.4896683199997, 122.71762454],"true", 2),
(self.obj60,self.obj77,[1073.0, 224.0, 1410.0057489840442, 223.03909536014726],"true", 2),
(self.obj60,self.obj78,[709.0, 364.9999999999998, 666.4999999999998, 366.9999999999999, 667.4948341602568, 367.85881226997896],"true", 3) )
    # Connections for obj61 (graphObject_: Obj33) named Backlight
    self.drawConnections(
(self.obj61,self.obj68,[349.99999999999983, 480.0000000000001, -298.41125024, 407.51228148],"true", 2),
(self.obj61,self.obj69,[349.99999999999983, 480.0000000000001, -195.49425102, 469.53909536],"true", 2),
(self.obj61,self.obj70,[349.99999999999983, 480.0000000000001, -192.91125023999993, 690.01228148],"true", 2) )
    # Connections for obj62 (graphObject_: Obj34) named Chrono loop
    self.drawConnections(
(self.obj62,self.obj72,[947.0000000000001, 391.99999999999994, 892.67160001, 414.809895833],"true", 2),
(self.obj62,self.obj73,[1185.0, 454.99999999999983, 1067.0057489800006, 459.53909535999975],"true", 2) )
    # Connections for obj63 (graphObject_: Obj35) named Alarm
    self.drawConnections(
(self.obj63,self.obj79,[1387.0, 727.0000000000002, 1231.78301645, 878.443472332],"true", 2),
(self.obj63,self.obj80,[1387.0, 727.0000000000002, 1414.999999999999, 843.0, 1463.00047774, 839.2412550930001],"true", 3),
(self.obj63,self.obj81,[1387.0, 727.0000000000002, 1403.1716000099996, 892.3098958329999],"true", 2) )
    # Connections for obj64 (graphObject_: Obj36) named AlarmState
    self.drawConnections(
(self.obj64,self.obj82,[706.9999999999999, 673.9999999999998, 551.166756321, 475.521548042],"true", 2),
(self.obj64,self.obj83,[706.9999999999999, 673.9999999999998, 605.005748984, 539.53909536],"true", 2),
(self.obj64,self.obj84,[706.9999999999999, 673.9999999999998, 756.5057489800004, 576.53909536],"true", 2) )
    # Connections for obj65 (graphObject_: Obj37) named Test
    self.drawConnections(
(self.obj65,self.obj85,[1387.0, 97.0, 2186.8739822931057, -604.9159519584647],"true", 2),
(self.obj65,self.obj86,[1559.0000000000005, 276.9999999999999, 2209.8739822931057, -499.41595195846486],"true", 2),
(self.obj65,self.obj87,[1559.0000000000005, 276.9999999999999, 1445.4896683205131, 303.2176245399576],"true", 2),
(self.obj65,self.obj88,[1559.0000000000005, 276.9999999999999, 1647.5057489840433, 185.53909536014726],"true", 2),
(self.obj65,self.obj89,[1559.0000000000005, 276.9999999999999, 1682.4896683205125, 157.7176245399579],"true", 2),
(self.obj65,self.obj90,[1559.0000000000005, 276.9999999999999, 1692.0057489840433, 186.0390953601473],"true", 2),
(self.obj65,self.obj91,[1559.0000000000005, 276.9999999999999, 1682.6716000071565, 193.8098958333333],"true", 2),
(self.obj65,self.obj92,[1564.9999999999995, 274.9999999999999, 1606.17160001, 272.309895833],"true", 2) )
    # Connections for obj66 (graphObject_: Obj38) of type contains
    self.drawConnections(
(self.obj66,self.obj30,[-405.62601771000004, 943.084048042, 412.979336641026, 656.4352490799158],"true", 2) )
    # Connections for obj67 (graphObject_: Obj39) of type contains
    self.drawConnections(
(self.obj67,self.obj29,[433.005748984, 279.5390953599997, 512.9793366410263, 256.4352490799159],"true", 2) )
    # Connections for obj68 (graphObject_: Obj40) of type contains
    self.drawConnections(
(self.obj68,self.obj31,[-298.41125024, 407.51228148, 372.0114979680868, 412.07819072029423],"true", 2) )
    # Connections for obj69 (graphObject_: Obj41) of type contains
    self.drawConnections(
(self.obj69,self.obj32,[-195.49425102, 469.53909536, 612.0114979680869, 412.07819072029423],"true", 2) )
    # Connections for obj70 (graphObject_: Obj42) of type contains
    self.drawConnections(
(self.obj70,self.obj33,[-192.91125023999993, 690.01228148, 452.9793366410264, 536.4352490799157],"true", 2) )
    # Connections for obj71 (graphObject_: Obj43) of type contains
    self.drawConnections(
(self.obj71,self.obj34,[476.78301645000033, 208.443472332, 425.2765809560262, 139.8869446635121],"true", 2) )
    # Connections for obj72 (graphObject_: Obj44) of type contains
    self.drawConnections(
(self.obj72,self.obj35,[892.67160001, 414.80989583299987, 770.001910947438, 431.9650203732912],"true", 2) )
    # Connections for obj73 (graphObject_: Obj45) of type contains
    self.drawConnections(
(self.obj73,self.obj36,[1067.0057489800006, 459.5390953599997, 990.0019109474379, 451.96502037329117],"true", 2) )
    # Connections for obj74 (graphObject_: Obj46) of type contains
    self.drawConnections(
(self.obj74,self.obj37,[757.6716000070002, 244.30989583299998, 725.2765809560262, 119.88694466351212],"true", 2) )
    # Connections for obj75 (graphObject_: Obj47) of type contains
    self.drawConnections(
(self.obj75,self.obj39,[1122.78301645, 129.4434723320001, 1030.0019109474379, 151.96502037329134],"true", 2) )
    # Connections for obj76 (graphObject_: Obj48) of type contains
    self.drawConnections(
(self.obj76,self.obj40,[1275.4896683199997, 122.71762454, 1028.920208901211, 287.49479166666663],"true", 2) )
    # Connections for obj77 (graphObject_: Obj49) of type contains
    self.drawConnections(
(self.obj77,self.obj41,[1410.0057489840442, 223.03909536014726, 830.0019109474379, 211.96502037329134],"true", 2) )
    # Connections for obj78 (graphObject_: Obj50) of type contains
    self.drawConnections(
(self.obj78,self.obj38,[667.4948341602568, 367.85881226997896, 668.4896683205133, 368.7176245399577, 712.9793366410265, 336.4352490799158],"true", 3) )
    # Connections for obj79 (graphObject_: Obj51) of type contains
    self.drawConnections(
(self.obj79,self.obj42,[1231.78301645, 878.443472332, 1240.8359093978318, 660.962062960042],"true", 2) )
    # Connections for obj80 (graphObject_: Obj52) of type contains
    self.drawConnections(
(self.obj80,self.obj43,[1463.00047774, 839.2412550930001, 1511.000955473719, 835.4825101866456, 1405.2765809560262, 659.886944663512],"true", 3) )
    # Connections for obj81 (graphObject_: Obj53) of type contains
    self.drawConnections(
(self.obj81,self.obj44,[1403.1716000099996, 892.3098958329999, 1556.566032899568, 659.886944663512],"true", 2) )
    # Connections for obj82 (graphObject_: Obj54) of type contains
    self.drawConnections(
(self.obj82,self.obj45,[551.166756321, 475.521548042, 733.3432000143138, 587.6197916666665],"true", 2) )
    # Connections for obj83 (graphObject_: Obj55) of type contains
    self.drawConnections(
(self.obj83,self.obj46,[605.005748984, 539.53909536, 912.0114979680869, 592.0781907202947],"true", 2) )
    # Connections for obj84 (graphObject_: Obj56) of type contains
    self.drawConnections(
(self.obj84,self.obj47,[756.5057489800004, 576.53909536, 1052.0114979680866, 592.0781907202947],"true", 2) )
    # Connections for obj85 (graphObject_: Obj57) of type contains
    self.drawConnections(
(self.obj85,self.obj50,[2186.8739822931047, -604.9159519584641, 1278.5301389642532, 222.65602490799182],"true", 2) )
    # Connections for obj86 (graphObject_: Obj58) of type contains
    self.drawConnections(
(self.obj86,self.obj51,[2209.8739822931057, -499.41595195846486, 1350.9202089012113, 416.49479166666623],"true", 2) )
    # Connections for obj87 (graphObject_: Obj59) of type contains
    self.drawConnections(
(self.obj87,self.obj52,[1445.4896683205131, 303.2176245399579, 1503.1774995176927, 412.024562960042],"true", 2) )
    # Connections for obj88 (graphObject_: Obj60) of type contains
    self.drawConnections(
(self.obj88,self.obj53,[1647.5057489840433, 185.5390953601472, 1450.0019109474376, 191.96502037329134],"true", 2) )
    # Connections for obj89 (graphObject_: Obj61) of type contains
    self.drawConnections(
(self.obj89,self.obj55,[1682.4896683205122, 157.7176245399579, 1450.0019109474379, 111.96502037329131],"true", 2) )
    # Connections for obj90 (graphObject_: Obj62) of type contains
    self.drawConnections(
(self.obj90,self.obj54,[1692.0057489840433, 186.0390953601473, 1290.0019109474379, 111.96502037329131],"true", 2) )
    # Connections for obj91 (graphObject_: Obj63) of type contains
    self.drawConnections(
(self.obj91,self.obj56,[1682.6716000071565, 193.8098958333333, 1290.0019109474379, 171.96502037329128],"true", 2) )
    # Connections for obj92 (graphObject_: Obj64) of type contains
    self.drawConnections(
(self.obj92,self.obj57,[1606.17160001, 272.309895833, 1270.0019109474374, 351.9650203732913],"true", 2) )
    # Connections for obj93 (graphObject_: Obj65) named 
    self.drawConnections(
(self.obj93,self.obj30,[419.99999999999886, 601.9999999999995, 434.99999999999886, 601.9999999999995, 425.33351264266855, 644.0430960830702],"true", 3) )
    # Connections for obj94 (graphObject_: Obj67) named 
    self.drawConnections(
(self.obj94,self.obj29,[521.0000000000001, 298.9999999999998, 504.9999999999997, 298.9999999999998, 516.566032899568, 259.88694466351205],"true", 3) )
    # Connections for obj95 (graphObject_: Obj69) named 
    self.drawConnections(
(self.obj95,self.obj31,[501.00160691518363, 422.02160424754584, 445.4992101600189, 421.99331166079514, 390.0019109474371, 411.9650203732913],"true", 3) )
    # Connections for obj96 (graphObject_: Obj71) named 
    self.drawConnections(
(self.obj96,self.obj33,[412.76366771618586, 477.68088350046924, 430.63151362373225, 503.7511713553589, 456.7479645862104, 524.1680960830704],"true", 3) )
    # Connections for obj97 (graphObject_: Obj73) named 
    self.drawConnections(
(self.obj97,self.obj32,[534.8442125965189, 464.04528963580174, 570.8589945314714, 436.28040398911367, 612.9793366410263, 416.4352490799158],"true", 3) )
    # Connections for obj98 (graphObject_: Obj75) named 
    self.drawConnections(
(self.obj98,self.obj34,[400.5299822133067, 202.43327707001237, 377.6621363057601, 176.3629892151226, 412.97933664102646, 136.43524907991585],"true", 3) )
    # Connections for obj99 (graphObject_: Obj77) named 
    self.drawConnections(
(self.obj99,self.obj29,[468.49456332893067, 174.6217636765701, 491.3624092364772, 200.6920515314596, 516.747964586211, 244.16809608307042],"true", 3) )
    # Connections for obj100 (graphObject_: Obj79) named 
    self.drawConnections(
(self.obj100,self.obj36,[870.0159489145641, 451.972404683225, 920.5183456697264, 457.0006972699757, 972.0114979680866, 452.07819072029423],"true", 3) )
    # Connections for obj101 (graphObject_: Obj81) named 
    self.drawConnections(
(self.obj101,self.obj36,[982.9999999999998, 489.99999999999994, 967.9999999999998, 489.99999999999994, 976.566032899568, 459.88694466351194],"true", 3) )
    # Connections for obj102 (graphObject_: Obj83) named 
    self.drawConnections(
(self.obj102,self.obj36,[1082.9999999999995, 454.9999999999997, 1082.9999999999995, 400.00000000000006, 988.9202089012113, 447.49479166666663],"true", 3) )
    # Connections for obj103 (graphObject_: Obj85) named 
    self.drawConnections(
(self.obj103,self.obj35,[876.99746, 406.0708064099998, 826.4950632457983, 401.04251382360894, 768.9202089012109, 427.49479166666663],"true", 3) )
    # Connections for obj104 (graphObject_: Obj87) named 
    self.drawConnections(
(self.obj104,self.obj35,[762.9999999999995, 469.9999999999997, 747.9999999999995, 469.9999999999997, 756.5660328995676, 439.886944663512],"true", 3) )
    # Connections for obj105 (graphObject_: Obj89) named 
    self.drawConnections(
(self.obj105,self.obj34,[422.0, 92.00000000000001, 437.0, 91.99999999999994, 425.33351264266946, 124.04309608307044],"true", 3) )
    # Connections for obj106 (graphObject_: Obj91) named 
    self.drawConnections(
(self.obj106,self.obj37,[640.191269441, 191.778068391, 681.2060513755719, 159.01318274415877, 716.5660328995683, 119.88694466351212],"true", 3) )
    # Connections for obj107 (graphObject_: Obj93) named 
    self.drawConnections(
(self.obj107,self.obj29,[616.7082761016187, 162.15197235573612, 575.693494166665, 194.91685800242374, 525.3335126426695, 244.0430960830703],"true", 3) )
    # Connections for obj108 (graphObject_: Obj95) named 
    self.drawConnections(
(self.obj108,self.obj38,[644.0469544607556, 278.65828528842513, 689.8822767274746, 302.57197811176894, 713.3432000143138, 327.61979166666663],"true", 3) )
    # Connections for obj109 (graphObject_: Obj97) named 
    self.drawConnections(
(self.obj109,self.obj29,[614.6737593463332, 312.6976063527106, 568.5395457247356, 294.90147070602285, 528.8063455279241, 256.4352490799159],"true", 3) )
    # Connections for obj110 (graphObject_: Obj99) named 
    self.drawConnections(
(self.obj110,self.obj39,[1017.0, 112.5, 1017.0, 90.99999999999994, 1021.177499517693, 143.0245629600421],"true", 3) )
    # Connections for obj111 (graphObject_: Obj101) named 
    self.drawConnections(
(self.obj111,self.obj41,[779.8398146037276, 153.2222064908919, 801.8252193682995, 175.15541824168054, 816.7479645862109, 204.16809608307048],"true", 3) )
    # Connections for obj112 (graphObject_: Obj103) named 
    self.drawConnections(
(self.obj112,self.obj41,[763.4274387922848, 265.37605296474493, 786.2355688565099, 239.3370151098553, 816.566032899568, 219.88694466351217],"true", 3) )
    # Connections for obj113 (graphObject_: Obj105) named 
    self.drawConnections(
(self.obj113,self.obj39,[903.6229065906244, 164.6011168642473, 949.6376885255781, 151.8362312175595, 1012.0114979680869, 152.07819072029446],"true", 3) )
    # Connections for obj114 (graphObject_: Obj107) named 
    self.drawConnections(
(self.obj114,self.obj41,[925.2453118971162, 200.6000673633019, 879.5009554737189, 214.48251018664567, 830.0019109474379, 211.96502037329134],"true", 3) )
    # Connections for obj115 (graphObject_: Obj109) named 
    self.drawConnections(
(self.obj115,self.obj40,[911.2044608740027, 272.6283599500527, 957.0057489840435, 291.5390953601472, 1012.0114979680869, 292.0781907202943],"true", 3) )
    # Connections for obj116 (graphObject_: Obj111) named 
    self.drawConnections(
(self.obj116,self.obj41,[937.3362777404379, 246.89620300998948, 891.5009554737189, 227.98251018664567, 830.0019109474379, 211.96502037329134],"true", 3) )
    # Connections for obj117 (graphObject_: Obj113) named 
    self.drawConnections(
(self.obj117,self.obj29,[671.0067044577623, 232.0216055467929, 530.0019109474379, 251.96502037329134],"true", 2) )
    # Connections for obj118 (graphObject_: Obj115) named 
    self.drawConnections(
(self.obj118,self.obj29,[770.4325067220625, 282.0068737753023, 649.2982931004651, 274.21073812861437, 528.8063455279241, 256.4352490799159],"true", 3) )
    # Connections for obj119 (graphObject_: Obj117) named 
    self.drawConnections(
(self.obj119,self.obj42,[1240.0000000000002, 588.5, 1277.0, 590.0000000000002, 1245.3335126426696, 644.0430960830704],"true", 3) )
    # Connections for obj120 (graphObject_: Obj119) named 
    self.drawConnections(
(self.obj120,self.obj43,[1319.003352228881, 643.5108027733963, 1354.5057489840433, 643.5390953601474, 1392.0114979680868, 652.0781907202946],"true", 3) )
    # Connections for obj121 (graphObject_: Obj121) named 
    self.drawConnections(
(self.obj121,self.obj46,[831.0658522288812, 574.7786458333333, 872.1716000071569, 574.8098958333333, 913.3432000143135, 587.6197916666665],"true", 3) )
    # Connections for obj122 (graphObject_: Obj123) named 
    self.drawConnections(
(self.obj122,self.obj45,[824.0136898665567, 615.0216079865897, 783.511293111394, 614.9933153998389, 748.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj123 (graphObject_: Obj125) named 
    self.drawConnections(
(self.obj123,self.obj44,[1469.5658522288813, 638.2786458333333, 1490.671600007157, 638.3098958333334, 1552.0114979680864, 652.0781907202946],"true", 3) )
    # Connections for obj124 (graphObject_: Obj127) named 
    self.drawConnections(
(self.obj124,self.obj43,[1467.8430946068734, 666.580548435857, 1435.9031727639622, 665.7176245399579, 1408.8063455279241, 656.4352490799158],"true", 3) )
    # Connections for obj125 (graphObject_: Obj129) named 
    self.drawConnections(
(self.obj125,self.obj47,[994.5658522288813, 575.2786458333333, 1025.671600007158, 575.3098958333333, 1053.3432000143137, 587.6197916666665],"true", 3) )
    # Connections for obj126 (graphObject_: Obj131) named 
    self.drawConnections(
(self.obj126,self.obj45,[908.446420542237, 635.7176245399578, 832.4031727639622, 635.7176245399578, 748.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj127 (graphObject_: Obj133) named 
    self.drawConnections(
(self.obj127,self.obj42,[1319.4464205422378, 682.7176245399579, 1283.4031727639622, 682.7176245399579, 1248.8063455279234, 656.4352490799158],"true", 3) )
    # Connections for obj128 (graphObject_: Obj135) named 
    self.drawConnections(
(self.obj128,self.obj42,[1385.9464205422378, 707.2176245399579, 1309.9031727639622, 707.2176245399579, 1248.8063455279234, 656.4352490799158],"true", 3) )
    # Connections for obj129 (graphObject_: Obj137) named 
    self.drawConnections(
(self.obj129,self.obj45,[914.0000000000003, 663.0, 837.0000000000002, 661.0, 748.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj130 (graphObject_: Obj139) named 
    self.drawConnections(
(self.obj130,self.obj45,[923.4999999999995, 693.5, 859.9999999999994, 694.0, 748.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj131 (graphObject_: Obj141) named 
    self.drawConnections(
(self.obj131,self.obj45,[929.4999999999995, 727.5, 868.9999999999997, 722.0, 748.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj132 (graphObject_: Obj143) named 
    self.drawConnections(
(self.obj132,self.obj45,[925.9999999999994, 762.5000000000001, 877.9999999999995, 759.0, 745.2765809560262, 599.8869446635119],"true", 3) )
    # Connections for obj133 (graphObject_: Obj145) named 
    self.drawConnections(
(self.obj133,self.obj28,[207.00095547371893, 428.98251018664564, 308.0, 425.9999999999998],"true", 2) )
    # Connections for obj134 (graphObject_: Obj147) named 
    self.drawConnections(
(self.obj134,self.obj49,[1696.5057489840433, 429.03909536014714, 1772.0114979680868, 432.07819072029434],"true", 2) )
    # Connections for obj135 (graphObject_: Obj149) named 
    self.drawConnections(
(self.obj135,self.obj52,[1423.0067044577627, 421.0216055467927, 1494.0114979680866, 421.0781907202943],"true", 2) )
    # Connections for obj136 (graphObject_: Obj151) named 
    self.drawConnections(
(self.obj136,self.obj53,[1451.0067044600005, 319.99331295999957, 1440.8359093978318, 200.96206296004203],"true", 2) )
    # Connections for obj137 (graphObject_: Obj153) named 
    self.drawConnections(
(self.obj137,self.obj55,[1441.0067044577631, 151.9933129600421, 1440.835909397833, 120.96206296004206],"true", 2) )
    # Connections for obj138 (graphObject_: Obj155) named 
    self.drawConnections(
(self.obj138,self.obj54,[1361.0067044577631, 112.02160554679284, 1290.0019109474374, 111.96502037329141],"true", 2) )
    # Connections for obj139 (graphObject_: Obj157) named 
    self.drawConnections(
(self.obj139,self.obj56,[1281.0067044577634, 141.9933129600421, 1281.1774995176936, 163.02456296004206],"true", 2) )
    # Connections for obj140 (graphObject_: Obj159) named 
    self.drawConnections(
(self.obj140,self.obj50,[1277.7296980123035, 201.14699740404853, 1274.623486626774, 221.33193184805506],"true", 2) )
    # Connections for obj141 (graphObject_: Obj161) named 
    self.drawConnections(
(self.obj141,self.obj51,[1261.6370799922984, 403.8030530217679, 1271.6224847568703, 413.98626477255647, 1334.011497968087, 421.07819072029434],"true", 3) )
    # Connections for obj142 (graphObject_: Obj163) named 
    self.drawConnections(
(self.obj142,self.obj57,[1267.7399457158272, 293.8376224040485, 1261.177499517693, 343.0245629600421],"true", 2) )
    # Connections for obj143 (graphObject_: Obj165) of type orthogonality
    self.drawConnections(
(self.obj143,self.obj61,[330.49999999999994, 386.99999999999994, 319.74999999999994, 339.2500000000001, 349.99999999999983, 480.0000000000001],"true", 3) )
    # Connections for obj144 (graphObject_: Obj166) of type orthogonality
    self.drawConnections(
(self.obj144,self.obj60,[327.0000000000001, 73.0, 709.0, 83.0],"true", 2) )
    # Connections for obj145 (graphObject_: Obj167) of type orthogonality
    self.drawConnections(
(self.obj145,self.obj59,[337.99999999999955, 328.5, 423.0, 593.9999999999999],"true", 2) )
    # Connections for obj146 (graphObject_: Obj168) of type orthogonality
    self.drawConnections(
(self.obj146,self.obj64,[504.4999999999999, 314.9999999999999, 706.9999999999999, 673.9999999999998],"true", 2) )
    # Connections for obj147 (graphObject_: Obj169) of type orthogonality
    self.drawConnections(
(self.obj147,self.obj62,[509.5000000000003, 227.5, 709.0, 454.9999999999999],"true", 2) )
    # Connections for obj148 (graphObject_: Obj170) of type orthogonality
    self.drawConnections(
(self.obj148,self.obj63,[747.5000000000002, 321.0, 1187.0, 652.9999999999995],"true", 2) )
    # Connections for obj149 (graphObject_: Obj171) of type orthogonality
    self.drawConnections(
(self.obj149,self.obj65,[754.4999999999999, 80.00000000000006, 1210.0000000000005, 276.9999999999999],"true", 2) )

newfunction = DigitalWatchStatechart_DCharts_MDL

loadedMMName = 'DCharts'

atom3version = '0.3'
