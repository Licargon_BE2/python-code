"""
__DigitalWatchStatechart_DCharts_MDL.py_____________________________________________________

Automatically generated AToM3 Model File (Do not modify directly)
Author: Timmy
Modified: Wed Dec 12 22:47:35 2012
____________________________________________________________________________________________
"""
from stickylink import *
from widthXfillXdecoration import *
from Composite import *
from Basic import *
from Orthogonal import *
from contains import *
from Hyperedge import *
from orthogonality import *
from graph_Basic import *
from graph_Orthogonal import *
from graph_Hyperedge import *
from graph_orthogonality import *
from graph_contains import *
from graph_Composite import *
from ATOM3Enum import *
from ATOM3String import *
from ATOM3BottomType import *
from ATOM3Constraint import *
from ATOM3Attribute import *
from ATOM3Float import *
from ATOM3List import *
from ATOM3Link import *
from ATOM3Connection import *
from ATOM3Boolean import *
from ATOM3Appearance import *
from ATOM3Text import *
from ATOM3Action import *
from ATOM3Integer import *
from ATOM3Port import *
from ATOM3MSEnum import *

def DigitalWatchStatechart_DCharts_MDL(self, rootNode, DChartsRootNode=None):

    # --- Generating attributes code for ASG DCharts ---
    if( DChartsRootNode ): 
        # variables
        DChartsRootNode.variables.setValue('\n')
        DChartsRootNode.variables.setHeight(15)

        # misc
        DChartsRootNode.misc.setValue('\n')
        DChartsRootNode.misc.setHeight(15)

        # event_clauses
        DChartsRootNode.event_clauses.setValue('\n')
        DChartsRootNode.event_clauses.setHeight(15)
    # --- ASG attributes over ---


    self.obj28=Composite(self)
    self.obj28.isGraphObjectVisual = True

    if(hasattr(self.obj28, '_setHierarchicalLink')):
      self.obj28._setHierarchicalLink(False)

    # auto_adjust
    self.obj28.auto_adjust.setValue((None, 1))
    self.obj28.auto_adjust.config = 0

    # name
    self.obj28.name.setValue('DigitalWatch')

    # is_default
    self.obj28.is_default.setValue((None, 0))
    self.obj28.is_default.config = 0

    # visible
    self.obj28.visible.setValue((None, 1))
    self.obj28.visible.config = 0

    # exit_action
    self.obj28.exit_action.setValue('\n')
    self.obj28.exit_action.setHeight(15)

    # enter_action
    self.obj28.enter_action.setValue('\n')
    self.obj28.enter_action.setHeight(15)

    self.obj28.graphClass_= graph_Composite
    if self.genGraphics:
       new_obj = graph_Composite(380.0,700.0,self.obj28)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Composite", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf2.handler,306.0,64.0,1593.0,789.0)
       self.UMLmodel.itemconfig(new_obj.gf2.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf2.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf2.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf2.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,306.0,57.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='DigitalWatch')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj28.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj28)
    self.globalAndLocalPostcondition(self.obj28, rootNode)
    self.obj28.postAction( rootNode.CREATE )

    self.obj29=Basic(self)
    self.obj29.isGraphObjectVisual = True

    if(hasattr(self.obj29, '_setHierarchicalLink')):
      self.obj29._setHierarchicalLink(False)

    # is_default
    self.obj29.is_default.setValue((None, 1))
    self.obj29.is_default.config = 0

    # name
    self.obj29.name.setValue('Time')

    # exit_action
    self.obj29.exit_action.setValue('\n')
    self.obj29.exit_action.setHeight(15)

    # enter_action
    self.obj29.enter_action.setValue('\n')
    self.obj29.enter_action.setHeight(15)

    self.obj29.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(500.0,240.0,self.obj29)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,512.0,243.0,530.0,261.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,523.125,270.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Time')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font-159207508')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj29.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj29)
    self.globalAndLocalPostcondition(self.obj29, rootNode)
    self.obj29.postAction( rootNode.CREATE )

    self.obj30=Basic(self)
    self.obj30.isGraphObjectVisual = True

    if(hasattr(self.obj30, '_setHierarchicalLink')):
      self.obj30._setHierarchicalLink(False)

    # is_default
    self.obj30.is_default.setValue((None, 1))
    self.obj30.is_default.config = 0

    # name
    self.obj30.name.setValue('TimeUpdate')

    # exit_action
    self.obj30.exit_action.setValue('\n')
    self.obj30.exit_action.setHeight(15)

    # enter_action
    self.obj30.enter_action.setValue('\n')
    self.obj30.enter_action.setHeight(15)

    self.obj30.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1180.0,160.0,self.obj30)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1192.0,163.0,1210.0,181.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1203.125,190.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='TimeUpdate')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font58756200')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj30.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj30)
    self.globalAndLocalPostcondition(self.obj30, rootNode)
    self.obj30.postAction( rootNode.CREATE )

    self.obj31=Basic(self)
    self.obj31.isGraphObjectVisual = True

    if(hasattr(self.obj31, '_setHierarchicalLink')):
      self.obj31._setHierarchicalLink(False)

    # is_default
    self.obj31.is_default.setValue((None, 0))
    self.obj31.is_default.config = 0

    # name
    self.obj31.name.setValue('LightOn')

    # exit_action
    self.obj31.exit_action.setValue('\n')
    self.obj31.exit_action.setHeight(15)

    # enter_action
    self.obj31.enter_action.setValue('\n')
    self.obj31.enter_action.setHeight(15)

    self.obj31.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(380.0,420.0,self.obj31)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,392.0,423.0,410.0,441.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,403.125,450.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='LightOn')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font54742984')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj31.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj31)
    self.globalAndLocalPostcondition(self.obj31, rootNode)
    self.obj31.postAction( rootNode.CREATE )

    self.obj32=Basic(self)
    self.obj32.isGraphObjectVisual = True

    if(hasattr(self.obj32, '_setHierarchicalLink')):
      self.obj32._setHierarchicalLink(False)

    # is_default
    self.obj32.is_default.setValue((None, 1))
    self.obj32.is_default.config = 0

    # name
    self.obj32.name.setValue('LightOff')

    # exit_action
    self.obj32.exit_action.setValue('\n')
    self.obj32.exit_action.setHeight(15)

    # enter_action
    self.obj32.enter_action.setValue('\n')
    self.obj32.enter_action.setHeight(15)

    self.obj32.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(620.0,420.0,self.obj32)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,632.0,423.0,650.0,441.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,643.125,450.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='LightOff')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font53782720')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj32.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj32)
    self.globalAndLocalPostcondition(self.obj32, rootNode)
    self.obj32.postAction( rootNode.CREATE )

    self.obj33=Basic(self)
    self.obj33.isGraphObjectVisual = True

    if(hasattr(self.obj33, '_setHierarchicalLink')):
      self.obj33._setHierarchicalLink(False)

    # is_default
    self.obj33.is_default.setValue((None, 0))
    self.obj33.is_default.config = 0

    # name
    self.obj33.name.setValue('Button released')

    # exit_action
    self.obj33.exit_action.setValue('\n')
    self.obj33.exit_action.setHeight(15)

    # enter_action
    self.obj33.enter_action.setValue('\n')
    self.obj33.enter_action.setHeight(15)

    self.obj33.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(460.0,540.0,self.obj33)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,472.0,543.0,490.0,561.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,483.125,570.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Button released')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font54893384')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj33.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj33)
    self.globalAndLocalPostcondition(self.obj33, rootNode)
    self.obj33.postAction( rootNode.CREATE )

    self.obj34=Basic(self)
    self.obj34.isGraphObjectVisual = True

    if(hasattr(self.obj34, '_setHierarchicalLink')):
      self.obj34._setHierarchicalLink(False)

    # is_default
    self.obj34.is_default.setValue((None, 0))
    self.obj34.is_default.config = 0

    # name
    self.obj34.name.setValue('Chrono')

    # exit_action
    self.obj34.exit_action.setValue('\n')
    self.obj34.exit_action.setHeight(15)

    # enter_action
    self.obj34.enter_action.setValue('\n')
    self.obj34.enter_action.setHeight(15)

    self.obj34.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(400.0,120.0,self.obj34)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,412.0,123.0,430.0,141.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,423.125,150.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font54188728')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj34.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj34)
    self.globalAndLocalPostcondition(self.obj34, rootNode)
    self.obj34.postAction( rootNode.CREATE )

    self.obj35=Basic(self)
    self.obj35.isGraphObjectVisual = True

    if(hasattr(self.obj35, '_setHierarchicalLink')):
      self.obj35._setHierarchicalLink(False)

    # is_default
    self.obj35.is_default.setValue((None, 1))
    self.obj35.is_default.config = 0

    # name
    self.obj35.name.setValue('Chrono off')

    # exit_action
    self.obj35.exit_action.setValue('\n')
    self.obj35.exit_action.setHeight(15)

    # enter_action
    self.obj35.enter_action.setValue('\n')
    self.obj35.enter_action.setHeight(15)

    self.obj35.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(800.0,440.0,self.obj35)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,812.0,443.0,830.0,461.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,823.125,470.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono off')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font66002384')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj35.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj35)
    self.globalAndLocalPostcondition(self.obj35, rootNode)
    self.obj35.postAction( rootNode.CREATE )

    self.obj36=Basic(self)
    self.obj36.isGraphObjectVisual = True

    if(hasattr(self.obj36, '_setHierarchicalLink')):
      self.obj36._setHierarchicalLink(False)

    # is_default
    self.obj36.is_default.setValue((None, 0))
    self.obj36.is_default.config = 0

    # name
    self.obj36.name.setValue('Chrono on')

    # exit_action
    self.obj36.exit_action.setValue('\n')
    self.obj36.exit_action.setHeight(15)

    # enter_action
    self.obj36.enter_action.setValue('\n')
    self.obj36.enter_action.setHeight(15)

    self.obj36.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1020.0,460.0,self.obj36)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1032.0,463.0,1050.0,481.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1043.125,490.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono on')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font67559792')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj36.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj36)
    self.globalAndLocalPostcondition(self.obj36, rootNode)
    self.obj36.postAction( rootNode.CREATE )

    self.obj37=Basic(self)
    self.obj37.isGraphObjectVisual = True

    if(hasattr(self.obj37, '_setHierarchicalLink')):
      self.obj37._setHierarchicalLink(False)

    # is_default
    self.obj37.is_default.setValue((None, 0))
    self.obj37.is_default.config = 0

    # name
    self.obj37.name.setValue('Time edit')

    # exit_action
    self.obj37.exit_action.setValue('\n')
    self.obj37.exit_action.setHeight(15)

    # enter_action
    self.obj37.enter_action.setValue('\n')
    self.obj37.enter_action.setHeight(15)

    self.obj37.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(700.0,100.0,self.obj37)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,712.0,103.0,730.0,121.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,723.125,130.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Time edit')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font60347960')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj37.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj37)
    self.globalAndLocalPostcondition(self.obj37, rootNode)
    self.obj37.postAction( rootNode.CREATE )

    self.obj38=Basic(self)
    self.obj38.isGraphObjectVisual = True

    if(hasattr(self.obj38, '_setHierarchicalLink')):
      self.obj38._setHierarchicalLink(False)

    # is_default
    self.obj38.is_default.setValue((None, 0))
    self.obj38.is_default.config = 0

    # name
    self.obj38.name.setValue('Alarm')

    # exit_action
    self.obj38.exit_action.setValue('\n')
    self.obj38.exit_action.setHeight(15)

    # enter_action
    self.obj38.enter_action.setValue('\n')
    self.obj38.enter_action.setHeight(15)

    self.obj38.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(700.0,320.0,self.obj38)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,712.0,323.0,730.0,341.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,723.125,350.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font96836552')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj38.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj38)
    self.globalAndLocalPostcondition(self.obj38, rootNode)
    self.obj38.postAction( rootNode.CREATE )

    self.obj39=Basic(self)
    self.obj39.isGraphObjectVisual = True

    if(hasattr(self.obj39, '_setHierarchicalLink')):
      self.obj39._setHierarchicalLink(False)

    # is_default
    self.obj39.is_default.setValue((None, 0))
    self.obj39.is_default.config = 0

    # name
    self.obj39.name.setValue('Edit')

    # exit_action
    self.obj39.exit_action.setValue('\n')
    self.obj39.exit_action.setHeight(15)

    # enter_action
    self.obj39.enter_action.setValue('\n')
    self.obj39.enter_action.setHeight(15)

    self.obj39.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1000.0,140.0,self.obj39)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1012.0,143.0,1030.0,161.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1023.125,170.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Edit')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font181967948')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj39.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj39)
    self.globalAndLocalPostcondition(self.obj39, rootNode)
    self.obj39.postAction( rootNode.CREATE )

    self.obj40=Basic(self)
    self.obj40.isGraphObjectVisual = True

    if(hasattr(self.obj40, '_setHierarchicalLink')):
      self.obj40._setHierarchicalLink(False)

    # is_default
    self.obj40.is_default.setValue((None, 0))
    self.obj40.is_default.config = 0

    # name
    self.obj40.name.setValue('Next')

    # exit_action
    self.obj40.exit_action.setValue('\n')
    self.obj40.exit_action.setHeight(15)

    # enter_action
    self.obj40.enter_action.setValue('\n')
    self.obj40.enter_action.setHeight(15)

    self.obj40.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1000.0,280.0,self.obj40)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1012.0,283.0,1030.0,301.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1023.125,310.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Next')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font181966220')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj40.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj40)
    self.globalAndLocalPostcondition(self.obj40, rootNode)
    self.obj40.postAction( rootNode.CREATE )

    self.obj41=Basic(self)
    self.obj41.isGraphObjectVisual = True

    if(hasattr(self.obj41, '_setHierarchicalLink')):
      self.obj41._setHierarchicalLink(False)

    # is_default
    self.obj41.is_default.setValue((None, 0))
    self.obj41.is_default.config = 0

    # name
    self.obj41.name.setValue('Start edit')

    # exit_action
    self.obj41.exit_action.setValue('\n')
    self.obj41.exit_action.setHeight(15)

    # enter_action
    self.obj41.enter_action.setValue('\n')
    self.obj41.enter_action.setHeight(15)

    self.obj41.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(800.0,200.0,self.obj41)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,812.0,203.0,830.0,221.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,823.125,230.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Start edit')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font182352332')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj41.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj41)
    self.globalAndLocalPostcondition(self.obj41, rootNode)
    self.obj41.postAction( rootNode.CREATE )

    self.obj42=Basic(self)
    self.obj42.isGraphObjectVisual = True

    if(hasattr(self.obj42, '_setHierarchicalLink')):
      self.obj42._setHierarchicalLink(False)

    # is_default
    self.obj42.is_default.setValue((None, 1))
    self.obj42.is_default.config = 0

    # name
    self.obj42.name.setValue('Check alarm')

    # exit_action
    self.obj42.exit_action.setValue('\n')
    self.obj42.exit_action.setHeight(15)

    # enter_action
    self.obj42.enter_action.setValue('\n')
    self.obj42.enter_action.setHeight(15)

    self.obj42.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1220.0,640.0,self.obj42)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1232.0,643.0,1250.0,661.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1243.125,670.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Check alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font41751256')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj42.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj42)
    self.globalAndLocalPostcondition(self.obj42, rootNode)
    self.obj42.postAction( rootNode.CREATE )

    self.obj43=Basic(self)
    self.obj43.isGraphObjectVisual = True

    if(hasattr(self.obj43, '_setHierarchicalLink')):
      self.obj43._setHierarchicalLink(False)

    # is_default
    self.obj43.is_default.setValue((None, 0))
    self.obj43.is_default.config = 0

    # name
    self.obj43.name.setValue('Start alarm')

    # exit_action
    self.obj43.exit_action.setValue('\n')
    self.obj43.exit_action.setHeight(15)

    # enter_action
    self.obj43.enter_action.setValue('\n')
    self.obj43.enter_action.setHeight(15)

    self.obj43.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1380.0,640.0,self.obj43)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1392.0,643.0,1410.0,661.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1403.125,670.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Start alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font41752624')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj43.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj43)
    self.globalAndLocalPostcondition(self.obj43, rootNode)
    self.obj43.postAction( rootNode.CREATE )

    self.obj44=Basic(self)
    self.obj44.isGraphObjectVisual = True

    if(hasattr(self.obj44, '_setHierarchicalLink')):
      self.obj44._setHierarchicalLink(False)

    # is_default
    self.obj44.is_default.setValue((None, 0))
    self.obj44.is_default.config = 0

    # name
    self.obj44.name.setValue('Blink')

    # exit_action
    self.obj44.exit_action.setValue('\n')
    self.obj44.exit_action.setHeight(15)

    # enter_action
    self.obj44.enter_action.setValue('\n')
    self.obj44.enter_action.setHeight(15)

    self.obj44.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1540.0,640.0,self.obj44)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1552.0,643.0,1570.0,661.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1563.125,670.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Blink')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font41764480')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj44.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj44)
    self.globalAndLocalPostcondition(self.obj44, rootNode)
    self.obj44.postAction( rootNode.CREATE )

    self.obj45=Basic(self)
    self.obj45.isGraphObjectVisual = True

    if(hasattr(self.obj45, '_setHierarchicalLink')):
      self.obj45._setHierarchicalLink(False)

    # is_default
    self.obj45.is_default.setValue((None, 1))
    self.obj45.is_default.config = 0

    # name
    self.obj45.name.setValue('AlarmOff')

    # exit_action
    self.obj45.exit_action.setValue('\n')
    self.obj45.exit_action.setHeight(15)

    # enter_action
    self.obj45.enter_action.setValue('\n')
    self.obj45.enter_action.setHeight(15)

    self.obj45.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(760.0,580.0,self.obj45)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,772.0,583.0,790.0,601.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,783.125,610.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmOff')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font180792460')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj45.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj45)
    self.globalAndLocalPostcondition(self.obj45, rootNode)
    self.obj45.postAction( rootNode.CREATE )

    self.obj46=Basic(self)
    self.obj46.isGraphObjectVisual = True

    if(hasattr(self.obj46, '_setHierarchicalLink')):
      self.obj46._setHierarchicalLink(False)

    # is_default
    self.obj46.is_default.setValue((None, 0))
    self.obj46.is_default.config = 0

    # name
    self.obj46.name.setValue('AlarmOn')

    # exit_action
    self.obj46.exit_action.setValue('\n')
    self.obj46.exit_action.setHeight(15)

    # enter_action
    self.obj46.enter_action.setValue('\n')
    self.obj46.enter_action.setHeight(15)

    self.obj46.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(940.0,580.0,self.obj46)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,952.0,583.0,970.0,601.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,963.125,610.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmOn')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font180792172')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj46.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj46)
    self.globalAndLocalPostcondition(self.obj46, rootNode)
    self.obj46.postAction( rootNode.CREATE )

    self.obj47=Basic(self)
    self.obj47.isGraphObjectVisual = True

    if(hasattr(self.obj47, '_setHierarchicalLink')):
      self.obj47._setHierarchicalLink(False)

    # is_default
    self.obj47.is_default.setValue((None, 0))
    self.obj47.is_default.config = 0

    # name
    self.obj47.name.setValue('AlarmStart')

    # exit_action
    self.obj47.exit_action.setValue('\n')
    self.obj47.exit_action.setHeight(15)

    # enter_action
    self.obj47.enter_action.setValue('\n')
    self.obj47.enter_action.setHeight(15)

    self.obj47.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1080.0,580.0,self.obj47)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1092.0,583.0,1110.0,601.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1103.125,610.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmStart')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font187842668')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj47.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj47)
    self.globalAndLocalPostcondition(self.obj47, rootNode)
    self.obj47.postAction( rootNode.CREATE )

    self.obj48=Basic(self)
    self.obj48.isGraphObjectVisual = True

    if(hasattr(self.obj48, '_setHierarchicalLink')):
      self.obj48._setHierarchicalLink(False)

    # is_default
    self.obj48.is_default.setValue((None, 1))
    self.obj48.is_default.config = 0

    # name
    self.obj48.name.setValue('Start')

    # exit_action
    self.obj48.exit_action.setValue('\n')
    self.obj48.exit_action.setHeight(15)

    # enter_action
    self.obj48.enter_action.setValue('\n')
    self.obj48.enter_action.setHeight(15)

    self.obj48.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(120.0,420.0,self.obj48)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,132.0,423.0,150.0,441.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKGREEN')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,143.125,450.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Start')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font162174796')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj48.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj48)
    self.globalAndLocalPostcondition(self.obj48, rootNode)
    self.obj48.postAction( rootNode.CREATE )

    self.obj49=Basic(self)
    self.obj49.isGraphObjectVisual = True

    if(hasattr(self.obj49, '_setHierarchicalLink')):
      self.obj49._setHierarchicalLink(False)

    # is_default
    self.obj49.is_default.setValue((None, 0))
    self.obj49.is_default.config = 0

    # name
    self.obj49.name.setValue('Stop')

    # exit_action
    self.obj49.exit_action.setValue('\n')
    self.obj49.exit_action.setHeight(15)

    # enter_action
    self.obj49.enter_action.setValue('\n')
    self.obj49.enter_action.setHeight(15)

    self.obj49.graphClass_= graph_Basic
    if self.genGraphics:
       new_obj = graph_Basic(1760.0,420.0,self.obj49)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Basic", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf3.handler,1772.0,423.0,1790.0,441.0)
       self.UMLmodel.itemconfig(new_obj.gf3.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, outline='DARKBLUE')
       self.UMLmodel.itemconfig(new_obj.gf3.handler, fill='lightgray')
       self.UMLmodel.coords(new_obj.gf1.handler,1783.125,450.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Stop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='font162158284')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj49.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj49)
    self.globalAndLocalPostcondition(self.obj49, rootNode)
    self.obj49.postAction( rootNode.CREATE )

    self.obj52=Orthogonal(self)
    self.obj52.isGraphObjectVisual = True

    if(hasattr(self.obj52, '_setHierarchicalLink')):
      self.obj52._setHierarchicalLink(False)

    # visible
    self.obj52.visible.setValue((None, 1))
    self.obj52.visible.config = 0

    # name
    self.obj52.name.setValue('Main timer loop')

    # auto_adjust
    self.obj52.auto_adjust.setValue((None, 1))
    self.obj52.auto_adjust.config = 0

    self.obj52.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(1040.0,40.0,self.obj52)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,1154.0,114.0,1252.0,204.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,1154.0,107.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Main timer loop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj52.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj52)
    self.globalAndLocalPostcondition(self.obj52, rootNode)
    self.obj52.postAction( rootNode.CREATE )

    self.obj53=Orthogonal(self)
    self.obj53.isGraphObjectVisual = True

    if(hasattr(self.obj53, '_setHierarchicalLink')):
      self.obj53._setHierarchicalLink(False)

    # visible
    self.obj53.visible.setValue((None, 1))
    self.obj53.visible.config = 0

    # name
    self.obj53.name.setValue('Displayloop')

    # auto_adjust
    self.obj53.auto_adjust.setValue((None, 1))
    self.obj53.auto_adjust.config = 0

    self.obj53.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(400.0,320.0,self.obj53)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,344.0,83.0,1074.0,364.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,344.0,76.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Displayloop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj53.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj53)
    self.globalAndLocalPostcondition(self.obj53, rootNode)
    self.obj53.postAction( rootNode.CREATE )

    self.obj54=Orthogonal(self)
    self.obj54.isGraphObjectVisual = True

    if(hasattr(self.obj54, '_setHierarchicalLink')):
      self.obj54._setHierarchicalLink(False)

    # visible
    self.obj54.visible.setValue((None, 1))
    self.obj54.visible.config = 0

    # name
    self.obj54.name.setValue('Backlight')

    # auto_adjust
    self.obj54.auto_adjust.setValue((None, 1))
    self.obj54.auto_adjust.config = 0

    self.obj54.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(360.0,260.0,self.obj54)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,370.0,417.0,676.0,584.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,370.0,410.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Backlight')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 0.83
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj54.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj54)
    self.globalAndLocalPostcondition(self.obj54, rootNode)
    self.obj54.postAction( rootNode.CREATE )

    self.obj55=Orthogonal(self)
    self.obj55.isGraphObjectVisual = True

    if(hasattr(self.obj55, '_setHierarchicalLink')):
      self.obj55._setHierarchicalLink(False)

    # visible
    self.obj55.visible.setValue((None, 1))
    self.obj55.visible.config = 0

    # name
    self.obj55.name.setValue('Chrono loop')

    # auto_adjust
    self.obj55.auto_adjust.setValue((None, 1))
    self.obj55.auto_adjust.config = 0

    self.obj55.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(660.0,380.0,self.obj55)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,769.0,412.0,1245.0,538.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='DARKGRAY')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,769.0,405.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Chrono loop')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj55.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj55)
    self.globalAndLocalPostcondition(self.obj55, rootNode)
    self.obj55.postAction( rootNode.CREATE )

    self.obj56=Orthogonal(self)
    self.obj56.isGraphObjectVisual = True

    if(hasattr(self.obj56, '_setHierarchicalLink')):
      self.obj56._setHierarchicalLink(False)

    # visible
    self.obj56.visible.setValue((None, 1))
    self.obj56.visible.config = 0

    # name
    self.obj56.name.setValue('Alarm')

    # auto_adjust
    self.obj56.auto_adjust.setValue((None, 1))
    self.obj56.auto_adjust.config = 0

    self.obj56.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(1100.0,660.0,self.obj56)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,1193.0,579.0,1586.0,727.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='darkgray')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,1193.0,572.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='Alarm')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj56.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj56)
    self.globalAndLocalPostcondition(self.obj56, rootNode)
    self.obj56.postAction( rootNode.CREATE )

    self.obj57=Orthogonal(self)
    self.obj57.isGraphObjectVisual = True

    if(hasattr(self.obj57, '_setHierarchicalLink')):
      self.obj57._setHierarchicalLink(False)

    # visible
    self.obj57.visible.setValue((None, 1))
    self.obj57.visible.config = 0

    # name
    self.obj57.name.setValue('AlarmState')

    # auto_adjust
    self.obj57.auto_adjust.setValue((None, 1))
    self.obj57.auto_adjust.config = 0

    self.obj57.graphClass_= graph_Orthogonal
    if self.genGraphics:
       new_obj = graph_Orthogonal(840.0,420.0,self.obj57)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Orthogonal", new_obj.tag)
       self.UMLmodel.coords(new_obj.gf5.handler,747.0,567.0,1146.0,782.0)
       self.UMLmodel.itemconfig(new_obj.gf5.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, width='2.0')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, outline='darkgray')
       self.UMLmodel.itemconfig(new_obj.gf5.handler, fill='')
       self.UMLmodel.coords(new_obj.gf1.handler,747.0,560.0)
       self.UMLmodel.itemconfig(new_obj.gf1.handler, stipple='')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, fill='black')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, text='AlarmState')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, width='0')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, font='Helvetica -12')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, anchor='center')
       self.UMLmodel.itemconfig(new_obj.gf1.handler, justify='left')
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj57.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj57)
    self.globalAndLocalPostcondition(self.obj57, rootNode)
    self.obj57.postAction( rootNode.CREATE )

    self.obj59=contains(self)
    self.obj59.isGraphObjectVisual = True

    if(hasattr(self.obj59, '_setHierarchicalLink')):
      self.obj59._setHierarchicalLink(False)

    self.obj59.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1154.37398229,-16.915951958,self.obj59)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj59.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj59)
    self.globalAndLocalPostcondition(self.obj59, rootNode)
    self.obj59.postAction( rootNode.CREATE )

    self.obj60=contains(self)
    self.obj60.isGraphObjectVisual = True

    if(hasattr(self.obj60, '_setHierarchicalLink')):
      self.obj60._setHierarchicalLink(False)

    self.obj60.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(433.005748984,279.53909536,self.obj60)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj60.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj60)
    self.globalAndLocalPostcondition(self.obj60, rootNode)
    self.obj60.postAction( rootNode.CREATE )

    self.obj61=contains(self)
    self.obj61.isGraphObjectVisual = True

    if(hasattr(self.obj61, '_setHierarchicalLink')):
      self.obj61._setHierarchicalLink(False)

    self.obj61.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(-258.41125024,447.51228148,self.obj61)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj61.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj61)
    self.globalAndLocalPostcondition(self.obj61, rootNode)
    self.obj61.postAction( rootNode.CREATE )

    self.obj62=contains(self)
    self.obj62.isGraphObjectVisual = True

    if(hasattr(self.obj62, '_setHierarchicalLink')):
      self.obj62._setHierarchicalLink(False)

    self.obj62.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(-155.49425102,509.53909536,self.obj62)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj62.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj62)
    self.globalAndLocalPostcondition(self.obj62, rootNode)
    self.obj62.postAction( rootNode.CREATE )

    self.obj63=contains(self)
    self.obj63.isGraphObjectVisual = True

    if(hasattr(self.obj63, '_setHierarchicalLink')):
      self.obj63._setHierarchicalLink(False)

    self.obj63.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(-152.91125024,730.01228148,self.obj63)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj63.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj63)
    self.globalAndLocalPostcondition(self.obj63, rootNode)
    self.obj63.postAction( rootNode.CREATE )

    self.obj64=contains(self)
    self.obj64.isGraphObjectVisual = True

    if(hasattr(self.obj64, '_setHierarchicalLink')):
      self.obj64._setHierarchicalLink(False)

    self.obj64.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(476.78301645,208.443472332,self.obj64)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj64.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj64)
    self.globalAndLocalPostcondition(self.obj64, rootNode)
    self.obj64.postAction( rootNode.CREATE )

    self.obj65=contains(self)
    self.obj65.isGraphObjectVisual = True

    if(hasattr(self.obj65, '_setHierarchicalLink')):
      self.obj65._setHierarchicalLink(False)

    self.obj65.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1012.67160001,454.809895833,self.obj65)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj65.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj65)
    self.globalAndLocalPostcondition(self.obj65, rootNode)
    self.obj65.postAction( rootNode.CREATE )

    self.obj66=contains(self)
    self.obj66.isGraphObjectVisual = True

    if(hasattr(self.obj66, '_setHierarchicalLink')):
      self.obj66._setHierarchicalLink(False)

    self.obj66.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1187.00574898,499.53909536,self.obj66)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj66.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj66)
    self.globalAndLocalPostcondition(self.obj66, rootNode)
    self.obj66.postAction( rootNode.CREATE )

    self.obj67=contains(self)
    self.obj67.isGraphObjectVisual = True

    if(hasattr(self.obj67, '_setHierarchicalLink')):
      self.obj67._setHierarchicalLink(False)

    self.obj67.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(757.671600007,244.309895833,self.obj67)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj67.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj67)
    self.globalAndLocalPostcondition(self.obj67, rootNode)
    self.obj67.postAction( rootNode.CREATE )

    self.obj68=contains(self)
    self.obj68.isGraphObjectVisual = True

    if(hasattr(self.obj68, '_setHierarchicalLink')):
      self.obj68._setHierarchicalLink(False)

    self.obj68.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1122.78301645,129.443472332,self.obj68)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj68.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj68)
    self.globalAndLocalPostcondition(self.obj68, rootNode)
    self.obj68.postAction( rootNode.CREATE )

    self.obj69=contains(self)
    self.obj69.isGraphObjectVisual = True

    if(hasattr(self.obj69, '_setHierarchicalLink')):
      self.obj69._setHierarchicalLink(False)

    self.obj69.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1275.48966832,122.71762454,self.obj69)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj69.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj69)
    self.globalAndLocalPostcondition(self.obj69, rootNode)
    self.obj69.postAction( rootNode.CREATE )

    self.obj70=contains(self)
    self.obj70.isGraphObjectVisual = True

    if(hasattr(self.obj70, '_setHierarchicalLink')):
      self.obj70._setHierarchicalLink(False)

    self.obj70.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1410.00574898,223.03909536,self.obj70)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj70.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj70)
    self.globalAndLocalPostcondition(self.obj70, rootNode)
    self.obj70.postAction( rootNode.CREATE )

    self.obj71=contains(self)
    self.obj71.isGraphObjectVisual = True

    if(hasattr(self.obj71, '_setHierarchicalLink')):
      self.obj71._setHierarchicalLink(False)

    self.obj71.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(667.49483416,367.85881227,self.obj71)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj71.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj71)
    self.globalAndLocalPostcondition(self.obj71, rootNode)
    self.obj71.postAction( rootNode.CREATE )

    self.obj72=contains(self)
    self.obj72.isGraphObjectVisual = True

    if(hasattr(self.obj72, '_setHierarchicalLink')):
      self.obj72._setHierarchicalLink(False)

    self.obj72.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1231.78301645,878.443472332,self.obj72)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj72.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj72)
    self.globalAndLocalPostcondition(self.obj72, rootNode)
    self.obj72.postAction( rootNode.CREATE )

    self.obj73=contains(self)
    self.obj73.isGraphObjectVisual = True

    if(hasattr(self.obj73, '_setHierarchicalLink')):
      self.obj73._setHierarchicalLink(False)

    self.obj73.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1463.00047774,839.241255093,self.obj73)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj73.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj73)
    self.globalAndLocalPostcondition(self.obj73, rootNode)
    self.obj73.postAction( rootNode.CREATE )

    self.obj74=contains(self)
    self.obj74.isGraphObjectVisual = True

    if(hasattr(self.obj74, '_setHierarchicalLink')):
      self.obj74._setHierarchicalLink(False)

    self.obj74.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(1403.17160001,892.309895833,self.obj74)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj74.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj74)
    self.globalAndLocalPostcondition(self.obj74, rootNode)
    self.obj74.postAction( rootNode.CREATE )

    self.obj75=contains(self)
    self.obj75.isGraphObjectVisual = True

    if(hasattr(self.obj75, '_setHierarchicalLink')):
      self.obj75._setHierarchicalLink(False)

    self.obj75.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(631.166756321,475.521548042,self.obj75)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj75.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj75)
    self.globalAndLocalPostcondition(self.obj75, rootNode)
    self.obj75.postAction( rootNode.CREATE )

    self.obj76=contains(self)
    self.obj76.isGraphObjectVisual = True

    if(hasattr(self.obj76, '_setHierarchicalLink')):
      self.obj76._setHierarchicalLink(False)

    self.obj76.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(685.005748984,539.53909536,self.obj76)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj76.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj76)
    self.globalAndLocalPostcondition(self.obj76, rootNode)
    self.obj76.postAction( rootNode.CREATE )

    self.obj77=contains(self)
    self.obj77.isGraphObjectVisual = True

    if(hasattr(self.obj77, '_setHierarchicalLink')):
      self.obj77._setHierarchicalLink(False)

    self.obj77.graphClass_= graph_contains
    if self.genGraphics:
       new_obj = graph_contains(836.50574898,576.53909536,self.obj77)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("contains", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj77.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj77)
    self.globalAndLocalPostcondition(self.obj77, rootNode)
    self.obj77.postAction( rootNode.CREATE )

    self.obj80=Hyperedge(self)
    self.obj80.isGraphObjectVisual = True

    if(hasattr(self.obj80, '_setHierarchicalLink')):
      self.obj80._setHierarchicalLink(False)

    # name
    self.obj80.name.setValue('')
    self.obj80.name.setNone()

    # broadcast
    self.obj80.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj80.broadcast.setHeight(15)

    # guard
    self.obj80.guard.setValue('1')

    # trigger
    self.obj80.trigger.setValue('AFTER(1)')

    # action
    self.obj80.action.setValue('controller.increaseTimeByOne()\n')
    self.obj80.action.setHeight(15)

    # broadcast_to
    self.obj80.broadcast_to.setValue('')
    self.obj80.broadcast_to.setNone()

    # display
    self.obj80.display.setValue('tm(1s)')

    self.obj80.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1200.0,122.0,self.obj80)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj80.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj80)
    self.globalAndLocalPostcondition(self.obj80, rootNode)
    self.obj80.postAction( rootNode.CREATE )

    self.obj81=Hyperedge(self)
    self.obj81.isGraphObjectVisual = True

    if(hasattr(self.obj81, '_setHierarchicalLink')):
      self.obj81._setHierarchicalLink(False)

    # name
    self.obj81.name.setValue('')
    self.obj81.name.setNone()

    # broadcast
    self.obj81.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj81.broadcast.setHeight(15)

    # guard
    self.obj81.guard.setValue('1')

    # trigger
    self.obj81.trigger.setValue('AFTER(1)')

    # action
    self.obj81.action.setValue('controller.refreshTimeDisplay()\n')
    self.obj81.action.setHeight(15)

    # broadcast_to
    self.obj81.broadcast_to.setValue('')
    self.obj81.broadcast_to.setNone()

    # display
    self.obj81.display.setValue('tm(1s)')

    self.obj81.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(521.0,299.0,self.obj81)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 0.97
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj81.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj81)
    self.globalAndLocalPostcondition(self.obj81, rootNode)
    self.obj81.postAction( rootNode.CREATE )

    self.obj82=Hyperedge(self)
    self.obj82.isGraphObjectVisual = True

    if(hasattr(self.obj82, '_setHierarchicalLink')):
      self.obj82._setHierarchicalLink(False)

    # name
    self.obj82.name.setValue('')
    self.obj82.name.setNone()

    # broadcast
    self.obj82.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj82.broadcast.setHeight(15)

    # guard
    self.obj82.guard.setValue('1')

    # trigger
    self.obj82.trigger.setValue('topRightPressed')

    # action
    self.obj82.action.setValue('controller.setIndiglo()\n')
    self.obj82.action.setHeight(15)

    # broadcast_to
    self.obj82.broadcast_to.setValue('')
    self.obj82.broadcast_to.setNone()

    # display
    self.obj82.display.setValue('topRightPressed/lighton')

    self.obj82.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(521.00160692,442.021604248,self.obj82)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj82.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj82)
    self.globalAndLocalPostcondition(self.obj82, rootNode)
    self.obj82.postAction( rootNode.CREATE )

    self.obj83=Hyperedge(self)
    self.obj83.isGraphObjectVisual = True

    if(hasattr(self.obj83, '_setHierarchicalLink')):
      self.obj83._setHierarchicalLink(False)

    # name
    self.obj83.name.setValue('')
    self.obj83.name.setNone()

    # broadcast
    self.obj83.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj83.broadcast.setHeight(15)

    # guard
    self.obj83.guard.setValue('1')

    # trigger
    self.obj83.trigger.setValue('topRightReleased')

    # action
    self.obj83.action.setValue('\n')
    self.obj83.action.setHeight(15)

    # broadcast_to
    self.obj83.broadcast_to.setValue('')
    self.obj83.broadcast_to.setNone()

    # display
    self.obj83.display.setValue('topRightReleased')

    self.obj83.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(432.76366772,497.6808835,self.obj83)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj83.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj83)
    self.globalAndLocalPostcondition(self.obj83, rootNode)
    self.obj83.postAction( rootNode.CREATE )

    self.obj84=Hyperedge(self)
    self.obj84.isGraphObjectVisual = True

    if(hasattr(self.obj84, '_setHierarchicalLink')):
      self.obj84._setHierarchicalLink(False)

    # name
    self.obj84.name.setValue('')
    self.obj84.name.setNone()

    # broadcast
    self.obj84.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj84.broadcast.setHeight(15)

    # guard
    self.obj84.guard.setValue('1')

    # trigger
    self.obj84.trigger.setValue('AFTER(2)')

    # action
    self.obj84.action.setValue('controller.unsetIndiglo()\n')
    self.obj84.action.setHeight(15)

    # broadcast_to
    self.obj84.broadcast_to.setValue('')
    self.obj84.broadcast_to.setNone()

    # display
    self.obj84.display.setValue('tm(2s)')

    self.obj84.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(554.8442126,484.045289636,self.obj84)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj84.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj84)
    self.globalAndLocalPostcondition(self.obj84, rootNode)
    self.obj84.postAction( rootNode.CREATE )

    self.obj85=Hyperedge(self)
    self.obj85.isGraphObjectVisual = True

    if(hasattr(self.obj85, '_setHierarchicalLink')):
      self.obj85._setHierarchicalLink(False)

    # name
    self.obj85.name.setValue('')
    self.obj85.name.setNone()

    # broadcast
    self.obj85.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj85.broadcast.setHeight(15)

    # guard
    self.obj85.guard.setValue('1')

    # trigger
    self.obj85.trigger.setValue('topLeftPressed')

    # action
    self.obj85.action.setValue('controller.refreshChronoDisplay()\n')
    self.obj85.action.setHeight(15)

    # broadcast_to
    self.obj85.broadcast_to.setValue('')
    self.obj85.broadcast_to.setNone()

    # display
    self.obj85.display.setValue('topLeftPressed/showChrono')

    self.obj85.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(400.529982213,202.43327707,self.obj85)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj85.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj85)
    self.globalAndLocalPostcondition(self.obj85, rootNode)
    self.obj85.postAction( rootNode.CREATE )

    self.obj86=Hyperedge(self)
    self.obj86.isGraphObjectVisual = True

    if(hasattr(self.obj86, '_setHierarchicalLink')):
      self.obj86._setHierarchicalLink(False)

    # name
    self.obj86.name.setValue('')
    self.obj86.name.setNone()

    # broadcast
    self.obj86.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj86.broadcast.setHeight(15)

    # guard
    self.obj86.guard.setValue('1')

    # trigger
    self.obj86.trigger.setValue('topLeftPressed')

    # action
    self.obj86.action.setValue('controller.refreshTimeDisplay()\n')
    self.obj86.action.setHeight(15)

    # broadcast_to
    self.obj86.broadcast_to.setValue('')
    self.obj86.broadcast_to.setNone()

    # display
    self.obj86.display.setValue('topLeftPressed/showTime')

    self.obj86.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(468.494563329,174.621763677,self.obj86)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj86.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj86)
    self.globalAndLocalPostcondition(self.obj86, rootNode)
    self.obj86.postAction( rootNode.CREATE )

    self.obj87=Hyperedge(self)
    self.obj87.isGraphObjectVisual = True

    if(hasattr(self.obj87, '_setHierarchicalLink')):
      self.obj87._setHierarchicalLink(False)

    # name
    self.obj87.name.setValue('')
    self.obj87.name.setNone()

    # broadcast
    self.obj87.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj87.broadcast.setHeight(15)

    # guard
    self.obj87.guard.setValue('[INSTATE("DigitalWatch.Displayloop.Chrono", 0)]')

    # trigger
    self.obj87.trigger.setValue('bottomRightPressed')

    # action
    self.obj87.action.setValue('\n')
    self.obj87.action.setHeight(15)

    # broadcast_to
    self.obj87.broadcast_to.setValue('')
    self.obj87.broadcast_to.setNone()

    # display
    self.obj87.display.setValue('bottomRightPressed/startChrono')

    self.obj87.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(930.01594892,471.972404683,self.obj87)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj87.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj87)
    self.globalAndLocalPostcondition(self.obj87, rootNode)
    self.obj87.postAction( rootNode.CREATE )

    self.obj88=Hyperedge(self)
    self.obj88.isGraphObjectVisual = True

    if(hasattr(self.obj88, '_setHierarchicalLink')):
      self.obj88._setHierarchicalLink(False)

    # name
    self.obj88.name.setValue('')
    self.obj88.name.setNone()

    # broadcast
    self.obj88.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj88.broadcast.setHeight(15)

    # guard
    self.obj88.guard.setValue('1')

    # trigger
    self.obj88.trigger.setValue('AFTER(0.25)')

    # action
    self.obj88.action.setValue('controller.increaseChronoByOne()\n')
    self.obj88.action.setHeight(15)

    # broadcast_to
    self.obj88.broadcast_to.setValue('')
    self.obj88.broadcast_to.setNone()

    # display
    self.obj88.display.setValue('tm(0.01s)')

    self.obj88.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1043.0,510.0,self.obj88)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj88.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj88)
    self.globalAndLocalPostcondition(self.obj88, rootNode)
    self.obj88.postAction( rootNode.CREATE )

    self.obj89=Hyperedge(self)
    self.obj89.isGraphObjectVisual = True

    if(hasattr(self.obj89, '_setHierarchicalLink')):
      self.obj89._setHierarchicalLink(False)

    # name
    self.obj89.name.setValue('')
    self.obj89.name.setNone()

    # broadcast
    self.obj89.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj89.broadcast.setHeight(15)

    # guard
    self.obj89.guard.setValue('[INSTATE("DigitalWatch.Displayloop.Chrono", 0)]')

    # trigger
    self.obj89.trigger.setValue('bottomLeftPressed')

    # action
    self.obj89.action.setValue('controller.resetChrono()\ncontroller.refreshChronoDisplay()\n')
    self.obj89.action.setHeight(15)

    # broadcast_to
    self.obj89.broadcast_to.setValue('')
    self.obj89.broadcast_to.setNone()

    # display
    self.obj89.display.setValue('bottomLeftPressed/reset')

    self.obj89.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1143.0,475.0,self.obj89)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj89.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj89)
    self.globalAndLocalPostcondition(self.obj89, rootNode)
    self.obj89.postAction( rootNode.CREATE )

    self.obj90=Hyperedge(self)
    self.obj90.isGraphObjectVisual = True

    if(hasattr(self.obj90, '_setHierarchicalLink')):
      self.obj90._setHierarchicalLink(False)

    # name
    self.obj90.name.setValue('')
    self.obj90.name.setNone()

    # broadcast
    self.obj90.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj90.broadcast.setHeight(15)

    # guard
    self.obj90.guard.setValue('1')

    # trigger
    self.obj90.trigger.setValue('bottomRightPressed')

    # action
    self.obj90.action.setValue('\n')
    self.obj90.action.setHeight(15)

    # broadcast_to
    self.obj90.broadcast_to.setValue('')
    self.obj90.broadcast_to.setNone()

    # display
    self.obj90.display.setValue('bottomRightPressed/pauseChrono')

    self.obj90.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(936.99746,426.07080641,self.obj90)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj90.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj90)
    self.globalAndLocalPostcondition(self.obj90, rootNode)
    self.obj90.postAction( rootNode.CREATE )

    self.obj91=Hyperedge(self)
    self.obj91.isGraphObjectVisual = True

    if(hasattr(self.obj91, '_setHierarchicalLink')):
      self.obj91._setHierarchicalLink(False)

    # name
    self.obj91.name.setValue('')
    self.obj91.name.setNone()

    # broadcast
    self.obj91.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj91.broadcast.setHeight(15)

    # guard
    self.obj91.guard.setValue('[INSTATE("DigitalWatch.Displayloop.Chrono", 0)]')

    # trigger
    self.obj91.trigger.setValue('bottomLeftPressed')

    # action
    self.obj91.action.setValue('controller.resetChrono()\ncontroller.refreshChronoDisplay()\n')
    self.obj91.action.setHeight(15)

    # broadcast_to
    self.obj91.broadcast_to.setValue('')
    self.obj91.broadcast_to.setNone()

    # display
    self.obj91.display.setValue('bottomLeftPressed/resetChrono')

    self.obj91.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(823.0,490.0,self.obj91)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj91.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj91)
    self.globalAndLocalPostcondition(self.obj91, rootNode)
    self.obj91.postAction( rootNode.CREATE )

    self.obj92=Hyperedge(self)
    self.obj92.isGraphObjectVisual = True

    if(hasattr(self.obj92, '_setHierarchicalLink')):
      self.obj92._setHierarchicalLink(False)

    # name
    self.obj92.name.setValue('')
    self.obj92.name.setNone()

    # broadcast
    self.obj92.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj92.broadcast.setHeight(15)

    # guard
    self.obj92.guard.setValue('1')

    # trigger
    self.obj92.trigger.setValue('AFTER(0.01)')

    # action
    self.obj92.action.setValue('controller.refreshChronoDisplay()\n')
    self.obj92.action.setHeight(15)

    # broadcast_to
    self.obj92.broadcast_to.setValue('')
    self.obj92.broadcast_to.setNone()

    # display
    self.obj92.display.setValue('refreshChronoDisplay')

    self.obj92.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(422.0,92.0,self.obj92)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj92.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj92)
    self.globalAndLocalPostcondition(self.obj92, rootNode)
    self.obj92.postAction( rootNode.CREATE )

    self.obj93=Hyperedge(self)
    self.obj93.isGraphObjectVisual = True

    if(hasattr(self.obj93, '_setHierarchicalLink')):
      self.obj93._setHierarchicalLink(False)

    # name
    self.obj93.name.setValue('')
    self.obj93.name.setNone()

    # broadcast
    self.obj93.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj93.broadcast.setHeight(15)

    # guard
    self.obj93.guard.setValue('1')

    # trigger
    self.obj93.trigger.setValue('bottomRightPressed')

    # action
    self.obj93.action.setValue('\n')
    self.obj93.action.setHeight(15)

    # broadcast_to
    self.obj93.broadcast_to.setValue('')
    self.obj93.broadcast_to.setNone()

    # display
    self.obj93.display.setValue('bottomRightPressed/EnterTimeEdit')

    self.obj93.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(640.191269441,191.778068391,self.obj93)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj93.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj93)
    self.globalAndLocalPostcondition(self.obj93, rootNode)
    self.obj93.postAction( rootNode.CREATE )

    self.obj94=Hyperedge(self)
    self.obj94.isGraphObjectVisual = True

    if(hasattr(self.obj94, '_setHierarchicalLink')):
      self.obj94._setHierarchicalLink(False)

    # name
    self.obj94.name.setValue('')
    self.obj94.name.setNone()

    # broadcast
    self.obj94.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj94.broadcast.setHeight(15)

    # guard
    self.obj94.guard.setValue('1')

    # trigger
    self.obj94.trigger.setValue('bottomRightReleased')

    # action
    self.obj94.action.setValue('\n')
    self.obj94.action.setHeight(15)

    # broadcast_to
    self.obj94.broadcast_to.setValue('')
    self.obj94.broadcast_to.setNone()

    # display
    self.obj94.display.setValue('bottomRightReleased/Time')

    self.obj94.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(616.708276102,162.151972356,self.obj94)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj94.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj94)
    self.globalAndLocalPostcondition(self.obj94, rootNode)
    self.obj94.postAction( rootNode.CREATE )

    self.obj95=Hyperedge(self)
    self.obj95.isGraphObjectVisual = True

    if(hasattr(self.obj95, '_setHierarchicalLink')):
      self.obj95._setHierarchicalLink(False)

    # name
    self.obj95.name.setValue('')
    self.obj95.name.setNone()

    # broadcast
    self.obj95.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj95.broadcast.setHeight(15)

    # guard
    self.obj95.guard.setValue('1')

    # trigger
    self.obj95.trigger.setValue('bottomLeftPressed')

    # action
    self.obj95.action.setValue('controller.setAlarm()\n[EVENT("setAlarm")]\n')
    self.obj95.action.setHeight(15)

    # broadcast_to
    self.obj95.broadcast_to.setValue('')
    self.obj95.broadcast_to.setNone()

    # display
    self.obj95.display.setValue('bottomLeftPressed/toggleAlarm')

    self.obj95.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(644.046954461,278.658285288,self.obj95)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj95.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj95)
    self.globalAndLocalPostcondition(self.obj95, rootNode)
    self.obj95.postAction( rootNode.CREATE )

    self.obj96=Hyperedge(self)
    self.obj96.isGraphObjectVisual = True

    if(hasattr(self.obj96, '_setHierarchicalLink')):
      self.obj96._setHierarchicalLink(False)

    # name
    self.obj96.name.setValue('')
    self.obj96.name.setNone()

    # broadcast
    self.obj96.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj96.broadcast.setHeight(15)

    # guard
    self.obj96.guard.setValue('1')

    # trigger
    self.obj96.trigger.setValue('bottomLeftReleased')

    # action
    self.obj96.action.setValue('\n')
    self.obj96.action.setHeight(15)

    # broadcast_to
    self.obj96.broadcast_to.setValue('')
    self.obj96.broadcast_to.setNone()

    # display
    self.obj96.display.setValue('bottomLeftReleased/Time')

    self.obj96.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(614.673759346,312.697606353,self.obj96)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 1.34
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj96.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj96)
    self.globalAndLocalPostcondition(self.obj96, rootNode)
    self.obj96.postAction( rootNode.CREATE )

    self.obj97=Hyperedge(self)
    self.obj97.isGraphObjectVisual = True

    if(hasattr(self.obj97, '_setHierarchicalLink')):
      self.obj97._setHierarchicalLink(False)

    # name
    self.obj97.name.setValue('')
    self.obj97.name.setNone()

    # broadcast
    self.obj97.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj97.broadcast.setHeight(15)

    # guard
    self.obj97.guard.setValue('1')

    # trigger
    self.obj97.trigger.setValue('AFTER(0.3)')

    # action
    self.obj97.action.setValue('\ncontroller.increaseSelection()\n')
    self.obj97.action.setHeight(15)

    # broadcast_to
    self.obj97.broadcast_to.setValue('')
    self.obj97.broadcast_to.setNone()

    # display
    self.obj97.display.setValue('tm(0.3s)')

    self.obj97.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1017.0,112.5,self.obj97)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj97.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj97)
    self.globalAndLocalPostcondition(self.obj97, rootNode)
    self.obj97.postAction( rootNode.CREATE )

    self.obj98=Hyperedge(self)
    self.obj98.isGraphObjectVisual = True

    if(hasattr(self.obj98, '_setHierarchicalLink')):
      self.obj98._setHierarchicalLink(False)

    # name
    self.obj98.name.setValue('')
    self.obj98.name.setNone()

    # broadcast
    self.obj98.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj98.broadcast.setHeight(15)

    # guard
    self.obj98.guard.setValue('1')

    # trigger
    self.obj98.trigger.setValue('AFTER(1.5)')

    # action
    self.obj98.action.setValue('\ncontroller.startSelection()\n')
    self.obj98.action.setHeight(15)

    # broadcast_to
    self.obj98.broadcast_to.setValue('')
    self.obj98.broadcast_to.setNone()

    # display
    self.obj98.display.setValue('tm(1.5s)')

    self.obj98.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(779.839814604,153.222206491,self.obj98)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj98.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj98)
    self.globalAndLocalPostcondition(self.obj98, rootNode)
    self.obj98.postAction( rootNode.CREATE )

    self.obj99=Hyperedge(self)
    self.obj99.isGraphObjectVisual = True

    if(hasattr(self.obj99, '_setHierarchicalLink')):
      self.obj99._setHierarchicalLink(False)

    # name
    self.obj99.name.setValue('')
    self.obj99.name.setNone()

    # broadcast
    self.obj99.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj99.broadcast.setHeight(15)

    # guard
    self.obj99.guard.setValue('1')

    # trigger
    self.obj99.trigger.setValue('AFTER(1.5)')

    # action
    self.obj99.action.setValue('\ncontroller.refreshAlarmDisplay()\ncontroller.startSelection()\n')
    self.obj99.action.setHeight(15)

    # broadcast_to
    self.obj99.broadcast_to.setValue('')
    self.obj99.broadcast_to.setNone()

    # display
    self.obj99.display.setValue('tm(1.5s)')

    self.obj99.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(763.427438792,265.376052965,self.obj99)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj99.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj99)
    self.globalAndLocalPostcondition(self.obj99, rootNode)
    self.obj99.postAction( rootNode.CREATE )

    self.obj100=Hyperedge(self)
    self.obj100.isGraphObjectVisual = True

    if(hasattr(self.obj100, '_setHierarchicalLink')):
      self.obj100._setHierarchicalLink(False)

    # name
    self.obj100.name.setValue('')
    self.obj100.name.setNone()

    # broadcast
    self.obj100.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj100.broadcast.setHeight(15)

    # guard
    self.obj100.guard.setValue('1')

    # trigger
    self.obj100.trigger.setValue('bottomLeftPressed')

    # action
    self.obj100.action.setValue('\n\ncontroller.increaseSelection()\n')
    self.obj100.action.setHeight(15)

    # broadcast_to
    self.obj100.broadcast_to.setValue('')
    self.obj100.broadcast_to.setNone()

    # display
    self.obj100.display.setValue('bottomLeftPressed')

    self.obj100.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(903.622906591,164.601116864,self.obj100)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj100.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj100)
    self.globalAndLocalPostcondition(self.obj100, rootNode)
    self.obj100.postAction( rootNode.CREATE )

    self.obj101=Hyperedge(self)
    self.obj101.isGraphObjectVisual = True

    if(hasattr(self.obj101, '_setHierarchicalLink')):
      self.obj101._setHierarchicalLink(False)

    # name
    self.obj101.name.setValue('')
    self.obj101.name.setNone()

    # broadcast
    self.obj101.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj101.broadcast.setHeight(15)

    # guard
    self.obj101.guard.setValue('1')

    # trigger
    self.obj101.trigger.setValue('bottomLeftReleased')

    # action
    self.obj101.action.setValue('\n')
    self.obj101.action.setHeight(15)

    # broadcast_to
    self.obj101.broadcast_to.setValue('')
    self.obj101.broadcast_to.setNone()

    # display
    self.obj101.display.setValue('bottomLeftReleased')

    self.obj101.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(925.245311897,200.600067363,self.obj101)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj101.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj101)
    self.globalAndLocalPostcondition(self.obj101, rootNode)
    self.obj101.postAction( rootNode.CREATE )

    self.obj102=Hyperedge(self)
    self.obj102.isGraphObjectVisual = True

    if(hasattr(self.obj102, '_setHierarchicalLink')):
      self.obj102._setHierarchicalLink(False)

    # name
    self.obj102.name.setValue('')
    self.obj102.name.setNone()

    # broadcast
    self.obj102.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj102.broadcast.setHeight(15)

    # guard
    self.obj102.guard.setValue('1')

    # trigger
    self.obj102.trigger.setValue('bottomRightPressed')

    # action
    self.obj102.action.setValue('\n')
    self.obj102.action.setHeight(15)

    # broadcast_to
    self.obj102.broadcast_to.setValue('')
    self.obj102.broadcast_to.setNone()

    # display
    self.obj102.display.setValue('bottomRightPressed')

    self.obj102.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(911.204460874,272.62835995,self.obj102)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj102.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj102)
    self.globalAndLocalPostcondition(self.obj102, rootNode)
    self.obj102.postAction( rootNode.CREATE )

    self.obj103=Hyperedge(self)
    self.obj103.isGraphObjectVisual = True

    if(hasattr(self.obj103, '_setHierarchicalLink')):
      self.obj103._setHierarchicalLink(False)

    # name
    self.obj103.name.setValue('')
    self.obj103.name.setNone()

    # broadcast
    self.obj103.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj103.broadcast.setHeight(15)

    # guard
    self.obj103.guard.setValue('1')

    # trigger
    self.obj103.trigger.setValue('bottomRightReleased')

    # action
    self.obj103.action.setValue('\ncontroller.selectNext()\n')
    self.obj103.action.setHeight(15)

    # broadcast_to
    self.obj103.broadcast_to.setValue('')
    self.obj103.broadcast_to.setNone()

    # display
    self.obj103.display.setValue('bottomRightReleased')

    self.obj103.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(937.33627774,246.89620301,self.obj103)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['Text Scale'] = 0.89
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj103.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj103)
    self.globalAndLocalPostcondition(self.obj103, rootNode)
    self.obj103.postAction( rootNode.CREATE )

    self.obj104=Hyperedge(self)
    self.obj104.isGraphObjectVisual = True

    if(hasattr(self.obj104, '_setHierarchicalLink')):
      self.obj104._setHierarchicalLink(False)

    # name
    self.obj104.name.setValue('')
    self.obj104.name.setNone()

    # broadcast
    self.obj104.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj104.broadcast.setHeight(15)

    # guard
    self.obj104.guard.setValue('1')

    # trigger
    self.obj104.trigger.setValue('AFTER(5)')

    # action
    self.obj104.action.setValue('\n\ncontroller.stopSelection()\n')
    self.obj104.action.setHeight(15)

    # broadcast_to
    self.obj104.broadcast_to.setValue('')
    self.obj104.broadcast_to.setNone()

    # display
    self.obj104.display.setValue('tm(5s)')

    self.obj104.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(671.006704458,232.021605547,self.obj104)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj104.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj104)
    self.globalAndLocalPostcondition(self.obj104, rootNode)
    self.obj104.postAction( rootNode.CREATE )

    self.obj105=Hyperedge(self)
    self.obj105.isGraphObjectVisual = True

    if(hasattr(self.obj105, '_setHierarchicalLink')):
      self.obj105._setHierarchicalLink(False)

    # name
    self.obj105.name.setValue('')
    self.obj105.name.setNone()

    # broadcast
    self.obj105.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj105.broadcast.setHeight(15)

    # guard
    self.obj105.guard.setValue('1')

    # trigger
    self.obj105.trigger.setValue('AFTER(2)')

    # action
    self.obj105.action.setValue('\n\ncontroller.stopSelection()\n')
    self.obj105.action.setHeight(15)

    # broadcast_to
    self.obj105.broadcast_to.setValue('')
    self.obj105.broadcast_to.setNone()

    # display
    self.obj105.display.setValue('tm(2s)')

    self.obj105.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(770.432506722,282.006873775,self.obj105)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj105.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj105)
    self.globalAndLocalPostcondition(self.obj105, rootNode)
    self.obj105.postAction( rootNode.CREATE )

    self.obj106=Hyperedge(self)
    self.obj106.isGraphObjectVisual = True

    if(hasattr(self.obj106, '_setHierarchicalLink')):
      self.obj106._setHierarchicalLink(False)

    # name
    self.obj106.name.setValue('')
    self.obj106.name.setNone()

    # broadcast
    self.obj106.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj106.broadcast.setHeight(15)

    # guard
    self.obj106.guard.setValue('[INSTATE("DigitalWatch.AlarmState.AlarmOn", 0)]')

    # trigger
    self.obj106.trigger.setValue('AFTER(1)')

    # action
    self.obj106.action.setValue('\ncontroller.checkTime()\n')
    self.obj106.action.setHeight(15)

    # broadcast_to
    self.obj106.broadcast_to.setValue('')
    self.obj106.broadcast_to.setNone()

    # display
    self.obj106.display.setValue('tm(1s)/checkTime')

    self.obj106.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1240.0,588.5,self.obj106)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj106.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj106)
    self.globalAndLocalPostcondition(self.obj106, rootNode)
    self.obj106.postAction( rootNode.CREATE )

    self.obj107=Hyperedge(self)
    self.obj107.isGraphObjectVisual = True

    if(hasattr(self.obj107, '_setHierarchicalLink')):
      self.obj107._setHierarchicalLink(False)

    # name
    self.obj107.name.setValue('')
    self.obj107.name.setNone()

    # broadcast
    self.obj107.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj107.broadcast.setHeight(15)

    # guard
    self.obj107.guard.setValue('1')

    # trigger
    self.obj107.trigger.setValue('alarmStart')

    # action
    self.obj107.action.setValue('\ncontroller.setIndiglo()\n')
    self.obj107.action.setHeight(15)

    # broadcast_to
    self.obj107.broadcast_to.setValue('')
    self.obj107.broadcast_to.setNone()

    # display
    self.obj107.display.setValue('alarmStart')

    self.obj107.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1319.00335223,643.510802773,self.obj107)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj107.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj107)
    self.globalAndLocalPostcondition(self.obj107, rootNode)
    self.obj107.postAction( rootNode.CREATE )

    self.obj108=Hyperedge(self)
    self.obj108.isGraphObjectVisual = True

    if(hasattr(self.obj108, '_setHierarchicalLink')):
      self.obj108._setHierarchicalLink(False)

    # name
    self.obj108.name.setValue('')
    self.obj108.name.setNone()

    # broadcast
    self.obj108.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj108.broadcast.setHeight(15)

    # guard
    self.obj108.guard.setValue('1')

    # trigger
    self.obj108.trigger.setValue('setAlarm')

    # action
    self.obj108.action.setValue('\n')
    self.obj108.action.setHeight(15)

    # broadcast_to
    self.obj108.broadcast_to.setValue('')
    self.obj108.broadcast_to.setNone()

    # display
    self.obj108.display.setValue('setAlarm')

    self.obj108.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(871.065852229,574.778645833,self.obj108)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj108.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj108)
    self.globalAndLocalPostcondition(self.obj108, rootNode)
    self.obj108.postAction( rootNode.CREATE )

    self.obj109=Hyperedge(self)
    self.obj109.isGraphObjectVisual = True

    if(hasattr(self.obj109, '_setHierarchicalLink')):
      self.obj109._setHierarchicalLink(False)

    # name
    self.obj109.name.setValue('')
    self.obj109.name.setNone()

    # broadcast
    self.obj109.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj109.broadcast.setHeight(15)

    # guard
    self.obj109.guard.setValue('1')

    # trigger
    self.obj109.trigger.setValue('setAlarm')

    # action
    self.obj109.action.setValue('\n')
    self.obj109.action.setHeight(15)

    # broadcast_to
    self.obj109.broadcast_to.setValue('')
    self.obj109.broadcast_to.setNone()

    # display
    self.obj109.display.setValue('setAlarm')

    self.obj109.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(864.013689867,615.021607987,self.obj109)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj109.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj109)
    self.globalAndLocalPostcondition(self.obj109, rootNode)
    self.obj109.postAction( rootNode.CREATE )

    self.obj110=Hyperedge(self)
    self.obj110.isGraphObjectVisual = True

    if(hasattr(self.obj110, '_setHierarchicalLink')):
      self.obj110._setHierarchicalLink(False)

    # name
    self.obj110.name.setValue('')
    self.obj110.name.setNone()

    # broadcast
    self.obj110.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj110.broadcast.setHeight(15)

    # guard
    self.obj110.guard.setValue('1')

    # trigger
    self.obj110.trigger.setValue('AFTER(0.5)')

    # action
    self.obj110.action.setValue('\n\ncontroller.unsetIndiglo()\n')
    self.obj110.action.setHeight(15)

    # broadcast_to
    self.obj110.broadcast_to.setValue('')
    self.obj110.broadcast_to.setNone()

    # display
    self.obj110.display.setValue('tm(0.5s)/unset')

    self.obj110.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1469.56585223,638.278645833,self.obj110)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj110.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj110)
    self.globalAndLocalPostcondition(self.obj110, rootNode)
    self.obj110.postAction( rootNode.CREATE )

    self.obj111=Hyperedge(self)
    self.obj111.isGraphObjectVisual = True

    if(hasattr(self.obj111, '_setHierarchicalLink')):
      self.obj111._setHierarchicalLink(False)

    # name
    self.obj111.name.setValue('')
    self.obj111.name.setNone()

    # broadcast
    self.obj111.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj111.broadcast.setHeight(15)

    # guard
    self.obj111.guard.setValue('1')

    # trigger
    self.obj111.trigger.setValue('AFTER(0.5)')

    # action
    self.obj111.action.setValue('\ncontroller.setIndiglo()\n')
    self.obj111.action.setHeight(15)

    # broadcast_to
    self.obj111.broadcast_to.setValue('')
    self.obj111.broadcast_to.setNone()

    # display
    self.obj111.display.setValue('tm(0.5s)/set')

    self.obj111.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1467.84309461,666.580548436,self.obj111)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj111.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj111)
    self.globalAndLocalPostcondition(self.obj111, rootNode)
    self.obj111.postAction( rootNode.CREATE )

    self.obj112=Hyperedge(self)
    self.obj112.isGraphObjectVisual = True

    if(hasattr(self.obj112, '_setHierarchicalLink')):
      self.obj112._setHierarchicalLink(False)

    # name
    self.obj112.name.setValue('')
    self.obj112.name.setNone()

    # broadcast
    self.obj112.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj112.broadcast.setHeight(15)

    # guard
    self.obj112.guard.setValue('1')

    # trigger
    self.obj112.trigger.setValue('alarmStart')

    # action
    self.obj112.action.setValue('\n')
    self.obj112.action.setHeight(15)

    # broadcast_to
    self.obj112.broadcast_to.setValue('')
    self.obj112.broadcast_to.setNone()

    # display
    self.obj112.display.setValue('alarmStart')

    self.obj112.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1034.56585223,575.278645833,self.obj112)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj112.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj112)
    self.globalAndLocalPostcondition(self.obj112, rootNode)
    self.obj112.postAction( rootNode.CREATE )

    self.obj113=Hyperedge(self)
    self.obj113.isGraphObjectVisual = True

    if(hasattr(self.obj113, '_setHierarchicalLink')):
      self.obj113._setHierarchicalLink(False)

    # name
    self.obj113.name.setValue('')
    self.obj113.name.setNone()

    # broadcast
    self.obj113.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj113.broadcast.setHeight(15)

    # guard
    self.obj113.guard.setValue('1')

    # trigger
    self.obj113.trigger.setValue('AFTER(4)')

    # action
    self.obj113.action.setValue('\n[EVENT("alarmStop")]\ncontroller.setAlarm()\n')
    self.obj113.action.setHeight(15)

    # broadcast_to
    self.obj113.broadcast_to.setValue('')
    self.obj113.broadcast_to.setNone()

    # display
    self.obj113.display.setValue('tm(4s)/alarmStop')

    self.obj113.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(948.446420542,635.71762454,self.obj113)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj113.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj113)
    self.globalAndLocalPostcondition(self.obj113, rootNode)
    self.obj113.postAction( rootNode.CREATE )

    self.obj114=Hyperedge(self)
    self.obj114.isGraphObjectVisual = True

    if(hasattr(self.obj114, '_setHierarchicalLink')):
      self.obj114._setHierarchicalLink(False)

    # name
    self.obj114.name.setValue('')
    self.obj114.name.setNone()

    # broadcast
    self.obj114.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj114.broadcast.setHeight(15)

    # guard
    self.obj114.guard.setValue('1')

    # trigger
    self.obj114.trigger.setValue('alarmStop')

    # action
    self.obj114.action.setValue('\ncontroller.unsetIndiglo()\n')
    self.obj114.action.setHeight(15)

    # broadcast_to
    self.obj114.broadcast_to.setValue('')
    self.obj114.broadcast_to.setNone()

    # display
    self.obj114.display.setValue('alarmStop/unset')

    self.obj114.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1319.44642054,682.71762454,self.obj114)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj114.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj114)
    self.globalAndLocalPostcondition(self.obj114, rootNode)
    self.obj114.postAction( rootNode.CREATE )

    self.obj115=Hyperedge(self)
    self.obj115.isGraphObjectVisual = True

    if(hasattr(self.obj115, '_setHierarchicalLink')):
      self.obj115._setHierarchicalLink(False)

    # name
    self.obj115.name.setValue('')
    self.obj115.name.setNone()

    # broadcast
    self.obj115.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj115.broadcast.setHeight(15)

    # guard
    self.obj115.guard.setValue('1')

    # trigger
    self.obj115.trigger.setValue('alarmStop')

    # action
    self.obj115.action.setValue('\n')
    self.obj115.action.setHeight(15)

    # broadcast_to
    self.obj115.broadcast_to.setValue('')
    self.obj115.broadcast_to.setNone()

    # display
    self.obj115.display.setValue('alarmStop')

    self.obj115.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1385.94642054,707.21762454,self.obj115)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj115.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj115)
    self.globalAndLocalPostcondition(self.obj115, rootNode)
    self.obj115.postAction( rootNode.CREATE )

    self.obj116=Hyperedge(self)
    self.obj116.isGraphObjectVisual = True

    if(hasattr(self.obj116, '_setHierarchicalLink')):
      self.obj116._setHierarchicalLink(False)

    # name
    self.obj116.name.setValue('')
    self.obj116.name.setNone()

    # broadcast
    self.obj116.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj116.broadcast.setHeight(15)

    # guard
    self.obj116.guard.setValue('1')

    # trigger
    self.obj116.trigger.setValue('bottomLeftPressed')

    # action
    self.obj116.action.setValue('\ncontroller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj116.action.setHeight(15)

    # broadcast_to
    self.obj116.broadcast_to.setValue('')
    self.obj116.broadcast_to.setNone()

    # display
    self.obj116.display.setValue('bottomLeftPressed/alarmStop')

    self.obj116.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(954.0,663.0,self.obj116)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj116.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj116)
    self.globalAndLocalPostcondition(self.obj116, rootNode)
    self.obj116.postAction( rootNode.CREATE )

    self.obj117=Hyperedge(self)
    self.obj117.isGraphObjectVisual = True

    if(hasattr(self.obj117, '_setHierarchicalLink')):
      self.obj117._setHierarchicalLink(False)

    # name
    self.obj117.name.setValue('')
    self.obj117.name.setNone()

    # broadcast
    self.obj117.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj117.broadcast.setHeight(15)

    # guard
    self.obj117.guard.setValue('1')

    # trigger
    self.obj117.trigger.setValue('bottomRightPressed')

    # action
    self.obj117.action.setValue('controller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj117.action.setHeight(15)

    # broadcast_to
    self.obj117.broadcast_to.setValue('')
    self.obj117.broadcast_to.setNone()

    # display
    self.obj117.display.setValue('bottomRightPressed/alarmStop')

    self.obj117.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(963.5,693.5,self.obj117)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj117.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj117)
    self.globalAndLocalPostcondition(self.obj117, rootNode)
    self.obj117.postAction( rootNode.CREATE )

    self.obj118=Hyperedge(self)
    self.obj118.isGraphObjectVisual = True

    if(hasattr(self.obj118, '_setHierarchicalLink')):
      self.obj118._setHierarchicalLink(False)

    # name
    self.obj118.name.setValue('')
    self.obj118.name.setNone()

    # broadcast
    self.obj118.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj118.broadcast.setHeight(15)

    # guard
    self.obj118.guard.setValue('1')

    # trigger
    self.obj118.trigger.setValue('topLeftPressed')

    # action
    self.obj118.action.setValue('controller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj118.action.setHeight(15)

    # broadcast_to
    self.obj118.broadcast_to.setValue('')
    self.obj118.broadcast_to.setNone()

    # display
    self.obj118.display.setValue('topLeftPressed/alarmStop')

    self.obj118.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(969.5,727.5,self.obj118)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj118.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj118)
    self.globalAndLocalPostcondition(self.obj118, rootNode)
    self.obj118.postAction( rootNode.CREATE )

    self.obj119=Hyperedge(self)
    self.obj119.isGraphObjectVisual = True

    if(hasattr(self.obj119, '_setHierarchicalLink')):
      self.obj119._setHierarchicalLink(False)

    # name
    self.obj119.name.setValue('')
    self.obj119.name.setNone()

    # broadcast
    self.obj119.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj119.broadcast.setHeight(15)

    # guard
    self.obj119.guard.setValue('1')

    # trigger
    self.obj119.trigger.setValue('topRightPressed')

    # action
    self.obj119.action.setValue('controller.setAlarm()\n[EVENT("alarmStop")]\n')
    self.obj119.action.setHeight(15)

    # broadcast_to
    self.obj119.broadcast_to.setValue('')
    self.obj119.broadcast_to.setNone()

    # display
    self.obj119.display.setValue('topRightPressed/alarmStop')

    self.obj119.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(966.0,762.5,self.obj119)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj119.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj119)
    self.globalAndLocalPostcondition(self.obj119, rootNode)
    self.obj119.postAction( rootNode.CREATE )

    self.obj120=Hyperedge(self)
    self.obj120.isGraphObjectVisual = True

    if(hasattr(self.obj120, '_setHierarchicalLink')):
      self.obj120._setHierarchicalLink(False)

    # name
    self.obj120.name.setValue('')
    self.obj120.name.setNone()

    # broadcast
    self.obj120.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj120.broadcast.setHeight(15)

    # guard
    self.obj120.guard.setValue('1')

    # trigger
    self.obj120.trigger.setValue('start')

    # action
    self.obj120.action.setValue('\ncontroller=[PARAMS]\n')
    self.obj120.action.setHeight(15)

    # broadcast_to
    self.obj120.broadcast_to.setValue('')
    self.obj120.broadcast_to.setNone()

    # display
    self.obj120.display.setValue('start')

    self.obj120.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(207.000955474,428.982510187,self.obj120)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj120.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj120)
    self.globalAndLocalPostcondition(self.obj120, rootNode)
    self.obj120.postAction( rootNode.CREATE )

    self.obj121=Hyperedge(self)
    self.obj121.isGraphObjectVisual = True

    if(hasattr(self.obj121, '_setHierarchicalLink')):
      self.obj121._setHierarchicalLink(False)

    # name
    self.obj121.name.setValue('')
    self.obj121.name.setNone()

    # broadcast
    self.obj121.broadcast.setValue('# return an instance of DEVSevent or None\nreturn None\n')
    self.obj121.broadcast.setHeight(15)

    # guard
    self.obj121.guard.setValue('1')

    # trigger
    self.obj121.trigger.setValue('stop')

    # action
    self.obj121.action.setValue('\n')
    self.obj121.action.setHeight(15)

    # broadcast_to
    self.obj121.broadcast_to.setValue('')
    self.obj121.broadcast_to.setNone()

    # display
    self.obj121.display.setValue('stop')

    self.obj121.graphClass_= graph_Hyperedge
    if self.genGraphics:
       new_obj = graph_Hyperedge(1696.50574898,429.03909536,self.obj121)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("Hyperedge", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
       new_obj.layConstraints['scale'] = [1.0, 1.0]
    else: new_obj = None
    self.obj121.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj121)
    self.globalAndLocalPostcondition(self.obj121, rootNode)
    self.obj121.postAction( rootNode.CREATE )

    self.obj122=orthogonality(self)
    self.obj122.isGraphObjectVisual = True

    if(hasattr(self.obj122, '_setHierarchicalLink')):
      self.obj122._setHierarchicalLink(False)

    self.obj122.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(354.5,407.5,self.obj122)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj122.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj122)
    self.globalAndLocalPostcondition(self.obj122, rootNode)
    self.obj122.postAction( rootNode.CREATE )

    self.obj123=orthogonality(self)
    self.obj123.isGraphObjectVisual = True

    if(hasattr(self.obj123, '_setHierarchicalLink')):
      self.obj123._setHierarchicalLink(False)

    self.obj123.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(325.0,73.5,self.obj123)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj123.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj123)
    self.globalAndLocalPostcondition(self.obj123, rootNode)
    self.obj123.postAction( rootNode.CREATE )

    self.obj124=orthogonality(self)
    self.obj124.isGraphObjectVisual = True

    if(hasattr(self.obj124, '_setHierarchicalLink')):
      self.obj124._setHierarchicalLink(False)

    self.obj124.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(730.0,89.0,self.obj124)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj124.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj124)
    self.globalAndLocalPostcondition(self.obj124, rootNode)
    self.obj124.postAction( rootNode.CREATE )

    self.obj125=orthogonality(self)
    self.obj125.isGraphObjectVisual = True

    if(hasattr(self.obj125, '_setHierarchicalLink')):
      self.obj125._setHierarchicalLink(False)

    self.obj125.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(526.5,315.5,self.obj125)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj125.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj125)
    self.globalAndLocalPostcondition(self.obj125, rootNode)
    self.obj125.postAction( rootNode.CREATE )

    self.obj126=orthogonality(self)
    self.obj126.isGraphObjectVisual = True

    if(hasattr(self.obj126, '_setHierarchicalLink')):
      self.obj126._setHierarchicalLink(False)

    self.obj126.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(537.5,238.0,self.obj126)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj126.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj126)
    self.globalAndLocalPostcondition(self.obj126, rootNode)
    self.obj126.postAction( rootNode.CREATE )

    self.obj127=orthogonality(self)
    self.obj127.isGraphObjectVisual = True

    if(hasattr(self.obj127, '_setHierarchicalLink')):
      self.obj127._setHierarchicalLink(False)

    self.obj127.graphClass_= graph_orthogonality
    if self.genGraphics:
       new_obj = graph_orthogonality(749.5,321.5,self.obj127)
       new_obj.DrawObject(self.UMLmodel)
       self.UMLmodel.addtag_withtag("orthogonality", new_obj.tag)
       new_obj.layConstraints = dict() # Graphical Layout Constraints 
    else: new_obj = None
    self.obj127.graphObject_ = new_obj

    # Add node to the root: rootNode
    rootNode.addNode(self.obj127)
    self.globalAndLocalPostcondition(self.obj127, rootNode)
    self.obj127.postAction( rootNode.CREATE )

    # Connections for obj28 (graphObject_: Obj0) named DigitalWatch
    self.drawConnections(
(self.obj28,self.obj122,[308.0, 425.9999999999998, 365.2500000000002, 455.2499999999999, 354.5, 407.50000000000006],"true", 3),
(self.obj28,self.obj123,[951.0, 62.999999999999986, 325.0000000000001, 73.5],"true", 2),
(self.obj28,self.obj124,[951.0, 62.999999999999986, 729.9999999999997, 89.00000000000004],"true", 2),
(self.obj28,self.obj125,[951.0, 62.999999999999986, 526.4999999999999, 315.49999999999994],"true", 2),
(self.obj28,self.obj126,[951.0, 62.999999999999986, 537.5000000000002, 238.0],"true", 2),
(self.obj28,self.obj127,[951.0, 62.999999999999986, 749.5000000000002, 321.5],"true", 2),
(self.obj28,self.obj121,[1594.9999999999995, 425.9999999999999, 1696.5057489840433, 429.03909536014714],"true", 2) )
    # Connections for obj29 (graphObject_: Obj1) named Time
    self.drawConnections(
(self.obj29,self.obj81,[525.2765809560262, 259.88694466351205, 536.9999999999997, 298.9999999999998, 521.0000000000001, 298.9999999999998],"true", 3),
(self.obj29,self.obj85,[512.0114979680868, 252.0781907202945, 423.39782812085275, 228.50356492490153, 400.5299822133067, 202.43327707001237],"true", 3),
(self.obj29,self.obj93,[528.920208901211, 247.4947916666667, 599.1764875056645, 224.54295403753403, 640.191269441, 191.778068391],"true", 3),
(self.obj29,self.obj95,[530.0019109474379, 251.96502037329134, 598.2116321940366, 254.74459246508133, 644.0469544607556, 278.65828528842513],"true", 3) )
    # Connections for obj30 (graphObject_: Obj2) named TimeUpdate
    self.drawConnections(
(self.obj30,self.obj80,[1196.7479645862113, 164.16809608307028, 1184.9999999999995, 121.99999999999955, 1199.9999999999998, 121.99999999999979],"true", 3) )
    # Connections for obj31 (graphObject_: Obj3) named LightOn
    self.drawConnections(
(self.obj31,self.obj83,[405.27658095602624, 439.88694466351194, 414.89582180863863, 471.61059564557945, 432.76366771618586, 497.68088350046924],"true", 3) )
    # Connections for obj32 (graphObject_: Obj4) named LightOff
    self.drawConnections(
(self.obj32,self.obj82,[632.0114979680864, 432.0781907202943, 576.5040036703447, 442.04989683429653, 521.0016069151834, 442.02160424754584],"true", 3) )
    # Connections for obj33 (graphObject_: Obj5) named Button released
    self.drawConnections(
(self.obj33,self.obj84,[488.9202089012112, 547.4947916666666, 518.8294306615649, 511.8101752824891, 554.8442125965189, 484.04528963580174],"true", 3) )
    # Connections for obj34 (graphObject_: Obj6) named Chrono
    self.drawConnections(
(self.obj34,self.obj86,[428.8063455279241, 136.43524907991585, 445.62671742138434, 148.55147582168055, 468.49456332893067, 174.6217636765701],"true", 3),
(self.obj34,self.obj92,[416.7479645862115, 124.16809608307034, 406.9999999999999, 91.99999999999994, 422.0, 92.00000000000001],"true", 3) )
    # Connections for obj35 (graphObject_: Obj7) named Chrono off
    self.drawConnections(
(self.obj35,self.obj87,[830.0019109474379, 451.96502037329117, 879.513552159402, 466.94411209647444, 930.0159489145641, 471.972404683225],"true", 3),
(self.obj35,self.obj91,[825.2765809560262, 459.88694466351205, 838.0, 489.9999999999997, 823.0, 489.9999999999997],"true", 3) )
    # Connections for obj36 (graphObject_: Obj8) named Chrono on
    self.drawConnections(
(self.obj36,self.obj88,[1045.2765809560262, 479.88694466351194, 1058.0, 510.0, 1043.0, 510.0],"true", 3),
(self.obj36,self.obj89,[1048.8063455279241, 476.4352490799158, 1142.9999999999998, 530.0, 1143.0, 475.00000000000006],"true", 3),
(self.obj36,self.obj90,[1033.3432000143137, 467.61979166666663, 987.4998567561222, 431.09909899711033, 936.99746, 426.0708064099998],"true", 3) )
    # Connections for obj37 (graphObject_: Obj9) named Time edit
    self.drawConnections(
(self.obj37,self.obj94,[712.9793366410263, 116.43524907991588, 657.7230580365728, 129.38708670904833, 616.7082761016187, 162.15197235573612],"true", 3),
(self.obj37,self.obj98,[728.8063455279241, 116.43524907991583, 757.8544098391559, 131.28899474010322, 779.8398146037276, 153.2222064908919],"true", 3) )
    # Connections for obj38 (graphObject_: Obj10) named Alarm
    self.drawConnections(
(self.obj38,self.obj96,[712.0114979680869, 332.0781907202944, 660.8079729679305, 330.49374199939825, 614.6737593463332, 312.6976063527106],"true", 3),
(self.obj38,self.obj99,[725.3335126426696, 324.04309608307045, 740.6193087280606, 291.4150908196345, 763.427438792285, 265.37605296474493],"true", 3) )
    # Connections for obj39 (graphObject_: Obj11) named Edit
    self.drawConnections(
(self.obj39,self.obj97,[1021.177499517693, 143.0245629600421, 1017.0, 134.0, 1017.0, 112.5],"true", 3),
(self.obj39,self.obj101,[1012.9793366410264, 156.43524907991588, 970.9896683205133, 186.71762453995794, 925.2453118971162, 200.60006736330186],"true", 3) )
    # Connections for obj40 (graphObject_: Obj12) named Next
    self.drawConnections(
(self.obj40,self.obj103,[1013.3432000143138, 287.61979166666663, 983.171600007157, 265.8098958333333, 937.3362777404379, 246.89620300998948],"true", 3),
(self.obj40,self.obj105,[1013.3432000143138, 287.61979166666663, 891.56672034366, 289.80300942199, 770.4325067220625, 282.0068737753023],"true", 3) )
    # Connections for obj41 (graphObject_: Obj13) named Start edit
    self.drawConnections(
(self.obj41,self.obj100,[828.920208901211, 207.49479166666674, 857.6081246556703, 177.366002510935, 903.6229065906244, 164.6011168642473],"true", 3),
(self.obj41,self.obj102,[828.8063455279241, 216.43524907991588, 865.4031727639621, 253.71762453995794, 911.2044608740027, 272.6283599500527],"true", 3),
(self.obj41,self.obj104,[812.0114979680868, 212.07819072029446, 671.0067044577623, 232.0216055467929],"true", 2) )
    # Connections for obj42 (graphObject_: Obj14) named Check alarm
    self.drawConnections(
(self.obj42,self.obj106,[1236.7479645862115, 644.1680960830704, 1203.0, 587.0, 1240.0000000000002, 588.5],"true", 3),
(self.obj42,self.obj107,[1250.0019109474374, 651.9650203732913, 1283.5009554737185, 643.4825101866454, 1319.003352228881, 643.5108027733963],"true", 3) )
    # Connections for obj43 (graphObject_: Obj15) named Start alarm
    self.drawConnections(
(self.obj43,self.obj110,[1408.920208901211, 647.4947916666665, 1448.4601044506055, 638.2473958333333, 1469.5658522288813, 638.2786458333333],"true", 3),
(self.obj43,self.obj114,[1392.9793366410265, 656.4352490799158, 1355.4896683205134, 682.7176245399579, 1319.4464205422378, 682.7176245399579],"true", 3) )
    # Connections for obj44 (graphObject_: Obj16) named Blink
    self.drawConnections(
(self.obj44,self.obj111,[1552.9793366410263, 656.4352490799157, 1499.783016449784, 667.443472331756, 1467.8430946068734, 666.580548435857],"true", 3),
(self.obj44,self.obj115,[1552.9793366410263, 656.4352490799157, 1461.9896683205134, 707.2176245399579, 1385.9464205422378, 707.2176245399579],"true", 3) )
    # Connections for obj45 (graphObject_: Obj17) named AlarmOff
    self.drawConnections(
(self.obj45,self.obj108,[788.9202089012109, 587.4947916666665, 829.9601044506055, 574.747395833333, 871.0658522288812, 574.7786458333333],"true", 3) )
    # Connections for obj46 (graphObject_: Obj18) named AlarmOn
    self.drawConnections(
(self.obj46,self.obj109,[952.9793366410265, 596.4352490799157, 904.5160866217188, 615.0499005733406, 864.0136898665567, 615.0216079865897],"true", 3),
(self.obj46,self.obj112,[968.9202089012111, 587.4947916666665, 1003.4601044506054, 575.2473958333333, 1034.5658522288813, 575.2786458333333],"true", 3) )
    # Connections for obj47 (graphObject_: Obj19) named AlarmStart
    self.drawConnections(
(self.obj47,self.obj113,[1092.9793366410265, 596.4352490799157, 1024.4896683205134, 635.7176245399578, 948.4464205422378, 635.7176245399578],"true", 3),
(self.obj47,self.obj116,[1096.566032899568, 599.8869446635119, 1031.0, 665.0000000000001, 954.0, 663.0],"true", 3),
(self.obj47,self.obj117,[1096.566032899568, 599.8869446635119, 1027.0, 693.0, 963.5, 693.5],"true", 3),
(self.obj47,self.obj118,[1096.566032899568, 599.8869446635119, 1030.0000000000005, 733.0, 969.5000000000001, 727.5],"true", 3),
(self.obj47,self.obj119,[1096.566032899568, 599.8869446635119, 1013.9999999999999, 766.0, 966.0000000000001, 762.5000000000001],"true", 3) )
    # Connections for obj48 (graphObject_: Obj20) named Start
    self.drawConnections(
(self.obj48,self.obj120,[150.0019109474379, 431.9650203732912, 207.00095547371893, 428.98251018664564],"true", 2) )
    # Connections for obj49 (graphObject_: Obj21) named Stop
    self.drawConnections(
 )
    # Connections for obj52 (graphObject_: Obj24) named Main timer loop
    self.drawConnections(
(self.obj52,self.obj59,[1203.0, 113.99999999999997, 1154.3739822929997, -16.91595195800005],"true", 2) )
    # Connections for obj53 (graphObject_: Obj25) named Displayloop
    self.drawConnections(
(self.obj53,self.obj60,[339.0, 211.0, 433.005748984, 279.5390953599997],"true", 2),
(self.obj53,self.obj64,[339.0, 211.0, 476.78301645000033, 208.443472332],"true", 2),
(self.obj53,self.obj67,[902.0000000000002, 211.0, 757.6716000070002, 244.30989583299998],"true", 2),
(self.obj53,self.obj68,[1073.0, 224.0, 1122.78301645, 129.44347233200006],"true", 2),
(self.obj53,self.obj69,[1073.0, 224.0, 1275.4896683199997, 122.71762454],"true", 2),
(self.obj53,self.obj70,[1073.0, 224.0, 1410.0057489840437, 223.03909536014726],"true", 2),
(self.obj53,self.obj71,[709.0, 364.9999999999999, 666.4999999999998, 366.9999999999999, 667.4948341602568, 367.85881226997896],"true", 3) )
    # Connections for obj54 (graphObject_: Obj26) named Backlight
    self.drawConnections(
(self.obj54,self.obj61,[365.9999999999999, 501.0, -258.4112502400001, 447.5122814799999],"true", 2),
(self.obj54,self.obj62,[365.9999999999999, 501.0, -155.4942510199999, 509.53909535999986],"true", 2),
(self.obj54,self.obj63,[365.9999999999999, 501.0, -152.91125024000007, 730.01228148],"true", 2) )
    # Connections for obj55 (graphObject_: Obj27) named Chrono loop
    self.drawConnections(
(self.obj55,self.obj65,[1001.0, 411.9999999999999, 1012.67160001, 454.80989583300004],"true", 2),
(self.obj55,self.obj66,[1242.9999999999995, 475.00000000000006, 1187.0057489800004, 499.53909535999986],"true", 2) )
    # Connections for obj56 (graphObject_: Obj28) named Alarm
    self.drawConnections(
(self.obj56,self.obj72,[1387.0, 727.0000000000002, 1231.78301645, 878.443472332],"true", 2),
(self.obj56,self.obj73,[1387.0, 727.0000000000002, 1414.9999999999995, 843.0, 1463.00047774, 839.2412550930001],"true", 3),
(self.obj56,self.obj74,[1387.0, 727.0000000000002, 1403.1716000099996, 892.3098958329999],"true", 2) )
    # Connections for obj57 (graphObject_: Obj29) named AlarmState
    self.drawConnections(
(self.obj57,self.obj75,[741.0, 675.0, 631.166756321, 475.52154804199995],"true", 2),
(self.obj57,self.obj76,[741.0, 675.0, 685.005748984, 539.53909536],"true", 2),
(self.obj57,self.obj77,[947.0, 566.9999999999998, 836.5057489799997, 576.53909536],"true", 2) )
    # Connections for obj59 (graphObject_: Obj31) of type contains
    self.drawConnections(
(self.obj59,self.obj30,[1154.3739822929997, -16.91595195800005, 1201.1774995176934, 163.02456296004192],"true", 2) )
    # Connections for obj60 (graphObject_: Obj32) of type contains
    self.drawConnections(
(self.obj60,self.obj29,[433.005748984, 279.5390953599997, 512.9793366410263, 256.4352490799159],"true", 2) )
    # Connections for obj61 (graphObject_: Obj33) of type contains
    self.drawConnections(
(self.obj61,self.obj31,[-258.4112502400001, 447.5122814799999, 392.0114979680868, 432.0781907202943],"true", 2) )
    # Connections for obj62 (graphObject_: Obj34) of type contains
    self.drawConnections(
(self.obj62,self.obj32,[-155.4942510199999, 509.53909535999986, 632.0114979680868, 432.0781907202943],"true", 2) )
    # Connections for obj63 (graphObject_: Obj35) of type contains
    self.drawConnections(
(self.obj63,self.obj33,[-152.91125024000007, 730.01228148, 472.9793366410265, 556.4352490799159],"true", 2) )
    # Connections for obj64 (graphObject_: Obj36) of type contains
    self.drawConnections(
(self.obj64,self.obj34,[476.78301645000033, 208.443472332, 425.2765809560262, 139.8869446635121],"true", 2) )
    # Connections for obj65 (graphObject_: Obj37) of type contains
    self.drawConnections(
(self.obj65,self.obj35,[1012.67160001, 454.80989583300004, 830.001910947438, 451.9650203732913],"true", 2) )
    # Connections for obj66 (graphObject_: Obj38) of type contains
    self.drawConnections(
(self.obj66,self.obj36,[1187.0057489800004, 499.53909535999986, 1050.0019109474379, 471.9650203732913],"true", 2) )
    # Connections for obj67 (graphObject_: Obj39) of type contains
    self.drawConnections(
(self.obj67,self.obj37,[757.6716000070002, 244.30989583299998, 725.2765809560262, 119.88694466351212],"true", 2) )
    # Connections for obj68 (graphObject_: Obj40) of type contains
    self.drawConnections(
(self.obj68,self.obj39,[1122.78301645, 129.44347233200006, 1030.0019109474379, 151.96502037329134],"true", 2) )
    # Connections for obj69 (graphObject_: Obj41) of type contains
    self.drawConnections(
(self.obj69,self.obj40,[1275.4896683199997, 122.71762454, 1028.920208901211, 287.49479166666663],"true", 2) )
    # Connections for obj70 (graphObject_: Obj42) of type contains
    self.drawConnections(
(self.obj70,self.obj41,[1410.0057489840437, 223.03909536014726, 830.0019109474379, 211.96502037329134],"true", 2) )
    # Connections for obj71 (graphObject_: Obj43) of type contains
    self.drawConnections(
(self.obj71,self.obj38,[667.4948341602568, 367.85881226997896, 668.4896683205133, 368.7176245399578, 712.9793366410265, 336.4352490799158],"true", 3) )
    # Connections for obj72 (graphObject_: Obj44) of type contains
    self.drawConnections(
(self.obj72,self.obj42,[1231.78301645, 878.443472332, 1240.8359093978318, 660.962062960042],"true", 2) )
    # Connections for obj73 (graphObject_: Obj45) of type contains
    self.drawConnections(
(self.obj73,self.obj43,[1463.00047774, 839.2412550930001, 1511.000955473719, 835.4825101866456, 1405.2765809560262, 659.886944663512],"true", 3) )
    # Connections for obj74 (graphObject_: Obj46) of type contains
    self.drawConnections(
(self.obj74,self.obj44,[1403.1716000099996, 892.3098958329999, 1556.566032899568, 659.886944663512],"true", 2) )
    # Connections for obj75 (graphObject_: Obj47) of type contains
    self.drawConnections(
(self.obj75,self.obj45,[631.166756321, 475.52154804199995, 773.3432000143138, 587.6197916666665],"true", 2) )
    # Connections for obj76 (graphObject_: Obj48) of type contains
    self.drawConnections(
(self.obj76,self.obj46,[685.005748984, 539.53909536, 952.0114979680869, 592.0781907202945],"true", 2) )
    # Connections for obj77 (graphObject_: Obj49) of type contains
    self.drawConnections(
(self.obj77,self.obj47,[836.5057489840433, 576.5390953601471, 1092.0114979680868, 592.0781907202945],"true", 2) )
    # Connections for obj80 (graphObject_: Obj52) named 
    self.drawConnections(
(self.obj80,self.obj30,[1199.9999999999998, 121.99999999999979, 1214.9999999999995, 121.99999999999979, 1205.3335126426691, 164.04309608307028],"true", 3) )
    # Connections for obj81 (graphObject_: Obj54) named 
    self.drawConnections(
(self.obj81,self.obj29,[521.0000000000001, 298.9999999999998, 504.9999999999997, 298.9999999999998, 516.566032899568, 259.88694466351205],"true", 3) )
    # Connections for obj82 (graphObject_: Obj56) named 
    self.drawConnections(
(self.obj82,self.obj31,[521.0016069151834, 442.02160424754584, 465.49921016001895, 441.99331166079514, 410.0019109474373, 431.9650203732913],"true", 3) )
    # Connections for obj83 (graphObject_: Obj58) named 
    self.drawConnections(
(self.obj83,self.obj33,[432.76366771618586, 497.68088350046924, 450.6315136237322, 523.7511713553589, 476.7479645862104, 544.1680960830704],"true", 3) )
    # Connections for obj84 (graphObject_: Obj60) named 
    self.drawConnections(
(self.obj84,self.obj32,[554.8442125965189, 484.04528963580174, 590.8589945314714, 456.28040398911367, 632.9793366410263, 436.4352490799158],"true", 3) )
    # Connections for obj85 (graphObject_: Obj62) named 
    self.drawConnections(
(self.obj85,self.obj34,[400.5299822133067, 202.43327707001237, 377.6621363057601, 176.3629892151226, 412.97933664102646, 136.43524907991585],"true", 3) )
    # Connections for obj86 (graphObject_: Obj64) named 
    self.drawConnections(
(self.obj86,self.obj29,[468.49456332893067, 174.6217636765701, 491.3624092364772, 200.6920515314596, 516.7479645862112, 244.16809608307042],"true", 3) )
    # Connections for obj87 (graphObject_: Obj66) named 
    self.drawConnections(
(self.obj87,self.obj36,[930.0159489145641, 471.972404683225, 980.5183456697263, 477.0006972699757, 1032.0114979680868, 472.0781907202944],"true", 3) )
    # Connections for obj88 (graphObject_: Obj68) named 
    self.drawConnections(
(self.obj88,self.obj36,[1043.0, 510.0, 1028.0, 510.0, 1036.566032899568, 479.88694466351194],"true", 3) )
    # Connections for obj89 (graphObject_: Obj70) named 
    self.drawConnections(
(self.obj89,self.obj36,[1143.0, 475.00000000000006, 1142.9999999999998, 420.0, 1048.920208901211, 467.49479166666663],"true", 3) )
    # Connections for obj90 (graphObject_: Obj72) named 
    self.drawConnections(
(self.obj90,self.obj35,[936.99746, 426.0708064099998, 886.4950632457983, 421.04251382360894, 828.9202089012111, 447.49479166666663],"true", 3) )
    # Connections for obj91 (graphObject_: Obj74) named 
    self.drawConnections(
(self.obj91,self.obj35,[823.0, 489.9999999999997, 808.0, 489.9999999999997, 816.566032899568, 459.88694466351205],"true", 3) )
    # Connections for obj92 (graphObject_: Obj76) named 
    self.drawConnections(
(self.obj92,self.obj34,[422.0, 92.00000000000001, 437.0, 91.99999999999994, 425.33351264266946, 124.04309608307044],"true", 3) )
    # Connections for obj93 (graphObject_: Obj78) named 
    self.drawConnections(
(self.obj93,self.obj37,[640.191269441, 191.778068391, 681.2060513755719, 159.01318274415877, 716.5660328995682, 119.88694466351212],"true", 3) )
    # Connections for obj94 (graphObject_: Obj80) named 
    self.drawConnections(
(self.obj94,self.obj29,[616.7082761016189, 162.15197235573612, 575.693494166665, 194.91685800242374, 525.3335126426695, 244.0430960830703],"true", 3) )
    # Connections for obj95 (graphObject_: Obj82) named 
    self.drawConnections(
(self.obj95,self.obj38,[644.0469544607556, 278.65828528842513, 689.8822767274746, 302.57197811176894, 713.3432000143138, 327.61979166666663],"true", 3) )
    # Connections for obj96 (graphObject_: Obj84) named 
    self.drawConnections(
(self.obj96,self.obj29,[614.6737593463332, 312.6976063527106, 568.5395457247356, 294.90147070602285, 528.8063455279241, 256.4352490799159],"true", 3) )
    # Connections for obj97 (graphObject_: Obj86) named 
    self.drawConnections(
(self.obj97,self.obj39,[1017.0, 112.5, 1017.0, 90.99999999999997, 1021.177499517693, 143.0245629600421],"true", 3) )
    # Connections for obj98 (graphObject_: Obj88) named 
    self.drawConnections(
(self.obj98,self.obj41,[779.8398146037276, 153.2222064908919, 801.8252193682995, 175.15541824168054, 816.7479645862111, 204.16809608307048],"true", 3) )
    # Connections for obj99 (graphObject_: Obj90) named 
    self.drawConnections(
(self.obj99,self.obj41,[763.427438792285, 265.37605296474493, 786.2355688565099, 239.33701510985532, 816.566032899568, 219.88694466351217],"true", 3) )
    # Connections for obj100 (graphObject_: Obj92) named 
    self.drawConnections(
(self.obj100,self.obj39,[903.6229065906244, 164.6011168642473, 949.6376885255781, 151.83623121755954, 1012.0114979680869, 152.07819072029446],"true", 3) )
    # Connections for obj101 (graphObject_: Obj94) named 
    self.drawConnections(
(self.obj101,self.obj41,[925.2453118971162, 200.60006736330186, 879.5009554737189, 214.48251018664567, 830.0019109474379, 211.96502037329134],"true", 3) )
    # Connections for obj102 (graphObject_: Obj96) named 
    self.drawConnections(
(self.obj102,self.obj40,[911.2044608740027, 272.6283599500527, 957.0057489840435, 291.5390953601472, 1012.0114979680869, 292.0781907202943],"true", 3) )
    # Connections for obj103 (graphObject_: Obj98) named 
    self.drawConnections(
(self.obj103,self.obj41,[937.3362777404379, 246.89620300998948, 891.5009554737189, 227.98251018664567, 830.0019109474379, 211.96502037329134],"true", 3) )
    # Connections for obj104 (graphObject_: Obj100) named 
    self.drawConnections(
(self.obj104,self.obj29,[671.0067044577623, 232.0216055467929, 530.0019109474379, 251.96502037329134],"true", 2) )
    # Connections for obj105 (graphObject_: Obj102) named 
    self.drawConnections(
(self.obj105,self.obj29,[770.4325067220625, 282.0068737753023, 649.2982931004651, 274.2107381286145, 528.8063455279241, 256.4352490799159],"true", 3) )
    # Connections for obj106 (graphObject_: Obj104) named 
    self.drawConnections(
(self.obj106,self.obj42,[1240.0000000000002, 588.5, 1277.0, 590.0000000000002, 1245.3335126426696, 644.0430960830704],"true", 3) )
    # Connections for obj107 (graphObject_: Obj106) named 
    self.drawConnections(
(self.obj107,self.obj43,[1319.003352228881, 643.5108027733963, 1354.5057489840433, 643.5390953601474, 1392.0114979680868, 652.0781907202945],"true", 3) )
    # Connections for obj108 (graphObject_: Obj108) named 
    self.drawConnections(
(self.obj108,self.obj46,[871.0658522288812, 574.7786458333333, 912.1716000071569, 574.8098958333333, 953.3432000143138, 587.6197916666665],"true", 3) )
    # Connections for obj109 (graphObject_: Obj110) named 
    self.drawConnections(
(self.obj109,self.obj45,[864.0136898665567, 615.0216079865897, 823.5112931113945, 614.9933153998389, 788.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj110 (graphObject_: Obj112) named 
    self.drawConnections(
(self.obj110,self.obj44,[1469.5658522288813, 638.2786458333333, 1490.671600007157, 638.3098958333334, 1552.0114979680864, 652.0781907202945],"true", 3) )
    # Connections for obj111 (graphObject_: Obj114) named 
    self.drawConnections(
(self.obj111,self.obj43,[1467.8430946068734, 666.580548435857, 1435.9031727639622, 665.7176245399579, 1408.8063455279241, 656.4352490799158],"true", 3) )
    # Connections for obj112 (graphObject_: Obj116) named 
    self.drawConnections(
(self.obj112,self.obj47,[1034.5658522288813, 575.2786458333333, 1065.6716000071574, 575.3098958333333, 1093.3432000143137, 587.6197916666665],"true", 3) )
    # Connections for obj113 (graphObject_: Obj118) named 
    self.drawConnections(
(self.obj113,self.obj45,[948.4464205422378, 635.7176245399578, 872.4031727639622, 635.7176245399578, 788.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj114 (graphObject_: Obj120) named 
    self.drawConnections(
(self.obj114,self.obj42,[1319.4464205422378, 682.7176245399579, 1283.4031727639622, 682.7176245399579, 1248.806345527924, 656.4352490799158],"true", 3) )
    # Connections for obj115 (graphObject_: Obj122) named 
    self.drawConnections(
(self.obj115,self.obj42,[1385.9464205422378, 707.2176245399579, 1309.9031727639622, 707.2176245399579, 1248.806345527924, 656.4352490799158],"true", 3) )
    # Connections for obj116 (graphObject_: Obj124) named 
    self.drawConnections(
(self.obj116,self.obj45,[954.0, 663.0, 877.0000000000002, 661.0, 788.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj117 (graphObject_: Obj126) named 
    self.drawConnections(
(self.obj117,self.obj45,[963.5, 693.5, 900.0, 694.0, 788.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj118 (graphObject_: Obj128) named 
    self.drawConnections(
(self.obj118,self.obj45,[969.5000000000001, 727.5, 909.0, 722.0, 788.8063455279242, 596.4352490799157],"true", 3) )
    # Connections for obj119 (graphObject_: Obj130) named 
    self.drawConnections(
(self.obj119,self.obj45,[966.0000000000001, 762.5000000000001, 918.0, 759.0, 785.2765809560262, 599.8869446635119],"true", 3) )
    # Connections for obj120 (graphObject_: Obj132) named 
    self.drawConnections(
(self.obj120,self.obj28,[207.00095547371893, 428.98251018664564, 308.0, 425.9999999999999],"true", 2) )
    # Connections for obj121 (graphObject_: Obj134) named 
    self.drawConnections(
(self.obj121,self.obj49,[1696.5057489840433, 429.03909536014714, 1772.0114979680868, 432.07819072029434],"true", 2) )
    # Connections for obj122 (graphObject_: Obj136) of type orthogonality
    self.drawConnections(
(self.obj122,self.obj54,[354.5, 407.50000000000006, 343.75000000000006, 359.7500000000001, 523.0, 417.0],"true", 3) )
    # Connections for obj123 (graphObject_: Obj137) of type orthogonality
    self.drawConnections(
(self.obj123,self.obj53,[325.0000000000001, 73.5, 709.0, 83.0],"true", 2) )
    # Connections for obj124 (graphObject_: Obj138) of type orthogonality
    self.drawConnections(
(self.obj124,self.obj52,[729.9999999999997, 89.00000000000004, 1148.0, 159.0],"true", 2) )
    # Connections for obj125 (graphObject_: Obj139) of type orthogonality
    self.drawConnections(
(self.obj125,self.obj57,[526.4999999999999, 315.49999999999994, 947.0, 566.9999999999997],"true", 2) )
    # Connections for obj126 (graphObject_: Obj140) of type orthogonality
    self.drawConnections(
(self.obj126,self.obj55,[537.5000000000002, 238.0, 1001.0, 411.9999999999999],"true", 2) )
    # Connections for obj127 (graphObject_: Obj141) of type orthogonality
    self.drawConnections(
(self.obj127,self.obj56,[749.5000000000002, 321.5, 1187.0, 652.9999999999995],"true", 2) )

newfunction = DigitalWatchStatechart_DCharts_MDL

loadedMMName = 'DCharts'

atom3version = '0.3'
