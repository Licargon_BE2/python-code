# Import code for DEVS model representation:
import pydevs
from pydevs.devs_exceptions import *
from pydevs.infinity import *
from pydevs.DEVS import *

# Import for uniform random number generators 
from random import uniform
from random import randint
from time import *

class Car:
  """ The Car class is a container for all the relevant information pertaining to a car (and its driver).
  """
  IDCount = 0

  def __init__(self, v_pref, departure_time, dv_pos_max=0.9, dv_neg_max=1.2):
    # dv_pos_max and dv_neg_max default values should be adjusted or made random

    # set unique car id
    self.ID = Car.IDCount
    Car.IDCount += 1

    # initialize variables
    self.v_pref = v_pref
    self.dv_pos_max = dv_pos_max
    self.dv_neg_max = dv_neg_max
    self.departure_time = departure_time
    self.distance_travelled = 0
    self.v = uniform(0, 3)

  def setSpeed(self, v):
    self.v = v

  def getSpeed(self):
    return self.v

  def getPrefSpeed(self):
    return self.v_pref

  def getPosAcceleration(self):
    return self.dv_pos_max

  def getNegAcceleration(self):
    return self.dv_neg_max

  def addDistance(self, dist):
    self.distance_travelled += dist

  def getDistance(self):
    return self.distance_travelled

  def getDepTime(self):
    return self.departure_time

# RoadSegment states
PROBE = 0
DELAY = 1

class RoadSegment(AtomicDEVS):
  """ Container for the information about each roadsegment
  """

  def __init__(self, length, v_max, observ_delay = 0.1, name=None):
    AtomicDEVS.__init__(self, name)

    self.length = length;
    self.v_max = v_max;
    self.observ_delay = observ_delay;
    self.carspresent = [];
    self.remaining_x = -1
    self.departure = -1
    self.currentState = None

    self.car_in = self.addInPort(name="car_in");
    self.car_out = self.addOutPort(name="car_out");
    self.Q_recv = self.addInPort(name="Q_recv");
    self.Q_sack = self.addOutPort(name="Q_sack");
    self.Q_send = self.addOutPort(name="Q_send");
    self.Q_rack = self.addInPort(name="Q_rack");

  def intTransition(self):
    #self.updateDeparture()
    pass

  def outputFnc(self):
    if self.currentState == PROBE:
      self.poke(self.Q_send, Query())
      self.currentState = None
    elif self.currentState == DELAY:
      ack = None
      if len(self.carspresent) == 0: # No cars: 0 time until departure
        ack = QueryAck(0)
      elif len(self.carspresent) == 1: # 1 car: Calculate how long it'll take at least for it to leave
        ack = QueryAck(self.departure)
      else:
        ack = QueryAck(INFINITY)
      self.poke(self.Q_sack, ack)
      self.currentState = None
    elif len(self.carspresent) == 1:
      self.carspresent[0].addDistance(self.length)
      self.poke(self.car_out, self.carspresent[0])
      self.carspresent.pop()
      self.remaining_x = -1
      self.departure = -1

  def extTransition(self): # 3 cases: Incoming car, incoming query, incoming ack
    car = self.peek(self.car_in)
    if car != None: # A car came in
      self.carspresent.append(car);
      if len(self.carspresent) > 1:
        # More than 1 car: collission. Set each car's speed to 0
        print "Collission occured."
        for car in self.carspresent:
          car.setSpeed(0)
      else:
        self.remaining_x = self.length
        self.departure = self.remaining_x / car.getSpeed()
        self.currentState = PROBE
        
    query = self.peek(self.Q_recv)
    if query != None: # We received a query
      self.currentState = DELAY # Wait until after the observation delay

    if len(self.carspresent) == 1:
      self.updateDeparture()

    incomingAck = self.peek(self.Q_rack)
    if incomingAck != None: # We received an acknowledgement
      t_no_coll = incomingAck.getTime()
      if len(self.carspresent) == 1:
        car = self.carspresent[0]
        v_pref = min(self.v_max, car.getPrefSpeed()) # Get the preferred speed that this car wants to reach
        if t_no_coll == 0:
          v_new = min(v_pref, self.getMaxAttainableSpeed(car)) # The new speed we'll want to reach here
          car.setSpeed(v_new)
          self.departure = self.remaining_x / v_new
        else: # We need to wait for a certain time
          t_target = self.remaining_x / v_pref
          v_updated = self.remaining_x / max(t_no_coll, t_target)
          v_new = min(v_updated, self.getMaxAttainableSpeed(car))
          car.setSpeed(v_new)
          self.departure = self.remaining_x / v_new

  def getCarAccel(self, car):
    v_pref = min(self.v_max, car.getPrefSpeed())
    if car.getSpeed() > v_pref:
      return -1 * car.getNegAcceleration()
    elif car.getSpeed() < v_pref:
      return car.getPosAcceleration()
    else:
      return 0

  def getMaxAttainableSpeed(self, car):
    carAccel = self.getCarAccel(car)
    v_pref = min(self.v_max, car.getPrefSpeed())
    if carAccel == 0:
      return car.getSpeed()
    else:
      v_max_att = car.getSpeed() + self.remaining_x / carAccel
      if carAccel < 0:
        return max(v_max_att, v_pref)
      else:
        return v_max_att

  def updateDeparture(self):
    if len(self.carspresent) == 1:
      speed = self.carspresent[0].getSpeed()
      if speed != 0:
        self.remaining_x -= self.elapsed * speed
        self.departure = self.remaining_x / speed

  def timeAdvance(self):
    if len(self.carspresent) > 1:
      return INFINITY
    if self.currentState == DELAY:
      return self.observ_delay
    elif self.currentState == PROBE:
      return 0
    elif len(self.carspresent) == 1 and self.departure > 0:
      return self.departure
    else:
      return INFINITY

class Query:
  """ Used to model, at a discrete event level of abstraction, 
      the driver's observation of the next road segment for the presence of a car.
  """
  def __init__(self):
    # nothing to initialize
    pass


class QueryAck:
  """ Carries information back about the presence of a car in the next road segment.
  """
  def __init__(self, t_until_dep):
    # t_until_dep indicates how long it will take for the car (if present) to leave the next road segment
    # possible values: 0, positive number, INFINITY
    self.t_until_dep = t_until_dep

  def getTime(self):
    return self.t_until_dep

# Generator states
QSEND = 0
QACKRECV = 1

class Generator(AtomicDEVS):
  """ Generates Cars.
  """
  def __init__(self, IAT_min, IAT_max, v_pref_min, v_pref_max, name=None):
    AtomicDEVS.__init__(self, name)

    # initialize variables
    # IAT = Inter Arrival Time
    self.IAT_min = IAT_min
    self.IAT_max = IAT_max
    self.v_pref_min = v_pref_min
    self.v_pref_max = v_pref_max

    self.currentTime = 0
    self.tA = 0 # timeAdvance
    self.t_until_dep = 0  # set to -1 to indicate we have no info about cars in next road segment
    self.currentState = QACKRECV
    
    # ports
    # out
    self.car_out = self.addOutPort(name="car_out")
    self.Q_send = self.addOutPort(name="Q_send")
    # in
    self.Q_rack = self.addInPort(name="Q_rack")

  def extTransition(self):

    self.currentTime += self.elapsed
    q_ack = self.peek(self.Q_rack)
    if q_ack != None:
      self.t_until_dep = q_ack.t_until_dep + 0.0001 # prevent devs collision
      self.currentState = QACKRECV

    return self.state

  def outputFnc(self):

    if self.currentState == QACKRECV: 
      v_pref = uniform(self.v_pref_min, self.v_pref_max)
      car = Car(v_pref, self.currentTime)
      self.poke(self.car_out, car)
      self.t_until_dep = -1
      self.currentState = None
      print "Car generated with speed ", car.v
    else:
      q = Query()
      self.poke(self.Q_send, q)
      self.currentState = QSEND

  def intTransition(self):

    self.currentTime += self.tA
    return self.state

  def timeAdvance(self):

    if self.t_until_dep >= 0:
      self.tA = self.t_until_dep
    elif self.currentState == QSEND:
      self.tA = INFINITY
    else:
      self.tA = uniform(self.IAT_min, self.IAT_max)
    return self.tA

class Collector(AtomicDEVS):
  def __init__(self, name=None):
    AtomicDEVS.__init__(self, name)

    self.name = name
    self.global_time = 0
    self.cars_collected = []
    self.transit_times = []
    self.speed_deviations = []

    self.car_in = self.addInPort(name="car_in")

  def extTransition(self):
    self.global_time += self.elapsed
    car = self.peek(self.car_in)
    if car != None:
      self.cars_collected.append(car)
      transit_time = self.global_time - car.getDepTime()
      self.speed_deviations.append(abs(car.getPrefSpeed() - car.getDistance() / transit_time))
      self.transit_times.append(transit_time)
      print "Car collected"

class TrafficSystem(CoupledDEVS):
  def __init__(self, name=None):
    CoupledDEVS.__init__(self, name)

    self.carGenerator = self.addSubModel(Generator(0, 10, 4.2, 5.4, "Generator"))
    self.collector = self.addSubModel(Collector("Collector"))
    self.roadsegment1 = self.addSubModel(RoadSegment(20, 6))
    self.roadsegment2 = self.addSubModel(RoadSegment(25, 6.5))
    self.roadsegment3 = self.addSubModel(RoadSegment(30, 5))

    # Carports
    self.connectPorts(self.carGenerator.car_out, self.roadsegment1.car_in)
    self.connectPorts(self.roadsegment1.car_out, self.roadsegment2.car_in)
    self.connectPorts(self.roadsegment2.car_out, self.roadsegment3.car_in)
    self.connectPorts(self.roadsegment3.car_out, self.collector.car_in)

    # Query ports
    self.connectPorts(self.carGenerator.Q_send, self.roadsegment1.Q_recv)
    self.connectPorts(self.roadsegment1.Q_sack, self.carGenerator.Q_rack)

    self.connectPorts(self.roadsegment1.Q_send, self.roadsegment2.Q_recv)
    self.connectPorts(self.roadsegment2.Q_sack, self.roadsegment1.Q_rack)

    self.connectPorts(self.roadsegment2.Q_send, self.roadsegment3.Q_recv)
    self.connectPorts(self.roadsegment3.Q_sack, self.roadsegment2.Q_rack)

