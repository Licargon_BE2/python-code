# Import code for model simulation:
import pydevs
from pydevs.infinity import *
from pydevs.simulator import *

from trafficModel import *

trafficSystem = TrafficSystem(name="highway")

sim = Simulator(trafficSystem)


def terminate_whenEndTimeReached(model, clock, end_time=999):
  if clock >= end_time:
    return True
  else:
    return False

sim.simulate(termination_condition=terminate_whenEndTimeReached, 
             verbose=True)
