class BaseBlock:

  # A base class for all types of CBD blocks

  def __init__(self, name):
    self.setBlockName(name)
    
    # The output signal produced by this block is encoded as an ordered list of values
    self.__signal = []
    
    # List of blocks that are linked to this block via indistinguishable inputs.
    # The time-delay (Delay, Integrator and Derivative) and Test blocks are different
    # as they have input ports that need to be distinguished
    # (IC for Delay/Integrator/Derivative blocks and TRUE_in/FALSE_in for Test blocks).
    # Each element of the list is a tuple containing 
    # (1) the block whose output this input is connected to, and 
    # (2) the name (a String) of the signal connecting the input block and self
    self.linksIN = []
    # Note that to find out which connections are made to the output
    # of this block, one needs to search through incoming connections
    # on other blocks. If finding outgoing connections from
    # a block turns out to be needed frequently, one 
    # add a linksOUT attribute and keep it consistent.
    # Note that multiple connections may be made from the output of
    # any block. They will all have the same signal[] and the same name (that of the block)
  
  def getBlockName(self):
    return self.__block_name
    
  def setBlockName(self, block_name):
    self.__block_name = block_name

  def getBlockType(self):
    return self.__class__.__name__

  def appendToSignal(self, value):
    self.__signal.append(value)

  def getSignal(self):
    return self.__signal

  def linkInput(self, in_block):
    self.linksIN.append(in_block)

  def __repr__(self):
    repr = self.getBlockName() + ":" + self.getBlockType() + "\n"
    if len(self.linksIN) == 0:
      repr+= "  No incoming connections to IN port\n"
    else:
      for in_block in self.linksIN:
        repr += "  IN <- " + in_block.getBlockName() + ":" + in_block.getBlockType() + "\n"
    return repr 
    

class ConstantBlock(BaseBlock):
  def __init__(self, block_name, value=0.0):
    BaseBlock.__init__(self, block_name)
    self.__value = value
  
  def getValue(self):
    return self.__value
    
  def setValue(self, value):
    self.__value = value
    
  def linkInput(self, in_block):
    print "Warning: Constant (block %s) does not accept input, ignoring extra connection" % self.getBlockName()

  def __repr__(self):    
    return BaseBlock.__repr__(self) + "  Value = " + str(self.getValue()) + "\n"
    

class NegatorBlock(BaseBlock):
  def __init__(self, block_name):
    BaseBlock.__init__(self, block_name)

  def linkInput(self, in_block):
    if len(self.linksIN) >= 1:
      print "Warning: Negator (block %s) takes exactly one input, ignoring extra connection (from block %s)"\
             % (self.getBlockName(), in_block.getBlockName())
    else:  
      self.linksIN.append(in_block)

class InverterBlock(BaseBlock):
  def __init__(self, block_name):
    BaseBlock.__init__(self, block_name)

  def linkInput(self, in_block):
    if len(self.linksIN) >= 1:
      print "Warning: Inverter (block %s) takes exactly one input, ignoring extra connection (from block %s)"\
             % (self.getBlockName(), in_block.getBlockName())
    else:  
      self.linksIN.append(in_block)
   

class AdderBlock(BaseBlock):
  def __init__(self, block_name):
    BaseBlock.__init__(self, block_name)
   

class ProductBlock(BaseBlock):
  def __init__(self, block_name):
    BaseBlock.__init__(self, block_name)
   

class GenericBlock(BaseBlock):
  def __init__(self, block_name, block_operator=None):
    # operator is the name (a string) of a Python function from the math library
    # which will be called when the block is evaluated
    # by default, initialized to None
    BaseBlock.__init__(self, block_name)
    self.__block_operator = block_operator
  
  def getBlockOperator(self):
    return self.__block_operator 

  def __repr__(self):    
    repr = BaseBlock.__repr__(self) 
    if self.__block_operator == None:
      repr += "  No operator given\n"
    else:
      repr += "  Operator :: " + self.__block_operator + "\n"
    return repr 

class DelayBlock(BaseBlock):
  def __init__(self, block_name):
    BaseBlock.__init__(self, block_name)

    # The block whose output is connected to the IC port, and 
    # None if no block connected to the IC port
    self.IC = None 

  def linkInput(self, in_block):
    if len(self.linksIN) >= 1:
      print "Warning: Delay (block %s) takes exactly one input, ignoring extra connection (from block %s)"\
             % (self.getBlockName(), in_block.getBlockName())
    else:  
      self.linksIN.append(in_block)

  def __repr__(self):
    repr = BaseBlock.__repr__(self) 
    if self.IC == None:
      repr += "  No incoming connection to IC port\n" 
    else:
      repr += "  IC <- " + self.IC.getBlockName() + ":" + self.IC.getBlockType() + "\n"
    return repr 
    

class TimeDelayBlock(DelayBlock):
  # serves as a base class for Integrator and Derivative
  def __init__(self, block_name, time_increment=0.001):
    DelayBlock.__init__(self, block_name)
    self.__timeIncrement = time_increment
  
  def getTimeIncrement(self):
    return self.__timeIncrement
 
  def __repr__(self):
    return DelayBlock.__repr__(self) #+ "  Time increment :: " + str(self.__timeIncrement)
    

class IntegratorBlock(TimeDelayBlock):
  def __init__(self, block_name, time_increment=0.001):
    TimeDelayBlock.__init__(self, block_name, time_increment=time_increment)

  def __repr__(self):
     return TimeDelayBlock.__repr__(self)

class DerivativeBlock(TimeDelayBlock):
  def __init__(self, block_name, time_increment=0.001):
    TimeDelayBlock.__init__(self, block_name, time_increment=time_increment)

  def __repr__(self):
     return TimeDelayBlock.__repr__(self)

class TestBlock(BaseBlock):
  def __init__(self, block_name):
    BaseBlock.__init__(self, block_name)
    # The block whose output is connected to the TRUE_in port, and 
    # None if no block connected to the TRUE_in port
    self.TRUE_in = None
    # The block whose output is connected to the FALSE_in port, and 
    # None if no block connected to the FALSE_in port
    self.FALSE_in = None
    
  def __repr__(self):    
    repr = BaseBlock.__repr__(self)
    if self.TRUE_in == None:
      repr += "  No incoming connection to TRUE_in port\n" 
    else:
      repr += "  TRUE_in <- " + self.TRUE_in.getBlockName() + ":" + self.TRUE_in.getBlockType() + "\n"
    if self.FALSE_in == None:
      repr += "  No incoming connection to FALSE_in port\n" 
    else:
      repr += "  FALSE_in <- " + self.FALSE_in.getBlockName() + ":" + self.FALSE_in.getBlockType() + "\n"
    return repr 

class CBD(BaseBlock):
  # The CBD class, contains an entire Causal Block Diagram 
  def __init__(self, block_name):
    BaseBlock.__init__(self, block_name)
    # The blocks in the CBD will be stored both 
    #  as an ordered list __blocks and   
    #  as a dictionary __blocksDict with the blocknames as keys for 
    #   fast name-based retrieval and 
    #   to ensure block names are unique within a single CBD
    self.__blocks = []
    self.__blocksDict = {}
    self.__replacedBlocks = {}
    self.__deltaTBlock = None

  def setBlocks(self, blocks):
    # blocks must be a list of BaseBlock (subclass) instances
    if type(blocks) == list:
      for block in blocks:
        if not isinstance(block, BaseBlock):
           exit("CBD.setBlocks() takes a list of BaseBlock (subclass) instances")
    else:
      exit("CBD.setBlocks() takes a list as argument, not a %s" % type(blocks))
   
  def getBlocks(self):
    return self.__blocks

  def getBlockByName(self, name):
    return self.__blocksDict[name]

  def addBlock(self, block):
    if not isinstance(block, BaseBlock):
      exit("Can only add BaseBlock (subclass) instances to a CBD")

    if isinstance(block, IntegratorBlock):
      # substitute Integrator block by using a delay block
      blockName = block.getBlockName()
      if self.__replacedBlocks.has_key(blockName):
        print "Warning: did not add this block as it has the same name %s as an existing block" % blockName
        return

      # substitute blocks
      self.addBlock(DelayBlock(block_name=blockName+"_delayIn"))
      self.addBlock(ProductBlock(block_name=blockName+"_product"))
      self.addBlock(DelayBlock(block_name=blockName+"_delayOut"))
      self.addBlock(AdderBlock(block_name=blockName+"_adder"))
      if self.__deltaTBlock == None:
        self.__deltaTBlock = ConstantBlock(block_name="deltaT", value=block.getTimeIncrement())
        self.addBlock(self.__deltaTBlock)

      # connections between substitute blocks
      self.addConnection(blockName+"_delayIn", blockName+"_product")
      self.addConnection("deltaT", blockName+"_product")
      self.addConnection(blockName+"_product", blockName+"_adder")
      self.addConnection(blockName+"_delayOut", blockName+"_adder")
      self.addConnection(blockName+"_adder", blockName+"_delayOut")

      # add original block to a seperate dictionary to be able to substitute connections
      self.__replacedBlocks[blockName] = block
      return

    elif isinstance(block, DerivativeBlock):
      # substitute Derivative block by using a delay block
      blockName = block.getBlockName()
      if self.__replacedBlocks.has_key(blockName):
        print "Warning: did not add this block as it has the same name %s as an existing block" % blockName
        return

      # substitute blocks
      self.addBlock(DelayBlock(block_name=blockName+"_delay"))
      self.addBlock(NegatorBlock(block_name=blockName+"_negator"))
      self.addBlock(AdderBlock(block_name=blockName+"_adder"))
      self.addBlock(InverterBlock(block_name=blockName+"_inverter"))
      self.addBlock(ProductBlock(block_name=blockName+"_product"))
      if self.__deltaTBlock == None:
        self.__deltaTBlock = ConstantBlock(block_name="deltaT", value=block.getTimeIncrement())
        self.addBlock(self.__deltaTBlock)

      # connections between substitute blocks
      self.addConnection(blockName+"_delay", blockName+"_negator")
      self.addConnection(blockName+"_negator", blockName+"_adder")
      self.addConnection("deltaT", blockName+"_inverter")
      self.addConnection(blockName+"_adder", blockName+"_product")
      self.addConnection(blockName+"_inverter", blockName+"_product")

      # add original block to a seperate dictionary to be able to substitute connections
      self.__replacedBlocks[blockName] = block
      return

    if not self.__blocksDict.has_key(block.getBlockName()):
      self.__blocks.append(block)
      self.__blocksDict[block.getBlockName()] = block
    else:
      print "Warning: did not add this block as it has the same name %s as an existing block" % block.getBlockName()

  def addConnection(self, from_block, to_block, port_name=None):

    # substitute connections to/from Integrator/Derivative blocks
    if self.__replacedBlocks.has_key(from_block):
      block = self.__replacedBlocks[from_block]
      if isinstance(block, IntegratorBlock):
        self.addConnection(from_block+"_delayOut", to_block)
      elif isinstance(block, DerivativeBlock):
        self.addConnection(from_block+"_product", to_block)
      return
    elif self.__replacedBlocks.has_key(to_block):
      block = self.__replacedBlocks[to_block]
      if isinstance(block, IntegratorBlock):
        if port_name == "IC":
          self.addConnection(from_block, to_block+"_delayIn", "IC")
          self.addConnection(from_block, to_block+"_delayOut", "IC")
        else:
          self.addConnection(from_block, to_block+"_delayIn")
      elif isinstance(block, DerivativeBlock):
        if port_name == "IC":
          self.addConnection(from_block, to_block+"_delay", "IC")
        else:
          self.addConnection(from_block, to_block+"_adder")
          self.addConnection(from_block, to_block+"_delay")
      return


    if type(from_block) == str:
      from_block = self.getBlockByName(from_block)
    if type(to_block) == str:
      to_block = self.getBlockByName(to_block)
    if port_name == None:
      to_block.linkInput(from_block) 
    elif isinstance(to_block, TestBlock): # TestBlock has two named ports
      if port_name == "TRUE":
        to_block.TRUE_in = from_block
      elif port_name == "FALSE":
        to_block.FALSE_in = from_block
      else: exit("Invalid port_name '%s' for TestBlock\n should be TRUE or FALSE" % port_name)
    elif isinstance(to_block, DelayBlock): # DelayBlock, DerivativeBlock, IntegratorBlock have one named port
      if port_name == "IC":
        to_block.IC = from_block
      else: exit("Invalid port_name '%s' for TestBlock\n should be IC" % port_name)
    else: 
      exit("addConnection to non-existing named port '%s'" % port_name)

  def __repr__(self):
    repr = BaseBlock.__repr__(self)
    repr += "\n"
    for block in self.getBlocks():
      repr+= block.__repr__()
    return repr

  def dump(self):
    print "=========== Start of Model Dump ==========="
    print self
    print "=========== End of Model Dump =============\n"

  def dumpSignals(self):
    print "=========== Start of Signals Dump ==========="
    for block in self.getBlocks():
        print "%s:%s" % (block.getBlockName(), block.getBlockType())
        print str(block.getSignal()) + "\n"
    print "=========== End of Signals Dump =============\n"

  def getSignalDict(self):
    dict = {}
    for block in self.getBlocks():
       dict[block.getBlockName()] = block.getSignal() 
    return dict


# To add: extensive conformance check of a model with the CBD meta-model.
# This, to avoid building nonsense models and above all to avoid
# passing such a model to a simulator.
# Best to add a checkConformance() method to CBD to be called by the simulator
# before starting.


# A note on hierarchically nested blocks (HV 31/10/2012).
#
# This used to be implemented in the ChildBlock class.
#
# As an entire CBD is now also a class, which specializes
# Baseblock, there is no more need for ChildBlock.
#
# For hierarchical modelling, the following need to be added:
#
# 1. input- and output-ports for entire CBDs. These will 
#    be modelled as InputBlock and OutputBlock classes.
#
# 2. a flatten() method which takes as input a hierarchical
#    CBD and produces as output a flattended CBD.
#    A flattened CBD will have unique names for all blocks and signals
#    obtained by "."-separated concatenation of all block
#    names, starting from the hierarchical model's root.
#    A flattened CBD will no longer have CBD instances in it.
#    All InputBlock and OutputBlock instances will have been
#    replaced by direct connections between blocks in the flattened model.
 
