from CBDsimulator import *
from Examples import *

class SimulateExample:

  def __init__(self, model):
    self.model  = model
    self.simulator = CBDsimulator(self.model)
 
  def run(self, num_steps=1): # one step = {NOW}
    for i in range(num_steps):
      self.simulator.step()

  def dumpModel(self):
    self.model.dump()

  def dumpSignals(self):
    self.model.dumpSignals()
  
if __name__== "__main__":
  
  # List of tuples of examples to run
  # 0: example name, 1: example model, 2: number of runs
  examples = []

  # Derivative block example
  examples.append(("Example Derivative", SimulateExample(ExampleDerivative("i")), 2))
  
  # Integrator block example
  examples.append(("Example Integrator", SimulateExample(ExampleIntegrator("i")), 2))
  
  # Circle test, integrator version
  examples.append(("Circle test, integrator version", SimulateExample(CircleTestIntegrator("i")), 2))
  
  # Circle test, derivative version
  examples.append(("Circle test, derivative version", SimulateExample(CircleTestDerivative("i")), 2))
  
  
  # Run examples
  for i in examples:
    example = i[1]
    print "#################################################"
    print "#  " + i[0]
    print "#################################################"
    example.dumpModel()
    example.run(i[2])
    example.dumpSignals()
    