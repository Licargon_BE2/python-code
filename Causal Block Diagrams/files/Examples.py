from CBD import *

class ExampleIntegrator(CBD):

  def __init__(self, block_name):
    CBD.__init__(self, block_name)
	
    # Blocks
    self.addBlock(IntegratorBlock(block_name="i1"))
    self.addBlock(ConstantBlock(block_name="c1", value=2.0))
    self.addBlock(ConstantBlock(block_name="c2", value=0.0))
	
    # Connections
    self.addConnection("c1", "i1")
    self.addConnection("c2", "i1", "IC")

class ExampleDerivative(CBD):

  def __init__(self, block_name):
    CBD.__init__(self, block_name)
    
    # Blocks
    self.addBlock(DerivativeBlock(block_name="d1"))
    self.addBlock(ConstantBlock(block_name="c1", value=2.0))
    self.addBlock(ConstantBlock(block_name="c2", value=0.0))
    
    # Connections
    self.addConnection("c1", "d1")
    self.addConnection("c2", "d1", "IC")
	
class CircleTestIntegrator(CBD):
	def __init__(self, block_name):
		CBD.__init__(self, block_name)
		
		# Blocks
		self.addBlock(IntegratorBlock(block_name="i1"))
		self.addBlock(IntegratorBlock(block_name="i2"))
		self.addBlock(ConstantBlock(block_name="IC1", value = 1))
		self.addBlock(ConstantBlock(block_name="IC2", value = 0))
		self.addBlock(NegatorBlock(block_name="n1"))
		
		# Connections
		self.addConnection("n1", "i1")
		self.addConnection("IC1", "i1", "IC")
		self.addConnection("i1", "i2")
		self.addConnection("IC2", "i2", "IC")
		self.addConnection("i2", "n1")
		
class CircleTestDerivative(CBD):
	def __init__(self, block_name):
		CBD.__init__(self, block_name)
		
		# Blocks
		self.addBlock(DerivativeBlock(block_name="d1"))
		self.addBlock(DerivativeBlock(block_name="d2"))
		self.addBlock(ConstantBlock(block_name = "IC1", value = 0))
		self.addBlock(ConstantBlock(block_name = "IC2", value = 1))
		self.addBlock(NegatorBlock(block_name = "n1"))
		
		# Connections
		self.addConnection("n1", "d1")
		self.addConnection("IC1", "d1", "IC")
		self.addConnection("d1", "d2")
		self.addConnection("IC2", "d2", "IC")
		self.addConnection("d2", "n1")
		