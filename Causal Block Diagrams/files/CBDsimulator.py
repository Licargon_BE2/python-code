import math

from CBD import *

class CBDsimulator:

  def __init__(self, model, curIteration=0):
    self.__model = model
    self.__curIteration = curIteration   
    self.__depGraph = None
    self.__sortedGraph = None
   
    # to add: flattening of hierarchical CBDs before simulation starts 
    
  def getModel(self):
    return self.__model
                              
  def step(self):    
      
    if (self.__curIteration < 2):
      self.__depGraph = self.__createDepGraph()
      print "----------------- Dependency Graph ---------------------"
      print self.__depGraph
      print "--------------------------------------------------------"
      self.__sortedGraph = self.__depGraph.getStrongComponents()
      print "-- Sorted collection of Strongly Connected Components --"
      print self.__sortedGraph # a list of lists
      print "--------------------------------------------------------"
 
    self.__computeBlocks()
    self.__curIteration += 1

  # Constructs the dependency graph of the current state of the CBD
  def __createDepGraph(self):  
    
    blocks = self.__model.getBlocks()
    depGraph = DepGraph()
  
    for block in blocks:      
      depGraph.addMember(block)
  
      # Treat dependencies on memory blocks (Delay, Integrator, Derivative) differently: 
      # During the first iteration (time == 0.0), the block only depends on the IC; 
      # During later iterations, it depends on the block type. 
      #
      # use depGraph.setDependency(block, block_it_depends_on)
    for block in blocks:
      if ( (isinstance(block, DelayBlock) or isinstance(block, IntegratorBlock) or isinstance(block, DerivativeBlock)) and (self.__curIteration == 0) ):
        # We only depend on the initial condition at this step
        depGraph.setDependency(block, block.IC)
      elif isinstance(block, TestBlock):
        # Testblock depends on the 2 possible input, and it's actual input
        depGraph.setDependency(block, block.TRUE_in)
        depGraph.setDependency(block, block.FALSE_in)
        depGraph.setDependency(block, block.linksIN[0])
      else:
        # Average case: block depends on all of its inputs
        for inputBlock in block.linksIN:
          print "Adding block: ", inputBlock
          depGraph.setDependency(block, inputBlock)

    return depGraph
    
  
  def __computeGeneric(self, block):
    operator = getattr(math, block.getBlockOperator())
    output = operator(block.linksIN[0].getSignal()[self.__curIteration])
    block.appendToSignal(output)

  def __computeConstant(self, block):
    block.appendToSignal(block.getValue())

  def __computeNegator(self, block):
    influentBlock = block.linksIN[0]
    block.appendToSignal(-influentBlock.getSignal()[self.__curIteration])

  def __computeInverter(self, block):
    influentBlock = block.linksIN[0]
    block.appendToSignal(1.0/influentBlock.getSignal()[self.__curIteration])
       
  def __computeAdder(self, block):
    sum = 0
    for influentBlock in block.linksIN:
      sum += influentBlock.getSignal()[self.__curIteration]
    block.appendToSignal(sum)

  def __computeProduct(self, block):
    prod = 1
    for influentBlock in block.linksIN:
      prod *= influentBlock.getSignal()[self.__curIteration]
    block.appendToSignal(prod)

  def __computeDelay(self, block):
    if (self.__curIteration == 0):
      block.appendToSignal(block.IC.getSignal()[self.__curIteration])
    else:
      block.appendToSignal(block.linksIN[0].getSignal()[self.__curIteration - 1])

  def __computeDerivative(self, block):
    # Should never be used. Derivative blocks should be replaced 
    # by block diagrams using only Delay blocks
    if (self.__curIteration == 0):
      output = block.IC.getSignal()[self.__curIteration]
    else:
      influent_block = block.linksIN[0] # only one input
      influent_signal = influent_block.getSignal()
      output = (influent_signal[self.__curIteration] - \
                influent_signal[self.__curIteration-1]) / \
                block.getTimeIncrement()
    block.appendToSignal(output)

  def __computeIntegrator(self, block):
    # Should never be used. Integrator blocks should be replaced 
    # by block diagrams using only Delay blocks
    if (self.__curIteration == 0):
      output = block.IC.getSignal()[self.__curIteration]
    else:
      influent_block = block.linksIN[0] # only one input
      influent_signal = influent_block.getSignal()
      output = block.getSignal()[self.__curIteration-1] + \
                influent_signal[self.__curIteration-1] * \
                block.getTimeIncrement()
    block.appendToSignal(output)

  def __computeTest(self, block):
    influentBlock = block.linksIN[0]
    if (influentBlock.getSignal()[self.__curIteration] > 0):
      if isinstance(block.TRUE_in, ConstantBlock):
        block.appendToSignal(block.TRUE_in.getValue())
      else:
        block.appendToSignal(block.TRUE_in.getSignal()[self.__curIteration])
    else:
      if isinstance(block.FALSE_in, ConstantBlock):
        block.appendToSignal(block.FALSE_in.getValue())
      else:
        block.appendToSignal(block.FALSE_in.getSignal()[self.__curIteration])

  def __computeBlocks(self):
    for component in self.__sortedGraph:
      if (not self.__hasCycle(component)):
        block = component[0]   # the strongly connected component has a single element
        if isinstance(block, GenericBlock):
          self.__computeGeneric(block)
        elif isinstance(block, ConstantBlock):
          self.__computeConstant(block)
        elif isinstance(block, NegatorBlock):
          self.__computeNegator(block)
        elif isinstance(block, InverterBlock):
          self.__computeInverter(block)
        elif isinstance(block, AdderBlock):
          self.__computeAdder(block)
        elif isinstance(block, ProductBlock):
          self.__computeProduct(block)
        elif isinstance(block, DerivativeBlock):
          self.__computeDerivative(block)
        elif isinstance(block, IntegratorBlock):
          self.__computeIntegrator(block)
        elif isinstance(block, DelayBlock): # note that this needs to come AFTER Derivative and Integrator !
                                            # as they are sub-classes of DelayBlock
          self.__computeDelay(block)
        elif isinstance(block, TestBlock):
          self.__computeTest(block)
        elif isinstance(block, CBD):
          raise ValueError("Hierachical blocks should have been flattened by now")
        else:
          raise ValueError("Unknown block type")
      else:
        # Detected a strongly connected component
        # assert self.__isLinear(component), "Cannot solve non-linear algebraic loop"
        if (self.__isLinear(component)):
          solverInput = self.__constructLinearInput(component)
          print "########### Input matrix for linear solver -> ###########"
          self.__printLinearInputMatrices(component, solverInput)
          self.__gaussjLinearSolver(solverInput)
          solutionVector = solverInput[1]
          print "########### Solution from linear solver -> ###########"
          for block in component:
            blockIndex = component.index(block) 
            block.appendToSignal(solutionVector[blockIndex])
            print block.getBlockName(), "=", solutionVector[blockIndex], "\n"
        else:
          print "########### Cannot solve non-linear algebraic loop ###########"
  
  # Determine whether a component is cyclic 
  def __hasCycle(self, component):
    assert len(component)>=1, "A component should have at least one element"
    if len(component)>1:
      return True
    else: # a strong component of size one may still have a cycle: a self-loop
      if self.__depGraph.hasDependency(component[0], component[0]):
        return True
      else:
        return False

  # Determines if an algebraic loop describes a linear equation or not
  def __isLinear(self, strongComponent):
    
    for block in strongComponent: # We need to check every block for linearity
      if isinstance(block, ProductBlock): 
        # A product is only linear is it's degree 1. Thus we check how many inputs are from within the SC
        # If there is more than 1 input from the SC, we have an expression of degree higher than one
        # thus meaning it's not linear
        count = 0
        for inputBlock in block.linksIN:
          if inputBlock in strongComponent:
            count += 1
        if count > 1:
          print "No linear loop detected"
          return False
      elif isinstance(block, GenericBlock) or isinstance(block, InverterBlock):
        # Only linear if the input is not in the SC, but as inverter only has 1 input
        # we can be sure that this block is not linear, as this is degree -1
        print "No linear loop detected"
        return False    
      elif isinstance(block, TestBlock):
        # Seeing as the input is a constant block, and this can't ever be in the SC, this block means it's not linear
        if block.linksIN[0] in strongComponent:
          print "No linear loop detected"
          return False
      # All other blocks are always linear, so we can conclude that we have a linear block
      print "Linear loop detected"
      return True
    

  # Constructs input for a solver of systems of linear equations
  # Input consists of two matrices:
  # M1: coefficient matrix, where each row represents an equation of the system
  # M2: result matrix, where each element is the result for the corresponding equation in M1
  def __constructLinearInput(self, strongComponent):
    size = len(strongComponent)
    row = []
    M1 = [] 
    M2 = []
    
    # Initialize matrices with zeros
    i = 0
    while (i < size):
      j = 0
      row = []
      while (j < size):
        row.append(0)
        j += 1
      M1.append(row)
      M2.append(0)
      i += 1
    
    i = 0
    for block in strongComponent: 
      if isinstance(block, NegatorBlock): # A = -B => A + B = 0
        M1[i][strongComponent.index(block)] = 1
        M1[i][strongComponent.index(block.linksIN[0])] = 1
        M2[i] = 0
      elif isinstance(block, AdderBlock): 
        # Not in SC: Add values to the righthand side. 
        # Block in SC: No explicit value, subtract them
        M1[i][strongComponent.index(block)] = 1 
        tempSum = 0
        for inputBlock in block.linksIN:
          if inputBlock in strongComponent:
            M1[i][strongComponent.index(inputBlock)] = -1 
          else:
            tempSum += inputBlock.getSignal()[self.__curIteration]
        M2[i] = tempSum # The result of the incoming calculated values added together
      elif isinstance(block, ProductBlock):
        # For example A = 1 * 2 * 4 * B => 0 = A - 8B
        M2[i] = 0
        M1[i][strongComponent.index(block)] = 1
        tempProduct = 1
        for inputBlock in block.linksIN:
          if inputBlock not in strongComponent:
            tempProduct *= inputBlock.getSignal()[self.__curIteration]
          else:
            index = strongComponent.index(inputBlock)
        M1[i][index] = -1 * tempProduct
      elif isinstance(block, TestBlock):
        # Output = B or Output = C -> Output - B = 0 or Output -C = 0
        # Get the constant input, calculate which fork to take, and put that value there
        M1[i][strongComponent.index(block)] = 1
        M2[i] = 0
        
        if block.linksIN[0].getSignal()[self.__curIteration] > 0:
          M1[i][strongComponent.index(block.TRUE_in)] = -1
        else:
          M1[i][strongComponent.index(block.FALSE_in)] = -1
      elif isinstance(block, DelayBlock) or isinstance(block, DerivativeBlock) or isinstance(block, IntegratorBlock):
        # Just propagate the IC input we get, e.g. A = B => A-B = 0
        M1[i][strongComponent.index(block)] = 1
        if (self.__curIteration == 0):
	    M1[i][strongComponent.index(block.IC)] = -1
        M2[i] = 0
      
      # Next row
      i += 1

    return [M1, M2]
   
  def __swap(self, a, b):
    t = a
    a = b
    b = t
    return (a, b)
  
  def __ivector(self, n):
    v = []
    for i in range(n):
      v.append(0)
    return v
    
  # Linear equation solution by Gauss-Jordan elimination
  def __gaussjLinearSolver(self, solverInput):
    M1 = solverInput[0]
    M2 = solverInput[1]
    n = len(M1)
    indxc = self.__ivector(n)
    indxr = self.__ivector(n)
    ipiv = self.__ivector(n)
    icol = 0
    irow = 0
    for i in range(n):
      big = 0.0
      for j in range(n):
        if (ipiv[j] != 1):
          for k in range(n):
            if (ipiv[k] == 0):
              if (math.fabs(M1[j][k]) >= big):
                big = math.fabs(M1[j][k])
                irow = j
                icol = k
            elif (ipiv[k] > 1): 
              raise ValueError("GAUSSJ: Singular Matrix-1")
      ipiv[icol] += 1
      if (irow != icol):
        for l in range(n):
          (M1[irow][l], M1[icol][l]) = self.__swap(M1[irow][l], M1[icol][l])
        (M2[irow], M2[icol]) = self.__swap(M2[irow], M2[icol])
      indxr[i] = irow
      indxc[i] = icol
      if (M1[icol][icol] == 0.0):
        raise ValueError("GAUSSJ: Singular Matrix-2")
      pivinv = 1.0/M1[icol][icol]
      M1[icol][icol] = 1.0
      for l in range(n):
        M1[icol][l] *= pivinv
      M2[icol] *= pivinv
      for ll in range(n):
        if (ll != icol):
          dum = M1[ll][icol]
          M1[ll][icol] = 0.0
          for l in range(n):
            M1[ll][l] -= M1[icol][l] * dum
          M2[ll] -= M2[icol] * dum
    l = n
    while (l > 0):
      l -= 1
      if (indxr[l] != indxc[l]):
        for k in range(n):
          (M1[k][indxr[l]], M1[k][indxc[l]]) = self.__swap(M1[k][indxr[l]], M1[k][indxc[l]])     
  
  # Print out the input matrices 
  def __printLinearInputMatrices(self, strongComponent, solverInput):
    print "Indices:"
    i = 0
    for block in strongComponent:
      print i, ":", block.getBlockName(), block.getBlockType()
      i += 1

    print "Matrices:"
    M1 = solverInput[0]
    M2 = solverInput[1]
    for row in M1:
      print row, "=", M2[M1.index(row)]

""" This module implements a dependency graph
    @author: Marc Provost
    @organization: McGill University
    @license: GNU General Public License
    @contact: marc.provost@mail.mcgill.ca
"""

import copy

class DepNode:
  """ Class implementing a node in the dependency graph.
  """
  
  def __init__(self, object):
    """ DepNode's constructor.
        @param object: Reference to a semantic object identifying the node
        @type object: Object
    """
    self.__object = object
    self.__isMarked   = False

  def mark(self):
    self.__isMarked = True
  
  def unMark(self):
    self.__isMarked = False
  
  def isMarked(self):
    return self.__isMarked 

  def getMappedObj(self):
    return self.__object
        
  def __repr__(self):
    return "DepNode :: "+str(self.__object)                 
          
class DepGraph:
  """ Class implementing dependency graph. 
  """

  def __init__(self):
    """ DepGraph's constructor.
    """
    #Dict holding a mapping "Object -> DepNode"
    self.__semanticMapping = {}
    
    #map object->list of objects depending on object
    self.__dependents = {}
    #map object->list of objects that influences object
    self.__influencers = {} 
 
  def __repr__(self):
    repr = "Dependents: \n"
    for dep in self.__dependents:
      repr += dep.getBlockName() + ":" + str(self.__dependents[dep]) + "\n"
    repr += "Influencers: \n"
    for infl in self.__influencers:
      repr += infl.getBlockName() + ":" + str(self.__influencers[infl]) + "\n"
    return repr

  def addMember(self, object):
    """ Add an object mapped to this graph.
        @param object: the object to be added
        @type object: Object
        @raise ValueError: If object is already in the graph
    """
    if not self.hasMember(object): 
      node = DepNode(object) 
      self.__dependents[object]                     = []
      self.__influencers[object]                    = []
      self.__semanticMapping[object] = node    
    else:
      raise ValueError("Specified object is already member of this graph")
      
  def hasMember(self, object):
    return self.__semanticMapping.has_key(object)      

  def removeMember(self, object):
    """ Remove a object from this graph.
        @param object: the object to be removed
        @type object: Object
        @raise ValueError: If object is not in the graph
    """  
    if self.hasMember(object):
      for dependent in self.getDependents(object):
        self.__influencers[dependent].remove(object)
      for influencer in self.getInfluencers(object):
        self.__dependents[influencer].remove(object)
      
      del self.__dependents[object]
      del self.__influencers[object]
      del self.__semanticMapping[object]       
    else:
      raise ValueError("Specified object is not member of this graph")

  def setDependency(self, dependent, influencer):
    """ Creates a dependency between two objects.
        @param dependent: The object which depends on the other
        @param influcencer: The object which influences the other
        @type dependent: Object
        @type dependent: Object
        @raise ValueError: if depedent or influencer is not member of this graph
        @raise ValueError: if the dependency already exists
    """
    if self.hasMember(dependent) and self.hasMember(influencer): 
      if not influencer in self.__influencers[dependent] and\
         not dependent in self.__dependents[influencer]:
        self.__influencers[dependent].append(influencer) 
        self.__dependents[influencer].append(dependent)
      else:
        raise ValueError("Specified dependency already exists")
    else:
      if not self.hasMember(dependent):
        raise ValueError("Specified dependent object is not member of this graph")
      if not self.hasMember(influencer):
        raise ValueError("Specified influencer object is not member of this graph")
        
  def hasDependency(self, dependent, influencer):
    if self.hasMember(dependent) and self.hasMember(influencer):          
      return influencer in self.__influencers[dependent] and\
             dependent in self.__dependents[influencer]    
    else:
      if not self.hasMember(dependent):
        raise ValueError("Specified dependent object is not member of this graph")
      if not self.hasMember(influencer):
        raise ValueError("Specified influencer object is not member of this graph")        
        
  def unsetDependency(self, dependent, influencer):
    """ Removes a dependency between two objects.
        @param dependent: The object which depends on the other
        @param influcencer: The object which influences the other
        @type dependent: Object
        @type dependent: Object
        @raise ValueError: if depedent or influencer is not member of this graph
        @raise ValueError: if the dependency does not exists
    """      
    if self.hasMember(dependent) and self.hasMember(influencer):   
      if influencer in self.__influencers[dependent] and\
         dependent in self.__dependents[influencer]:    
        self.__influencers[dependent].remove(influencer) 
        self.__dependents[influencer].remove(dependent)
      else:
        raise ValueError("Specified dependency does not exists")
    else:
      if not self.hasMember(dependent):
        raise ValueError("Specified dependent object is not member of this graph")
      if not self.hasMember(influencer):
        raise ValueError("Specified influencer object is not member of this graph")
                                                                                                                              
  def getDependents(self, object):
    if self.hasMember(object):
      return copy.copy(self.__dependents[object])
    else:
      raise ValueError("Specified object is not member of this graph") 

  def getInfluencers(self, object):
    if self.hasMember(object):
      return copy.copy(self.__influencers[object])
    else:
      raise ValueError("Specified object is not member of this graph") 
      
  def getStrongComponents(self):
    return self.__strongComponents()
      
  def __getDepNode(self, object):
    if self.hasMember(object):
      return self.__semanticMapping[object]
    else:
      raise ValueError("Specified object is not a member of this graph")      
      
  def __mark(self, object):
    self.__getDepNode(object).mark()
    
  def __unMark(self, object):
    self.__getDepNode(object).unMark()
    
  def __isMarked(self, object):
    return self.__getDepNode(object).isMarked()    
                                                      
  def __topoSort(self):
    """ Performs a topological sort on the graph.
    """
    for object in self.__semanticMapping.keys():
      self.__unMark(object)
    
    sortedList = []
      
    for object in self.__semanticMapping.keys():
      if not self.__isMarked(object):
        self.__dfsSort(object, sortedList)

    return sortedList
    
  def __dfsSort(self, object, sortedList):
    """ Performs a depth first search collecting
        the objects in topological order.
        @param object: the currently visited object.
        @param sortedList: partial sorted list of objects
        @type object: Object
        @type sortedList: list Of Object
    """
    
    if not self.__isMarked(object):
      self.__mark(object)
    
      for influencer in self.getInfluencers(object):      
        self.__dfsSort(influencer, sortedList)
    
      sortedList.append(object)

  def __strongComponents(self):
    """ Determine the strong components of the graph
        @rtype: list of list of Object
    """
    strongComponents = []
    sortedList = self.__topoSort()
    
    for object in self.__semanticMapping.keys():
      self.__unMark(object) 
          
    sortedList.reverse()
        
    for object in sortedList:
      if not self.__isMarked(object):
        component = []      
        self.__dfsCollect(object, component)
        strongComponents.append(component)
    
    strongComponents.reverse()         
    return strongComponents

  def __dfsCollect(self, object, component):
    """ Collects objects member of a strong component.
        @param object: Node currently visited
        @param component: current component
        @type object: Object
        @type component: List of Object
    """
    if not self.__isMarked(object):
      self.__mark(object)
                  
      for dependent in self.getDependents(object):
        self.__dfsCollect(dependent, component)
        
      component.append(object)
