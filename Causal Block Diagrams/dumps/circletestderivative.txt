#################################################
#  Circle test, derivative version
#################################################
=========== Start of Model Dump ===========
i:CircleTestDerivative
  No incoming connections to IN port

d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock
d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock
d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001
d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock
d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock
d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
IC1:ConstantBlock
  No incoming connections to IN port
  Value = 0
IC2:ConstantBlock
  No incoming connections to IN port
  Value = 1
n1:NegatorBlock
  IN <- d2_product:ProductBlock

=========== End of Model Dump =============

Adding block:  d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock

Adding block:  d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock

Adding block:  n1:NegatorBlock
  IN <- d2_product:ProductBlock

Adding block:  deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001

Adding block:  d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock

Adding block:  d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock

Adding block:  d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock

Adding block:  d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock

Adding block:  d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock

Adding block:  deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001

Adding block:  d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock

Adding block:  d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock

Adding block:  d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock

----------------- Dependency Graph ---------------------
Dependents: 
d2_adder:[d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
]
d1_adder:[d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
]
d2_delay:[d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock
]
d1_delay:[d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock
]
n1:[d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
]
IC1:[d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock
]
deltaT:[d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
, d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
]
d2_inverter:[d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
]
d1_inverter:[d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
]
d2_negator:[d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
]
d1_negator:[d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
]
IC2:[d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock
]
d2_product:[n1:NegatorBlock
  IN <- d2_product:ProductBlock
]
d1_product:[d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
]
Influencers: 
d2_adder:[d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock
, d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
]
d1_adder:[d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock
, n1:NegatorBlock
  IN <- d2_product:ProductBlock
]
d2_delay:[IC2:ConstantBlock
  No incoming connections to IN port
  Value = 1
]
d1_delay:[IC1:ConstantBlock
  No incoming connections to IN port
  Value = 0
]
n1:[d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
]
IC1:[]
deltaT:[]
d2_inverter:[deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001
]
d1_inverter:[deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001
]
d2_negator:[d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock
]
d1_negator:[d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock
]
IC2:[]
d2_product:[d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
, d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
]
d1_product:[d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
, d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
]

--------------------------------------------------------
-- Sorted collection of Strongly Connected Components --
[[IC2:ConstantBlock
  No incoming connections to IN port
  Value = 1
], [d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock
], [d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock
], [IC1:ConstantBlock
  No incoming connections to IN port
  Value = 0
], [d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock
], [d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock
], [deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001
], [d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
], [d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
], [d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
, d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
, n1:NegatorBlock
  IN <- d2_product:ProductBlock
, d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
, d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
]]
--------------------------------------------------------
Linear loop detected
########### Input matrix for linear solver -> ###########
Indices:
0 : d1_product ProductBlock
1 : d1_adder AdderBlock
2 : n1 NegatorBlock
3 : d2_product ProductBlock
4 : d2_adder AdderBlock
Matrices:
[1, -1000.0, 0, 0, 0] = 0
[0, 1, -1, 0, 0] = 0
[0, 0, 1, 1, 0] = 0
[0, 0, 0, 1, -1000.0] = 0
[-1, 0, 0, 0, 1] = -1
########### Solution from linear solver -> ###########
d1_product = 0.999999000001 

d1_adder = 0.000999999000001 

n1 = 0.000999999000001 

d2_product = -0.000999999000001 

d2_adder = -9.99999000001e-07 

Adding block:  n1:NegatorBlock
  IN <- d2_product:ProductBlock

Adding block:  d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock

Adding block:  d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock

Adding block:  n1:NegatorBlock
  IN <- d2_product:ProductBlock

Adding block:  deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001

Adding block:  d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock

Adding block:  d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock

Adding block:  d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock

Adding block:  d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock

Adding block:  d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock

Adding block:  d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock

Adding block:  deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001

Adding block:  d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock

Adding block:  d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock

Adding block:  d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock

----------------- Dependency Graph ---------------------
Dependents: 
d2_adder:[d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
]
d1_adder:[d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
]
d2_delay:[d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock
]
d1_delay:[d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock
]
n1:[d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock
, d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
]
IC1:[]
deltaT:[d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
, d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
]
d2_inverter:[d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
]
d1_inverter:[d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
]
d2_negator:[d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
]
d1_negator:[d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
]
IC2:[]
d2_product:[n1:NegatorBlock
  IN <- d2_product:ProductBlock
]
d1_product:[d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock
, d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
]
Influencers: 
d2_adder:[d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock
, d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
]
d1_adder:[d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock
, n1:NegatorBlock
  IN <- d2_product:ProductBlock
]
d2_delay:[d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
]
d1_delay:[n1:NegatorBlock
  IN <- d2_product:ProductBlock
]
n1:[d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
]
IC1:[]
deltaT:[]
d2_inverter:[deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001
]
d1_inverter:[deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001
]
d2_negator:[d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock
]
d1_negator:[d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock
]
IC2:[]
d2_product:[d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
, d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
]
d1_product:[d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
, d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
]

--------------------------------------------------------
-- Sorted collection of Strongly Connected Components --
[[deltaT:ConstantBlock
  No incoming connections to IN port
  Value = 0.001
], [d2_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
], [d1_inverter:InverterBlock
  IN <- deltaT:ConstantBlock
], [d2_negator:NegatorBlock
  IN <- d2_delay:DelayBlock
, d2_delay:DelayBlock
  IN <- d1_product:ProductBlock
  IC <- IC2:ConstantBlock
, d1_product:ProductBlock
  IN <- d1_adder:AdderBlock
  IN <- d1_inverter:InverterBlock
, d1_adder:AdderBlock
  IN <- d1_negator:NegatorBlock
  IN <- n1:NegatorBlock
, d1_negator:NegatorBlock
  IN <- d1_delay:DelayBlock
, d1_delay:DelayBlock
  IN <- n1:NegatorBlock
  IC <- IC1:ConstantBlock
, n1:NegatorBlock
  IN <- d2_product:ProductBlock
, d2_product:ProductBlock
  IN <- d2_adder:AdderBlock
  IN <- d2_inverter:InverterBlock
, d2_adder:AdderBlock
  IN <- d2_negator:NegatorBlock
  IN <- d1_product:ProductBlock
], [IC1:ConstantBlock
  No incoming connections to IN port
  Value = 0
], [IC2:ConstantBlock
  No incoming connections to IN port
  Value = 1
]]
--------------------------------------------------------
Linear loop detected
########### Input matrix for linear solver -> ###########
Indices:
0 : d2_negator NegatorBlock
1 : d2_delay DelayBlock
2 : d1_product ProductBlock
3 : d1_adder AdderBlock
4 : d1_negator NegatorBlock
5 : d1_delay DelayBlock
6 : n1 NegatorBlock
7 : d2_product ProductBlock
8 : d2_adder AdderBlock
Matrices:
[1, 1, 0, 0, 0, 0, 0, 0, 0] = 0
[0, 1, 0, 0, 0, 0, 0, 0, 0] = 0
[0, 0, 1, -1000.0, 0, 0, 0, 0, 0] = 0
[0, 0, 0, 1, -1, 0, -1, 0, 0] = 0
[0, 0, 0, 0, 1, 1, 0, 0, 0] = 0
[0, 0, 0, 0, 0, 1, 0, 0, 0] = 0
[0, 0, 0, 0, 0, 0, 1, 1, 0] = 0
[0, 0, 0, 0, 0, 0, 0, 1, -1000.0] = 0
[-1, 0, -1, 0, 0, 0, 0, 0, 1] = 0
########### Solution from linear solver -> ###########
d2_negator = 0.0 

d2_delay = 0.0 

d1_product = 0.0 

d1_adder = 0.0 

d1_negator = 0.0 

d1_delay = 0.0 

n1 = 0.0 

d2_product = 0.0 

d2_adder = 0.0 

=========== Start of Signals Dump ===========
d1_delay:DelayBlock
[0, 0.0]

d1_negator:NegatorBlock
[0, 0.0]

d1_adder:AdderBlock
[0.000999999000001, 0.0]

d1_inverter:InverterBlock
[1000.0, 1000.0]

d1_product:ProductBlock
[0.999999000001, 0.0]

deltaT:ConstantBlock
[0.001, 0.001]

d2_delay:DelayBlock
[1, 0.0]

d2_negator:NegatorBlock
[-1, 0.0]

d2_adder:AdderBlock
[-9.999990000010003e-07, 0.0]

d2_inverter:InverterBlock
[1000.0, 1000.0]

d2_product:ProductBlock
[-0.0009999990000010002, 0.0]

IC1:ConstantBlock
[0, 0]

IC2:ConstantBlock
[1, 1]

n1:NegatorBlock
[0.000999999000001, 0.0]

=========== End of Signals Dump =============