Python projects, University of Antwerp
==============

*Causal Block Diagrams*

Implementation of Causal Block Diagrams in Python. Made for the course "Modelling of Software Intensive Systems".

*Digital Watch*

Digital (stop)watch modelled in AToM3, with Python-code generated from it. Not a lot of programming myself, but a nice exercise for code generation.

*Pacman Multi Agent*

Pacman-agent implementing several search strategies and heuristics to create an agent that can reliably finish a game of Pacman by itself. Made in the framework "Pacman Projects" provided by UC Berkeley.

*Traffic Model Simulator*

System modelling a traffic situation (stretch of road with random cars going over it), to be used in a Python DEVS simulator.